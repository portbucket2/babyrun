using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;
using DG.Tweening;

public class BalloonOnCat : Task
{
    [SerializeField] private Animator catAnim;
    [SerializeField] private Transform target;
    [SerializeField] private FollowTargetAtRuntime balloon;
    [SerializeField] private CinemachineVirtualCamera vCam;
    [SerializeField] private AnimationCurve moveCurve;
    [SerializeField] private Vector2 multiplier;
    [SerializeField] private float duration;

    private Vector3 catStartPosition;

    public override void OnInteract()
    {
        catAnim.SetTrigger(Const.NEXT);
        balloon.target = target;
        vCam.LookAt = transform;
        vCam.Follow = transform;

        LevelManager.Instance.ForceFillMeter();

        catStartPosition = transform.position;
        DOVirtual.Float(0f, 1f, duration, MoveCat).SetDelay(0.5f);
        transform.DORotate(new Vector3(0f, 360f, 0f), duration, RotateMode.FastBeyond360).SetDelay(0.5f);
    }

    public override void OnStart()
    {
        //
    }

    private void MoveCat(float value)
    {
        transform.position = catStartPosition + new Vector3(-value * multiplier.x, moveCurve.Evaluate(value) * multiplier.y, 0f);
    }
}
