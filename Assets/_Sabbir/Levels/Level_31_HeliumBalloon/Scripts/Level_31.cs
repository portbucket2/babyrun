using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class Level_31 : MonoBehaviour
{
    [SerializeField] private Transform balloon;
    [SerializeField] private Transform cat;
    [SerializeField] private ParentRigController rig;
    [SerializeField] private float duration;
    [SerializeField] private Vector3 endPosition;

    private Tween petrol;

    private void Start()
    {
        LevelManager.Instance.onLevelFinished += LeaveBalloon;
        transform.DOMoveZ(-8f, 3f).OnComplete(StartPetrol);
    }

    private void LeaveBalloon(bool success)
    {
        if (success)
        {
            petrol.Kill();
            rig.SetAnimation(Const.CHASING);
            transform.DOMove(endPosition, duration).OnUpdate(() => transform.LookAt(balloon.position.XZ())).SetDelay(1f).OnComplete(() =>
            {
                rig.Grab(cat);
                balloon.parent = null;
                balloon.DOMoveY(50f, 25f);
            });
        }
        else
        {
            petrol.Kill();
            rig.SetAnimation(Const.FAIL);
            balloon.parent = null;
            balloon.DOMoveY(50f, 25f);
        }
    }

    public void StartPetrol()
    {
        petrol = DOTween.Sequence()
                .Append(transform.DORotate(Vector3.zero, 0.1f))
                .Append(transform.DOMoveZ(0f, 4f))
                .Append(transform.DORotate(new Vector3(0f, 180f, 0f), 0.1f))
                .Append(transform.DOMoveZ(-8f, 4f)).SetLoops(-1, LoopType.Restart);
    }
}
