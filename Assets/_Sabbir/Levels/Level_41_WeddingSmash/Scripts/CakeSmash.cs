using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class CakeSmash : MonoBehaviour
{
    [SerializeField] private Transform cake;
    [SerializeField] private Transform target;
    [SerializeField] private GameObject whole;
    [SerializeField] private GameObject smashed;

    private float randomValue = 1.5f;

    void Start()
    {
        float timer = Random.Range(1f, 1.5f);
        Vector3 targetPosition = target.position + new Vector3(Random.Range(-randomValue, randomValue), 0f, Random.Range(-randomValue, randomValue));
        cake.DOJump(targetPosition, 3f, 1, timer).OnComplete(() =>
        {
            whole.Inactive();
            smashed.Active();
        });
    }
}
