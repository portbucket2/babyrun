using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UntiePipe : Task
{
    [SerializeField] private Rigidbody[] pipeRigidbodies;
    [SerializeField] private GameObject[] ropes;


    public override void OnInteract()
    {
        Camera.main.GetComponent<SwitchVirtualCamera>().SwitchCam(1, 2);
        foreach (var item in pipeRigidbodies)
        {
            item.isKinematic = false;
            item.AddForce(item.transform.forward, ForceMode.Impulse);
        }

        foreach (var item in ropes)
        {
            item.Inactive();
        }
    }

    public override void OnStart()
    {
        //
    }
}
