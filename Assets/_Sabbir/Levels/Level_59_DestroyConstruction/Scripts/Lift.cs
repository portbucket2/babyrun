using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class Lift : MonoBehaviour
{
    private PlayerController playerController;

    private void Start()
    {
        playerController = PlayerController.Instance;

        transform.DOLocalMoveY(1.06f, 4f).OnComplete(() =>
        {
            playerController.transform.parent = null;
            playerController.inputEnabled = true;
            playerController.rb.isKinematic = false;
        });
    }
}
