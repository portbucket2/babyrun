using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class LiftDown : Task
{
    public override void OnInteract()
    {
        PlayerController.Instance.inputEnabled = false;
        PlayerController.Instance.MakeKinematic();
        PlayerController.Instance.transform.parent = transform;

        LevelManager.Instance.ForceFillMeter();

        transform.DOLocalMoveY(-10.3f, 4f).OnComplete(() =>
        {

        });
    }

    public override void OnStart()
    {
        //
    }
}
