using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class StrawExtend : Task
{
    [SerializeField] private Transform straw;
    [SerializeField] private Transform wine;

    private Vector3 wineScale;
    private LevelManager levelManager;

    public override void OnInteract()
    {
        straw.Active();
        straw.DOScaleY(1f, 0.5f).SetDelay(1f);
    }

    public override void OnStart()
    {
        levelManager = LevelManager.Instance;
        wineScale = wine.localScale;
    }

    private void Update()
    {
        wineScale.y = 2f - levelManager.ui.barFiller.fillAmount * 2f;
        wine.localScale = wineScale;
    }
}
