using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Level_91 : MonoBehaviour
{
    [SerializeField] private GameObject oldGlass;
    [SerializeField] private GameObject newGlass;

    private IEnumerator Start()
    {
        yield return new WaitForSeconds(1.75f);
        oldGlass.SwapWith(newGlass);
    }

}
