using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class BabiesInLine : MonoBehaviour
{
    [SerializeField] private Animator babyAnim;
    [SerializeField] private Animator teacherAnim;
    [SerializeField] private bool player;
    [SerializeField] public ChoiceLevelUI choiceScreen;
    [SerializeField] public int babyID;
    [SerializeField] public GameObject pie;

    internal bool selected = false;
    public void StartRun(Level_42 level_42)
    {
        StartCoroutine(BabyRoutine(level_42));
    }

    private IEnumerator BabyRoutine(Level_42 level_42)
    {
        babyAnim.SetTrigger(Const.RUN);
        transform.DOMoveX(2f, 1f);
        yield return new WaitForSeconds(1f);
        yield return Interact();
        transform.DORotate(180 * Vector3.up, 1f);
        transform.DOMoveZ(-9f, 5f);
        yield return new WaitForSeconds(1f);
        level_42.SwitchBaby();
    }

    private IEnumerator Interact()
    {
        if(!player)
        {
            transform.DOJump(transform.position, 0.75f, 1, 0.5f).SetDelay(0.5f);
            babyAnim.SetTrigger($"interact{babyID}");
            teacherAnim.SetTrigger($"interact{babyID}");
        }
        else
        {
            babyAnim.SetTrigger("idle");
            choiceScreen.ShowChoicePopup();
            yield return new WaitUntil(() => selected);
        }

        yield return new WaitForSeconds(1f);
    }


    public void OnCorrectButtonPressed()
    {
        transform.DOJump(transform.position, 0.75f, 1, 0.5f).SetDelay(0.5f);
        babyAnim.SetTrigger("interact1");
        teacherAnim.SetTrigger(Const.FAIL);
        DOVirtual.DelayedCall(0.75f, pie.Active, false);
        LevelManager.Instance.ForceFillMeter();
        selected = true;
    }

    public void OnWrongButtonPressed()
    {
        transform.DOJump(transform.position, 0.75f, 1, 0.5f).SetDelay(0.5f);
        babyAnim.SetTrigger("interact0");
        teacherAnim.SetTrigger("interact0");
        LevelManager.Instance.ForceFillMeter();
        selected = true;
    }
}

