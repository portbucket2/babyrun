using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class Level_42 : MonoBehaviour
{
    [SerializeField] private BabiesInLine[] babies;

    private int currentBaby = 0;

    void Start()
    {
        babies[currentBaby].StartRun(this);
    }

    public void SwitchBaby()
    {
        if (currentBaby >= 2) return;
        currentBaby++;
        babies[currentBaby].StartRun(this);
    }
}
