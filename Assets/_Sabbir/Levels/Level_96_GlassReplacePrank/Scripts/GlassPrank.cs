using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class GlassPrank : MonoBehaviour
{
    [SerializeField] private Transform correctGirl;
    [SerializeField] private Transform wrongGirl;
    [SerializeField] private Animator boyAnim;
    [SerializeField] private Animator girl1Anim;
    [SerializeField] private Animator girl2Anim;

    [SerializeField] private GameObject correctGlass_1;
    [SerializeField] private GameObject correctGlass_2;
    [SerializeField] private GameObject wrongGlass_1;
    [SerializeField] private GameObject wrongGlass_2;

    private string endClip;
    private Transform targetGirl;
    private Transform otherGirl;

    public void CorrectGirl()
    {
        endClip = Const.SUCCESS;
        targetGirl = correctGirl;
        otherGirl = wrongGirl;
        OnLevelEnd();
        DOVirtual.DelayedCall(0.5f, () => correctGlass_1.SwapWith(correctGlass_2), false);
    }

    public void WrongGirl()
    {
        endClip = Const.FAIL;
        targetGirl = wrongGirl;
        otherGirl = correctGirl;
        OnLevelEnd();
        DOVirtual.DelayedCall(0.5f, () => wrongGlass_1.SwapWith(wrongGlass_2), false);
    }

    private void OnLevelEnd()
    {
        PlayerController.Instance.inputEnabled = false;
        Camera.main.GetComponent<SwitchVirtualCamera>().SwitchCam(1, 2);
        transform.DOLookAt(targetGirl.GetChild(0).position, 0.2f).SetDelay(1f);
        transform.DOMove(targetGirl.position, 2f).SetDelay(1f).OnComplete(OnLevelComplete);
    }

    private void OnLevelComplete()
    {
        transform.DOLookAt(targetGirl.GetChild(0).position, 0.2f);
        otherGirl.GetChild(0).DOLookAt(targetGirl.GetChild(0).position, 0.2f);
        boyAnim.CrossFadeInFixedTime(endClip, 0.25f);
        girl1Anim.CrossFadeInFixedTime(endClip, 0.25f);
        girl2Anim.CrossFadeInFixedTime(endClip, 0.25f);
    }
}
