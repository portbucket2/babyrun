using DG.Tweening;
using System.Collections;
using UnityEngine;

public class PlaneController : MonoBehaviour
{
    private enum StartType { Instant, Tap, Delay }
    [SerializeField] private StartType startType;
    [SerializeField] private float delayTime;
    [SerializeField] private float levelDuration;
    [SerializeField] private VariableJoystick variableJoystick;
    [SerializeField] private Rigidbody rb;
    [SerializeField] private float speed;
    [SerializeField] private float elevation = 2f;
    [SerializeField] private float elevationTime = 2f;
    [SerializeField] private Animator planeAnim;

    private float posY;
    private LevelManager levelManager;
    private Vector3 camForward;
    private Vector3 camRight;
    private const float sqrDeadZone = 0.0001f;
    private Vector3 moveDirection;

    private void Start()
    {
        camForward = Camera.main.transform.forward;
        camForward.y = 0f;

        camRight = Camera.main.transform.right;
        camRight.y = 0f;

        moveDirection = transform.forward;

        levelManager = LevelManager.Instance;
        StartCoroutine(StartGame());
    }

    private IEnumerator StartGame()
    {
        switch (startType)
        {
            case StartType.Instant: break;
            case StartType.Tap: yield return new WaitUntil(() => Input.GetMouseButtonDown(0)); break;
            case StartType.Delay: yield return new WaitForSeconds(delayTime); break;
        }

        planeAnim.Play("fly");
        DOVirtual.Float(0f, elevation, elevationTime, value => posY = value);

        levelManager.GameStarted = true;

        DOVirtual.Float(0f, 1f, levelDuration, value => LevelManager.Instance.ui.barFiller.fillAmount = value);

        yield return new WaitForSeconds(levelDuration);
        levelManager.CheckLevelState();
    }

    public void Update()
    {
        if (!levelManager.InPlayMode) return;

        Vector3 direction = camForward * variableJoystick.Vertical + camRight * variableJoystick.Horizontal;

        if (direction.sqrMagnitude > sqrDeadZone)
        {
            moveDirection = direction;
        }
    }

    public void FixedUpdate()
    {
        if (!levelManager.InPlayMode) return;

        rb.MoveRotation(Quaternion.Lerp(transform.rotation, Quaternion.LookRotation(moveDirection, Vector3.up), 20f * Time.fixedDeltaTime));
        Vector3 targetPos = transform.position + transform.forward * speed * Time.fixedDeltaTime;
        targetPos.y = posY;
        rb.MovePosition(targetPos);
    }
}
