using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GetOnTheFireExtinguisher : Task
{
    [SerializeField] private Transform playerRoot;

    private PlayerController playerController;
    public override void OnInteract()
    {
        playerController.transform.parent = playerRoot;
        playerController.transform.ResetLocalPositionAndRotation();
        playerController.rig.SetBabyAnimation(Const.IDLE_SITTING_POOP);
        playerController.inputEnabled = false;
        playerController.MakeKinematic();
        ParentController.Instance.GetStartle();
    }

    public override void OnStart()
    {
        playerController = PlayerController.Instance;
    }
}
