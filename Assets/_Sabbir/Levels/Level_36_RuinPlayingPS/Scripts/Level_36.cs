using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class Level_36 : Interactable
{
    [SerializeField] private Rigidbody wireRigidbody;
    [SerializeField] private AdultRigController rig;
    [SerializeField] private Transform adult;

    public override void OnInteract()
    {
        wireRigidbody.isKinematic = false;
        wireRigidbody.AddForce(new Vector3(1f, 5f, 1f), ForceMode.Impulse);
        LevelManager.Instance.ForceFillMeter();

        adult.DOMoveX(2.9f, 0.25f);

        rig.SetTrigger(Const.FAIL);
        rig.ChangeFace(AdultFace.Surprised);
        rig.PlayExclamation();
    }
}
