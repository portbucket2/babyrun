using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class GirlCrying : MonoBehaviour
{
    private void Start()
    {
        DOTween.Sequence()
            .Append(transform.DOMoveX(1.8f, 1f))
            .Append(transform.DORotate(new Vector3(0f, 180f, 0f), 0.25f));
    }
}
