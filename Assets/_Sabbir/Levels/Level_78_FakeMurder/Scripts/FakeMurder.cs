using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class FakeMurder : MonoBehaviour
{
    [SerializeField] private float duration;
    [SerializeField] private Transform target;
    [SerializeField] private GameObject girl;
    [SerializeField] private RectTransform sleep;
    [SerializeField] private RectTransform angry;
    [SerializeField] private RectTransform barFiller;
    [SerializeField] private Animator anim;
    [SerializeField] private ParticleSystem sleepParticle;
    [SerializeField] private ParticleSystem sauceParticle;

    [SerializeField] private GameObject tutorialPanel;

    private SwitchVirtualCamera cam;
    private LevelManager levelManager;
    private PlayerController playerController;

    private void Start()
    {
        levelManager = LevelManager.Instance;
        playerController = PlayerController.Instance;
        playerController.transform.position = target.position;
        playerController.transform.rotation = target.rotation;
        playerController.inputEnabled = false;
        cam = Camera.main.GetComponent<SwitchVirtualCamera>();
        cam.SwitchCam(1, 2);
        playerController.moving = false;

        min = sleep.localPosition.x;
        max = angry.localPosition.x;
        barPosition = min;
        barInterval = max - min;
        SetFillerBar();


        tutorialPanel.Active();
    }

    internal Tween bar;

    private float barInterval;
    private float barPosition;

    private float min;
    private float max;

    private void SetFillerBar()
    {
        bar = DOVirtual.Float(0f, 1f, duration, value => levelManager.ui.barFiller.fillAmount = value).OnComplete(() =>
        {
            OnComplete();
            girl.Active();
            playerController.rig.SetBabyAnimation(Const.HOLDING, true);
            DOVirtual.DelayedCall(5f, () =>
            {
                anim.CrossFadeInFixedTime(Const.WAKEUP, 0.25f);
                levelManager.CheckLevelState();
            });
        });
        bar.timeScale = 0f;
    }

    void Update()
    {
        if (levelManager.gameEnded) return;

        if (Input.GetMouseButtonDown(0))
        {
            playerController.rig.SetBabyAnimation(Const.HOLDING, false);
            sauceParticle.Play();
            bar.timeScale = 1f;
        }

        if (Input.GetMouseButtonUp(0))
        {
            playerController.rig.SetBabyAnimation(Const.HOLDING, true);
            sauceParticle.Stop();
            bar.timeScale = 0f;
        }

        if (Input.GetMouseButton(0))
        {
            barPosition = Mathf.Clamp(barPosition + barInterval * Time.deltaTime * 0.5f, min, max);
        }
        else
        {
            barPosition = Mathf.Clamp(barPosition - barInterval * Time.deltaTime, min, max);
        }

        if (barPosition < max)
        {
            barFiller.localPosition = new Vector3(barPosition, barFiller.localPosition.y);
        }
        else
        {
            bar.Kill();
            anim.CrossFadeInFixedTime(Const.WAKEUP, 0.25f);
            levelManager.ReportState(LevelState.Fail);
            OnComplete();
            levelManager.CheckLevelState();
            //anim.transform.DOLookAt(playerController.transform.position, 0.25f);
        }
    }

    private void OnComplete()
    {
        sleepParticle.gameObject.Inactive();
        sauceParticle.Stop();
        cam.SwitchCam(2, 1);
        tutorialPanel.Inactive();
        this.enabled = false;
    }
}
