using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class GunPowder : Task
{
    private ParticleSystem mainParticle;
    private ParticleSystem.Particle[] particles;
    [SerializeField] private Transform targetPoint;
    [SerializeField] private Animator boyAnim;
    [SerializeField] private Animator girlAnim;

    public override void OnInteract()
    {
        ParticleSystem.EmissionModule emission = mainParticle.emission;
        emission.enabled = false;

        int particleCount = mainParticle.GetParticles(particles);

        for (int i = particleCount - 1; i >= 0; i--)
        {
            particles[i].remainingLifetime = 1f + (particleCount - i) * (2f / particleCount);
        }

        mainParticle.SetParticles(particles, particleCount);
        PlayerController.Instance.transform.position = targetPoint.position;
        PlayerController.Instance.inputEnabled = false;
        PlayerController.Instance.rig.SetBabyAnimation(Const.IDLE);
        Camera.main.GetComponent<SwitchVirtualCamera>().SwitchCam(1, 2);
        DOVirtual.DelayedCall(3f, OnLevelEnd, false);
    }

    public override void OnStart()
    {
        if (mainParticle == null) mainParticle = GetComponent<ParticleSystem>();

        if (particles == null || particles.Length < mainParticle.main.maxParticles)
            particles = new ParticleSystem.Particle[mainParticle.main.maxParticles];
    }

    private void OnLevelEnd()
    {
        boyAnim.CrossFadeInFixedTime(Const.SUCCESS, 0.25f);
        girlAnim.CrossFadeInFixedTime(Const.SUCCESS, 0.25f);
        LevelManager.Instance.ForceFillMeter();
    }
}
