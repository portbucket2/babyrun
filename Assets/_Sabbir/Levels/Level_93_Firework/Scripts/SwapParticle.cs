using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwapParticle : Task
{
    [SerializeField] private GrabableItem grabableItem;
    [SerializeField] private ParticleSystem burnParticel;
    [SerializeField] private ParticleSystem nonBurnParticel;

    public override void OnInteract()
    {
        grabableItem.itemParticle = burnParticel;
        burnParticel.Play();
        nonBurnParticel.Stop();
    }

    public override void OnStart()
    {
        //throw new System.NotImplementedException();
    }
}
