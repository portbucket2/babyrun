using DG.Tweening;
using UnityEngine;
using UnityEngine.Events;

public class ChoiceLevelEnding : MonoBehaviour
{
    [SerializeField] internal bool interactable = true;
    private enum EndChoice { HandOver, Throw}
    [SerializeField] private EndChoice endChoice;
    [SerializeField] private Transform targetPoint;
    [SerializeField] private float angle;
    [SerializeField] private float endDelay;
    [SerializeField] private UnityEvent levelEndEvent;
    [SerializeField] private UnityEvent successEvent;
    [SerializeField] private UnityEvent failEvent;
    [SerializeField] private ParticleSystem particle;

    public void Start()
    {
        LevelManager.Instance.onLevelFinished += OnLevelFisnished;
    }

    private void OnLevelFisnished(bool success)
    {
        if (success)
        {
            successEvent?.Invoke();
        }
        else
        {
            failEvent?.Invoke();
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (!interactable || !LevelManager.Instance.InPlayMode) return;
        if (other.CompareTag(Const.PLAYER)) OnInteract();
    }

    private void OnInteract()
    {
        interactable = false;
        PickableItem tr = PickableItem.currentItem;

        switch (endChoice)
        {
            case EndChoice.HandOver:
                tr.transform.SetParent(targetPoint, true);
                tr.transform.localPosition = tr.pickingPoint.InverseTransformPointUnscaled(tr.transform);
                tr.transform.localRotation = Quaternion.Inverse(tr.pickingPoint.rotation) * tr.transform.rotation;
                break;

            case EndChoice.Throw:
                tr.DropItems(targetPoint, angle);
                break;
        }

        levelEndEvent?.Invoke();
        DOVirtual.DelayedCall(endDelay, CallLevelEnd, false);
    }

    private void CallLevelEnd()
    {
        LevelManager.Instance.totalFill = 1f;
    }
}
