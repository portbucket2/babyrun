using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;

public class SwitchVirtualCamera : MonoBehaviour
{
    [SerializeField] private CinemachineVirtualCamera[] virtualCameras;

    public void SwitchCam(int from, int to)
    {
        int priority = virtualCameras[to - 1].Priority;
        virtualCameras[to - 1].Priority = virtualCameras[from - 1].Priority;
        virtualCameras[from - 1].Priority = priority;
    }
}
