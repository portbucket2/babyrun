using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwingOnCollision : MonoBehaviour
{
    [SerializeField] private Animator anim;

    private void OnCollisionEnter(Collision collision)
    {
        anim.Play("swing", -1, 0);
    }
}
