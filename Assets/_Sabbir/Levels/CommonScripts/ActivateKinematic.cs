using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActivateKinematic : MonoBehaviour
{
    [SerializeField] private Collider itemCollider;
    [SerializeField] private Rigidbody[] rigidbodies;
    [SerializeField] private bool hasUpForce;

    private bool interactable = true;

    private void OnCollisionEnter(Collision collision)
    {
        if (!interactable || !collision.gameObject.CompareTag(Const.PLAYER)) return;
  

        if(itemCollider) itemCollider.enabled = false;

        foreach (var item in rigidbodies)
        {
            item.isKinematic = false;
            if(hasUpForce) item.AddForce(Vector3.up * Random.Range(4.5f, 5.5f), ForceMode.Impulse);
        }

        interactable = false;
    }
}
