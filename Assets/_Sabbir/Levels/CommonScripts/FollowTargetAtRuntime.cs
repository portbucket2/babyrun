using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowTargetAtRuntime : MonoBehaviour
{
    [SerializeField] internal Transform target;
    [SerializeField] private Vector3 offset;
    [SerializeField] private bool rotateWithTarget;

    private void Update()
    {
        transform.position = target.position + offset;

        if (rotateWithTarget) transform.rotation = target.rotation;
    }

    public void ResetPosition()
    {
        transform.position = Vector3.zero;
    }
    public void ResetRotation()
    {
        transform.rotation = Quaternion.identity;
    }

    public void ResetPositionAndRotation()
    {
        transform.position = Vector3.zero;
        transform.rotation = Quaternion.identity;
    }
}
