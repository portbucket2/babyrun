using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using System;

public class TimeTap : MonoBehaviour
{
    [SerializeField] private float levelDuration = 10f;
    [SerializeField] private float  pointerSpeed = 1f;
    [SerializeField] private RectTransform sleep;
    [SerializeField] private RectTransform angry;
    [SerializeField] private RectTransform pointer;
    [SerializeField] internal GameObject tutorialPanel;

    private float barInterval;
    private float barPosition;
    private float min;
    private float max;

    internal bool initialized = false;

    private LevelManager levelManager;
    private Tween completionBar;

    public Action onLevelComplete;
    public Action onLevelFailed;

    public Action onTap;
    public Action onRelease;

    private void Start()
    {
        min = sleep.localPosition.x;
        max = angry.localPosition.x;
        barPosition = min;
        barInterval = max - min;

        levelManager = LevelManager.Instance;
        SetCompletionBar();
    }

    private void SetCompletionBar()
    {
        completionBar = DOVirtual.Float(0f, 1f, levelDuration, value => levelManager.ui.barFiller.fillAmount = value)
            .OnComplete(() => onLevelComplete?.Invoke());

        completionBar.timeScale = 0f;
    }

    private void Update()
    {
        if (!initialized || levelManager.gameEnded) return;

        if (Input.GetMouseButtonDown(0))
        {
            onTap?.Invoke();
            completionBar.timeScale = 1f;
        }

        if (Input.GetMouseButtonUp(0))
        {
            onRelease?.Invoke();
            completionBar.timeScale = 0f;
        }

        if (Input.GetMouseButton(0))
        {
            barPosition = Mathf.Clamp(barPosition + barInterval * Time.deltaTime * pointerSpeed * 0.5f, min, max);
        }
        else
        {
            barPosition = Mathf.Clamp(barPosition - barInterval * Time.deltaTime * pointerSpeed, min, max);
        }

        if (barPosition < max)
        {
            pointer.localPosition = new Vector2(barPosition, pointer.localPosition.y);
        }
        else
        {
            completionBar.Kill();
            onLevelFailed?.Invoke();
        }
    }
}
