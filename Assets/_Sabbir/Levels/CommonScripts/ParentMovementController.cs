using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEngine.Events;

public class ParentMovementController : MonoBehaviour
{
    [SerializeField] private float captureDistance = 0.3f;
    [SerializeField] private float turnTime = 0.5f;
    [SerializeField] private float chaseSpeed = 1.2f;
    [SerializeField] private float turnLerpRate = 3f;
    [SerializeField] private bool dontCatch = false;

    [Header("Unity Events")]
    [SerializeField] internal UnityEvent beforeChase;
    [SerializeField] internal UnityEvent afterChase;

    internal Transform target;

    private ParentController parentController;
    private PlayerController playerController;
    private Vector3 targetDirection;
    private float sqrCaptureDistance;

    private void Start()
    {
        parentController = ParentController.Instance;
        playerController = PlayerController.Instance;
        target = playerController.transform;
        sqrCaptureDistance = captureDistance * captureDistance;
    }

    private void Update()
    {
        if (!parentController.chasing || playerController.inJumpState) return;

        targetDirection = (target.position - transform.position).XZ();

        if (targetDirection.sqrMagnitude > sqrCaptureDistance)
        {
            transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.LookRotation(targetDirection, Vector3.up), turnLerpRate * Time.deltaTime);
            transform.Translate(transform.forward * Time.deltaTime * chaseSpeed, Space.World);
        }
        else
        {
            if(!dontCatch) parentController.OnBabyCaught();
        }
    }

    public void TurnTowardsBaby()
    {
        transform.DOLookAt(target.position.XZ(), turnTime);
    }
}
