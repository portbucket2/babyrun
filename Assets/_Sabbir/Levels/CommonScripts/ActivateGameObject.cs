using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActivateGameObject : MonoBehaviour
{
    [SerializeField] private Collider itemCollider;
    [SerializeField] private GameObject[] gameObjects;

    private bool interactable = true;

    private void OnCollisionEnter(Collision collision)
    {
        if (!interactable || !collision.gameObject.CompareTag(Const.PLAYER)) return;


        if (itemCollider) itemCollider.enabled = false;

        foreach (var item in gameObjects)
        {
            item.Active();
        }

        interactable = false;
    }
}
