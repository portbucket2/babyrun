using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BalloonPop : MonoBehaviour
{
    private Rigidbody ballonRigidbody;
    [SerializeField] private ParticleSystem particle;
    [SerializeField] private bool noPin = false;


    private void Start()
    {
        ballonRigidbody = GetComponent<Rigidbody>();
    }

    private void OnCollisionEnter(Collision collision)
    {
        if(collision.gameObject.CompareTag(Const.PLAYER))
        {
            ballonRigidbody.AddForce(Vector3.up * 0.025f, ForceMode.Impulse);

            if(noPin || PickableItem.currentItem)
            {
                if(particle) particle.Play();
                gameObject.SetActive(false);
            }
        }
        else if(collision.gameObject.CompareTag(Const.PARENT))
        {
            ballonRigidbody.AddForce(Vector3.up * 0.025f, ForceMode.Impulse);
        }
    }
}
