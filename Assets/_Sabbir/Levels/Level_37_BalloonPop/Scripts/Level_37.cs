using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Level_37 : MonoBehaviour
{
    [SerializeField] private ParentRigController parentRigController;

    private void Start()
    {
        parentRigController.SetAnimation(Const.CLAP);
    }
}
