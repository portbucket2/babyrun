using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEngine.Events;

public class FoamPrank : MonoBehaviour
{
    [SerializeField] private TimeTap timeTap;
    [SerializeField] private Transform startPoint;
    [SerializeField] private Animator victimAnim;
    [SerializeField] private float levelEndDelay = 1f;
    [SerializeField] private ParticleSystem sleepParticle;
    [SerializeField] private ParticleSystem mainParticle;
    [SerializeField] private Transform parentFailedTarget;
    [SerializeField] private Transform parentSuccessTarget;
    [SerializeField] private UnityEvent onLevelSuccess;
    [SerializeField] private UnityEvent onLevelFail;

    private SwitchVirtualCamera cam;
    private PlayerController playerController;

    private void Start()
    {
        playerController = PlayerController.Instance;
        playerController.transform.Relocate(startPoint);
        playerController.inputEnabled = false;
        playerController.moving = false;
        playerController.rig.SetBabyAnimation(Const.IDLE);

        cam = Camera.main.GetComponent<SwitchVirtualCamera>();
        cam.SwitchCam(1, 2);

        timeTap.onLevelComplete = OnLevelComplete;
        timeTap.onLevelFailed = OnLevelFailed;
        timeTap.onTap = OnTap;
        timeTap.onRelease = OnRelase;

        timeTap.initialized = true;

        timeTap.tutorialPanel.Active();
    }

    private void OnLevelComplete()
    {
        StartCoroutine(CompleteRoutine(levelEndDelay, true));
    }

    private void OnLevelFailed()
    {
        LevelManager.Instance.ReportState(LevelState.Fail);
        StartCoroutine(CompleteRoutine(levelEndDelay, false));
    }

    private void OnTap()
    {
        playerController.rig.SetBabyAnimation(Const.HOLDING, false);
        if (mainParticle) mainParticle.Play();
    }

    private void OnRelase()
    {
        playerController.rig.SetBabyAnimation(Const.HOLDING, true);
        if (mainParticle) mainParticle.Stop();
    }

    private IEnumerator CompleteRoutine(float endDelay, bool success)
    {
        if (sleepParticle) sleepParticle.gameObject.Inactive();
        if (mainParticle) mainParticle.Stop();
        cam.SwitchCam(2, 1);
        timeTap.tutorialPanel.Inactive();

        if (success)
        {
            victimAnim.CrossFadeInFixedTime(Const.SUCCESS, 0.25f);
        }
        else
        {
            victimAnim.CrossFadeInFixedTime(Const.FAIL, 0.25f);
        }

        LevelManager.Instance.CheckLevelState();

        yield return new WaitForSeconds(endDelay);
        if (success)
        {
            onLevelSuccess?.Invoke();
            if (parentSuccessTarget) victimAnim.transform.Relocate(parentSuccessTarget);
        }
        else
        {
            onLevelFail?.Invoke();
            if (parentFailedTarget)
            {
                victimAnim.transform.DOMove(parentFailedTarget.position, 0.25f);
                victimAnim.transform.DORotate(parentFailedTarget.rotation.eulerAngles, 0.25f);
            }
        }
    }
}
