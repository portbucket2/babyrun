using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;
using DG.Tweening;

public class SitOnChair : Task
{
    [SerializeField] private CinemachineVirtualCamera[] vCams;
    [SerializeField] private GameObject activate;
    [SerializeField] private float delay;

    internal bool startPaint;

    private Transform baby;
    private PlayerController playerController;

    public override void OnInteract()
    {
        baby.DOJump(transform.position, 0.5f, 1, 0.5f).OnComplete(() => baby.rotation = transform.rotation);
        playerController.MakeKinematic();
        playerController.inputEnabled = false;
        playerController.rig.SetBabyAnimation(Const.SIT);

        startPaint = true;
        SwitchCam(1, 2);
        PlayerController.Instance.inputEnabled = false;

        if (activate) StartCoroutine(DelayActive());
    }

    public override void OnStart()
    {
        playerController = PlayerController.Instance;
        baby = playerController.transform;
    }

    public void SwitchCam(int from, int to)
    {
        int priority = vCams[to - 1].Priority;
        vCams[to - 1].Priority = vCams[from - 1].Priority;
        vCams[from - 1].Priority = priority;
    }

    private IEnumerator DelayActive()
    {
        yield return new WaitForSeconds(delay);
        activate.Active();
        activate.transform.DOScale(1f, 0.25f);
    }
}
