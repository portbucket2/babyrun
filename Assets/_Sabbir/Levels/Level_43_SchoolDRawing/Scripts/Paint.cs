using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class Paint : MonoBehaviour
{
	[SerializeField] private Camera canvasCam;
	[SerializeField] private Transform brushContainer;
	[SerializeField] private GameObject brushPrefab;
	[SerializeField] private Color[] brushColors;
	[SerializeField] private float brushSize = 1.0f;
	[SerializeField] private LayerMask paintLayer;
	[SerializeField] private float duration;
	[SerializeField] private GameObject deactiveOnComplete;
	[SerializeField] private Animator anim;
	[SerializeField] private GameObject coin;

	private Camera sceneCamera;
	private Vector2 texCoord = Vector2.zero;
	private Color brushColor;
	private float orthographicSize;
	private int brushCounter = 0;
    private readonly int maxBrushCount = 3000;
	private bool taskCompleted = false;
	internal Tween bar;

	private float timer = 0f;

	private bool paintStarted = false;

	private void Start()
	{
		sceneCamera = Camera.main;
		brushColor = brushColors[0];
		orthographicSize = canvasCam.orthographicSize;
		StartCoroutine(Wait());
	}

	private IEnumerator Wait()
    {
		yield return new WaitUntil(() => paintStarted);

		bar = DOVirtual.Float(0f, 1f, duration, value => LevelManager.Instance.ui.barFiller.fillAmount = value).OnComplete(() =>
		{
			sceneCamera.GetComponent<SwitchVirtualCamera>().SwitchCam(2, 1);
			if (deactiveOnComplete) deactiveOnComplete.Inactive();
			LevelManager.Instance.ForceFillMeter();
			PlayerController.Instance.transform.rotation = Quaternion.Euler(0f, 90f, 0f);
			//anim.CrossFadeInFixedTime(Const.WAKEUP, 0.25f);
			paintStarted = false;
		});
		bar.timeScale = 0f;
	}

	public void StartPainting()
    {
		paintStarted = true;
    }

    private void Update()
    {
		if (!paintStarted) return;

		if(Input.GetMouseButtonDown(0))
        {
			bar.timeScale = 1f;
        }

		if (Input.GetMouseButtonUp(0))
		{
			bar.timeScale = 0f;
		}

		if (Input.GetMouseButton(0))
        {
			ApplyPaint();
        }
    }

	public void SelectBrushColor(int index)
    {
		brushColor = brushColors[index];
    }

    public void ApplyPaint()
	{
		if (taskCompleted) return;

		timer += Time.deltaTime;
		if (timer > 0.5f)
		{
			timer = 0f;
			if (Physics.Raycast(sceneCamera.ScreenPointToRay(Input.mousePosition), out RaycastHit hitInfo, 100f, paintLayer))
			{
				Instantiate(coin, hitInfo.point, Quaternion.identity);
			}
		}


		Vector3 uvWorldPosition = Vector3.zero;

		if (HitTestUVPosition(ref uvWorldPosition))
		{

			GameObject brushObj = Instantiate(brushPrefab, brushContainer);
			brushObj.GetComponent<SpriteRenderer>().color = brushColor;
			brushObj.transform.localPosition = uvWorldPosition;
			brushObj.transform.localScale = Vector3.one * brushSize;
			brushCounter++;
			
		}

		if (brushCounter >= maxBrushCount)
        {
			taskCompleted = true;
        }
	}

	private bool HitTestUVPosition(ref Vector3 uvWorldPosition)
	{
		if (Physics.Raycast(sceneCamera.ScreenPointToRay(Input.mousePosition), out RaycastHit hitInfo, 100f, paintLayer))
		{
			if ((hitInfo.textureCoord - texCoord).sqrMagnitude < 0.0001f) return false;

			texCoord = hitInfo.textureCoord;
			uvWorldPosition.x = texCoord.x - orthographicSize;
			uvWorldPosition.y = texCoord.y - orthographicSize;
			uvWorldPosition.z = 0.0f;
			return true;
		}
		else
		{
			return false;
		}
	}
}
