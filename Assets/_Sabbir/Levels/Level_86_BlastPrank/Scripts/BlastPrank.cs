using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class BlastPrank : Task
{
    [SerializeField] private Animator boyAnim;
    [SerializeField] private Animator dogAnim;
    [SerializeField] private Transform dogTarget;
    [SerializeField] private Transform boyTarget;
    [SerializeField] private ParticleSystem balloonBlast;
    [SerializeField] private GameObject balloon;

    private PlayerController playerController;

    public override void OnInteract()
    {
        balloon.Inactive();
        balloonBlast.transform.parent = null;
        balloonBlast.Play();
        playerController.inputEnabled = false;
        playerController.moving = false;
        playerController.transform.DOMove(boyTarget.position, 1f).OnComplete(() => playerController.rig.SetBabyAnimation(Const.HOLDING, false));
        dogAnim.transform.DOMove(dogTarget.position, 1f).SetDelay(0.5f);
        dogAnim.SetTrigger(Const.WAKEUP);
        boyAnim.CrossFadeInFixedTime(Const.WALK, 0.25f);
        boyAnim.transform.position = new Vector3(2f, 0f, -5.6f);
        boyAnim.transform.DORotate(new Vector3(0f, 270f, 0f), 0.2f);
        Camera.main.GetComponent<SwitchVirtualCamera>().SwitchCam(1, 2);
        DOTween.Sequence()
            .AppendInterval(1f)
            .Append(boyAnim.transform.DOMoveX(0.4f, 1f).OnComplete(() => boyAnim.transform.DORotate(new Vector3(0f, 180f, 0f), 0.2f)))
            .Append(boyAnim.transform.DOMoveZ(-8.3f, 1.5f).OnComplete(LookForBlast));
    }

    public override void OnStart()
    {
        playerController = PlayerController.Instance;
    }

    private void LookForBlast()
    {
        boyAnim.CrossFadeInFixedTime(Const.SUCCESS, 0.25f);
        boyAnim.transform.DORotate(new Vector3(0f, 120f, 0f), 0.2f);
        DOVirtual.DelayedCall(1f, LevelManager.Instance.ForceFillMeter);
    }
}
