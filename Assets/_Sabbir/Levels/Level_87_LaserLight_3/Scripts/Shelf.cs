using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shelf : Task
{
    [SerializeField] private Throwable[] throwable;
    public override void OnInteract()
    {
        foreach (var item in throwable)
        {
            item.Throw();
        }
    }

    public override void OnStart()
    {
        //
    }
}
