using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class Jump_87 : Task
{
    [SerializeField] private Transform target;
    [SerializeField] private Transform cat;
    [SerializeField] private Animator maleAnim;

    public override void OnInteract()
    {
        cat.DOJump(target.position, 0.7f, 1, 0.5f).OnComplete(() =>
        {
            maleAnim.CrossFadeInFixedTime(Const.SAD, 0.25f);
        });
    }

    public override void OnStart()
    {
        //throw new System.NotImplementedException();
    }
}
