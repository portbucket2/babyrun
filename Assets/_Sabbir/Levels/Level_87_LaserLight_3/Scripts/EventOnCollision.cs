using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class EventOnCollision : MonoBehaviour
{
    [SerializeField] private float delay;
    [SerializeField] private UnityEvent onCollisionEvent;

    private bool interactable = true;

    private void OnCollisionEnter(Collision collision)
    {
        if (interactable && collision.gameObject.CompareTag(Const.PLAYER))
        {
            interactable = false;
            StartCoroutine(TriggerEvent());
        }
    }

    private IEnumerator TriggerEvent()
    {
        yield return new WaitForSeconds(delay);
        onCollisionEvent?.Invoke();
    }
}
