using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class CarCrash : MonoBehaviour
{
    [SerializeField] private Transform car;
    [SerializeField] private Transform carDoor;
    [SerializeField] private Transform boy;
    [SerializeField] private Animator boyAnim;
    [SerializeField] private GameObject indicator;

    private void Start()
    {
        DOTween.Sequence()
            .Append(car.DOMoveZ(-6.5f, 1f))
            .Append(carDoor.DOLocalRotate(new Vector3(0f, -90f, 0f), 0.25f).OnComplete(GetDownFromCar))
            .Append(boy.DOMove(new Vector3(0.9f, 0f, -7.25f), 0.5f))
            .Append(boy.DOMove(new Vector3(3f, 0f, -10.8f), 2f).OnComplete(StartTask));
    }

    private void GetDownFromCar()
    {
        boy.parent = null;
        boyAnim.CrossFadeInFixedTime(Const.WALK, 0.25f);
    }

    private void StartTask()
    {
        boyAnim.CrossFadeInFixedTime("pull", 0.25f);
        indicator.Active();
    }
}
