using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class Task_Level101 : Task
{
    [SerializeField] private Transform target;
    [SerializeField] private Transform car;
    [SerializeField] private Animator boyAnim;
    [SerializeField] private Animator dogAnim;
    [SerializeField] private Transform dogTarget;
    [SerializeField] private GameObject garageDoor;
    [SerializeField] private ParticleSystem dustParticle;

    private PlayerController playerController;
    private bool interactable = false;

    public override void OnInteract()
    {
        interactable = true;
        LevelManager.Instance.ui.tutorialPanel.Active();
        playerController.inputEnabled = false;
        playerController.MakeKinematic();
        playerController.transform.parent = car;
        playerController.rig.SetBabyAnimation(Const.SIT);
        playerController.transform.DOJump(target.position, 0.25f, 1, 0.3f).OnComplete(OnBabySit);
        boyAnim.transform.DORotate(Vector3.zero, 0.1f);
        boyAnim.transform.DOMoveZ(-11f, 0.1f);
        boyAnim.CrossFadeInFixedTime(Const.SUCCESS, 0.25f);
        dogAnim.transform.DOMove(dogTarget.position, 1f).SetDelay(0.5f);
        dogAnim.SetTrigger(Const.WAKEUP);
    }

    private void Update()
    {
        if (!interactable) return;

        if(Input.GetMouseButtonDown(0))
        {
            OnTap();
            interactable = false;
            LevelManager.Instance.ui.tutorialPanel.Inactive();
        }
    }

    private void OnTap()
    {
        boyAnim.CrossFadeInFixedTime(Const.SLIP, 0.25f);
        car.DOMoveZ(-9f, 0.25f).SetEase(Ease.InExpo).OnComplete(() =>
        {
            garageDoor.Inactive();
            dustParticle.Play();
            LevelManager.Instance.ForceFillMeter();
        });
    }

    public override void OnStart()
    {
        playerController = PlayerController.Instance;
    }

    private void OnBabySit()
    {
        playerController.transform.RelocateLocal(target);
    }
}
