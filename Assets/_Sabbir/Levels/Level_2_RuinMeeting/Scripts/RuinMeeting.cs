﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RuinMeeting : MonoBehaviour
{
    public Animator chairanimator;
    public Animator parentAnim;

    public float enragetime=1;
    public float turntime=1;
    public float chasetime=1;
    public float shametime=1;
    public float turnbacktime = 1;
    public float returntime=1;
    public float parentmovedist = 1;

    public ParticleSystem angry;
    public ParticleSystem haha;
    public ParticleSystem exclaim;

    public Transform endPoint;

    private Transform target;
    private LevelManager levelManager;

    private bool taskStarted = false;
    private bool TaskCompleted = false;

    private void Start()
    {
        target = PlayerController.Instance.transform;
        levelManager = LevelManager.Instance;

        parentAnim.Play(Const.IDLE_SITTING_PC);
    }

    private void Update()
    {
        if(!taskStarted && PickItem.currentItem)
        {
            levelManager.MeterFiller();
            parentAnim.CrossFadeInFixedTime(Const.ANNOYED, 0.25f);
            exclaim.Play();
            taskStarted = true;
        }

        if (!taskStarted) return;

        levelManager.fillFactor = PickItem.currentItem && (transform.position - target.position).sqrMagnitude < 4f ? 1f : 0f;

        if (!TaskCompleted && levelManager.totalFill >= 0.99f)
        {
            PlayerController.Instance.RunForTheWin(endPoint.position);
            StartCoroutine(ParentRoutine());
            TaskCompleted = true;
        }
    }

    IEnumerator ParentRoutine()
    {
        parentAnim.CrossFadeInFixedTime(Const.ENRAGED, 0.25f);
        chairanimator.SetTrigger("move");
        angry.Play();
        //changeface(0);

        yield return new WaitForSeconds(enragetime);

        float t = 0;
        while (t < turntime)
        {
            //BabyCTRL.instance.camc.addoffset(Vector3.right * (Time.deltaTime / turntime));
            transform.Rotate(Vector3.up * (Time.deltaTime / turntime) * 180);
            t += Time.deltaTime;
            yield return null;
        }
        transform.eulerAngles = -90 * Vector3.up;
        parentAnim.SetTrigger("expose");
        haha.Play();
        Camera.main.GetComponent<SwitchVirtualCamera>().SwitchCam(1, 2);

        t = 0;

        while (t < chasetime)
        {
            transform.Translate(transform.right * (Time.deltaTime / chasetime) * parentmovedist);
            t += Time.deltaTime;
            yield return null;
        }
        parentAnim.SetTrigger("ashamed");


        yield return new WaitForSeconds(shametime);

        parentAnim.SetTrigger("return");

        t = 0;

        while (t < turnbacktime)
        {
            transform.Rotate(Vector3.up * (Time.deltaTime / turnbacktime) * 180);
            t += Time.deltaTime;
            yield return null;
        }
        transform.eulerAngles = 90 * Vector3.up;

        t = 0;

        while (t < returntime)
        {
            
            transform.Translate(-transform.right * (Time.deltaTime / returntime) * parentmovedist);
            t += Time.deltaTime;
            yield return null;
        }
        parentAnim.SetTrigger("repair");
    }
}
