using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEngine.Events;

public class CatShaving : MonoBehaviour
{
	[SerializeField] private Camera canvasCam;
	[SerializeField] private Transform brushContainer;
	[SerializeField] private GameObject brushPrefab;
	[SerializeField] private Color brushColor;
	[SerializeField] private float brushSize = 1.0f;
	[SerializeField] private LayerMask paintLayer;
	[SerializeField] private float duration;
	[SerializeField] private Animator anim;
	[SerializeField] private RectTransform sleep;
	[SerializeField] private RectTransform angry;
	[SerializeField] private RectTransform barFiller;

	[SerializeField] private ParticleSystem ps;
	[SerializeField] private ParticleSystem sleepParticle;
	[SerializeField] private Transform trimmer;
	[SerializeField] private UnityEvent onColorComplete;

	private Camera sceneCamera;
	private Vector2 texCoord = Vector2.zero;
	private float orthographicSize;
	private int brushCounter = 0;
	private readonly int maxBrushCount = 3000;
	private bool taskCompleted = false;
	
	internal Tween bar;
	private float barInterval;
	private float barPosition;

	private float min;
	private float max;

	private bool startTask = false;
	private bool painting = false;

	private SwitchVirtualCamera cam;
	private LevelManager levelManager;
	private PlayerController playerController;

	private float barSpeed = 3f;

	private void Start()
	{
		sceneCamera = Camera.main;
		cam = sceneCamera.GetComponent<SwitchVirtualCamera>();
		orthographicSize = canvasCam.orthographicSize;
		levelManager = LevelManager.Instance;
		playerController = PlayerController.Instance;

		min = sleep.localPosition.x;
		max = angry.localPosition.x;
		barPosition = min;
		barInterval = max - min;

		StartCoroutine(Wait());
	}
	  
	public void StartPaint()
    {
		startTask = true;
    }

	private IEnumerator Wait()
	{
		yield return new WaitUntil(() => startTask);
		cam.SwitchCam(1, 2);
		playerController.inputEnabled = false;
		playerController.transform.DOMove(new Vector3(-2f, 0f, -4f), 1f);

		bar = DOVirtual.Float(0f, 1f, duration, value => LevelManager.Instance.ui.barFiller.fillAmount = value).OnComplete(() =>
		{
			cam.SwitchCam(2, 1);
			trimmer.Inactive();
			anim.CrossFadeInFixedTime(Const.WAKEUP, 0.25f);
			onColorComplete?.Invoke();
			sleepParticle.Stop();
			levelManager.CheckLevelState();
		});
		bar.timeScale = 0f;
	}

	private void Update()
	{
		if (!startTask) return;

		//if (Input.GetMouseButtonDown(0))
		//{
		//	bar.timeScale = 1f;
		//	ps.Play();
		//}

		if (Input.GetMouseButtonUp(0))
		{
			bar.timeScale = 0f;
			ps.Stop();
		}

		if (Input.GetMouseButton(0))
		{
			Paint();

			if(painting)
            {
				bar.timeScale = 1f;
				ps.Play();
				barPosition = Mathf.Clamp(barPosition + barInterval * Time.deltaTime * barSpeed * 0.5f, min, max);
			}
			else
            {
				bar.timeScale = 0f;
				ps.Stop();
			}
		}
		else
		{
			barPosition = Mathf.Clamp(barPosition - barInterval * barSpeed * Time.deltaTime, min, max);
		}

		if (barPosition < max)
		{
			barFiller.localPosition = new Vector3(barPosition, barFiller.localPosition.y);
		}
		else
		{
			cam.SwitchCam(2, 1);
			trimmer.Inactive();
			bar.Kill();
			anim.CrossFadeInFixedTime(Const.WAKEUP, 0.25f);
			sleepParticle.Stop();
			levelManager.ReportState(LevelState.Fail);
			levelManager.CheckLevelState();
		}
	}

	public void Paint()
	{
		if (taskCompleted) return;
		Vector3 uvWorldPosition = Vector3.zero;

		if (HitTestUVPosition(ref uvWorldPosition))
		{
			painting = true;
			GameObject brushObj = Instantiate(brushPrefab, brushContainer);
			brushObj.GetComponent<SpriteRenderer>().color = brushColor;
			brushObj.transform.localPosition = uvWorldPosition;
			brushObj.transform.localScale = Vector3.one * brushSize;
			brushCounter++;
		}
		else
        {
			painting = false;
		}

		if (brushCounter >= maxBrushCount)
		{
			taskCompleted = true;
		}
	}

	private bool HitTestUVPosition(ref Vector3 uvWorldPosition)
	{
		if (Physics.Raycast(sceneCamera.ScreenPointToRay(Input.mousePosition), out RaycastHit hitInfo, 100f, paintLayer))
		{
			if ((hitInfo.textureCoord - texCoord).sqrMagnitude < 0.0001f) return false;

			trimmer.position = hitInfo.point;
			texCoord = hitInfo.textureCoord;
			uvWorldPosition.x = texCoord.x - orthographicSize;
			uvWorldPosition.y = texCoord.y - orthographicSize;
			uvWorldPosition.z = 0.0f;
			return true;
		}
		else
		{
			return false;
		}
	}
}
