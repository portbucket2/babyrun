using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CartBonusLevel : CartLevel
{
    [SerializeField] private GameObject coin;
    private Transform currentTransform;
    private float timer = 0f;

    protected override void OnStart()
    {
        base.OnStart();
        StartCoroutine(StartGame());
    }

    void Update()
    {
        if (Input.GetMouseButtonDown(0)) playerController.rig.SetBabyAnimation(Const.PICK_IDLE);
        if (!levelManager.InPlayMode) return;

        if (Input.GetMouseButtonUp(0)) playerController.rig.SetBabyAnimation(Const.IDLE);
        UpdateDistance();
    }


    private void FixedUpdate()
    {
        if (Input.GetMouseButton(0)) DropItem();
    }

    private void DropItem()
    {
        timer += Time.deltaTime;

        if (timer > 0.1f)
        {
            if (Physics.Raycast(pickingPoint.position, pickingPoint.forward, out RaycastHit hitInfo, 2f))
            {
                if (hitInfo.transform == currentTransform) return;

                currentTransform = hitInfo.transform;
                currentTransform.position = pickingPoint.position;
                currentTransform.GetComponent<Rigidbody>().isKinematic = false;
                Instantiate(coin, currentTransform.position, Quaternion.identity);
                timer = 0f;
            }
        }
    }

    private IEnumerator StartGame()
    {
        yield return StartControl();
        parentController.rig.PlayAnimation(Const.WALK_CART);

        yield return new WaitForSeconds(levelDuration);
        playerController.inputEnabled = false;
        levelManager.CheckLevelState();
    }
}

