using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEngine.Events;

public class ToothBrush : MonoBehaviour
{
    [SerializeField] internal bool interactable = true;
    [SerializeField] internal Transform boy;
    [SerializeField] internal Transform stool;
    [SerializeField] private UnityEvent levelEndEvent;
    [SerializeField] private UnityEvent successEvent;
    [SerializeField] private UnityEvent failEvent;
    [SerializeField] private ParticleSystem particle;

    [SerializeField] private GameObject[] pastes;
    [SerializeField] private GameObject[] brushes;

    private LevelManager levelManager;
    private PlayerController playerController;

    public void Start()
    {
        levelManager = LevelManager.Instance;
        levelManager.onLevelFinished += OnLevelFisnished;

        playerController = PlayerController.Instance;

    }

    private void OnLevelFisnished(bool success)
    {
        if (success)
        {
            successEvent?.Invoke();
        }
        else
        {
            failEvent?.Invoke();
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (!interactable || !LevelManager.Instance.InPlayMode) return;
        if (other.CompareTag(Const.PLAYER)) OnInteract();
    }

    private void OnInteract()
    {
        boy.Active();
        playerController.transform.DOJump(stool.position, 1f, 1, 0.5f).OnComplete(ActivePaste);
        DOTween.Sequence()
            .Append(boy.DOMoveX(0.225f, 3f))
            .Append(boy.DORotate(new Vector3(0f, 180f, 0f), 0.25f).OnComplete(SwitchBrush));
        interactable = false;
        levelEndEvent?.Invoke();
    }

    private void ActivePaste()
    {
        if(levelManager.state == LevelState.Fail)
        {
            pastes[0].Active();
        }
        else
        {
            pastes[1].Active();
        }

        playerController.rig.SetBabyAnimation(Const.PICK);
        playerController.rig.SetBabyAnimation(Const.HOLDING, false);
        playerController.transform.rotation = stool.rotation;
    }

    private void SwitchBrush()
    {
        brushes[0].Inactive();
        brushes[1].Active();

        DOVirtual.DelayedCall(2f, () => LevelManager.Instance.totalFill = 1f, false);
    }
}
