using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PenetrateItem : Interactable
{
    [SerializeField] private float bonusFill;
    [SerializeField] private Animator floater;
    [SerializeField] private Animator girl;
    [SerializeField] private Collider collider;

    public override void OnInteract()
    {
        if (!PickableItem.currentItem) return;
        //PlayerController.Instance.SetBabyAnimation(StringManager.THROW);
        PlayerController.Instance.transform.LookAt(transform.position.XZ());
        StartCoroutine(DelayStart());
    }

    private IEnumerator DelayStart()
    {
        GetComponent<Floater>().Kill();
        yield return new WaitForSeconds(0.2f);
        floater.enabled = true;
        girl.CrossFadeInFixedTime(Const.FLOATING_SCARE, 0.25f);
        LevelManager.Instance.totalFill += bonusFill;
    }
}
