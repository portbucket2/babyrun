using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class Floater : MonoBehaviour
{
    [SerializeField] private float duration;
    [SerializeField] private Vector3 rotationValue;

    private Transform child;
    ParentController parentController;

    private Vector3 rotation180 = new Vector3(0f, 180f, 0f);
    Sequence tweenMove;
    Tween tweenRotate;

    void Start()
    {
        child = transform.GetChild(0);
        parentController = ParentController.Instance;

        tweenMove = DOTween.Sequence()
        .Append(transform.DOMoveX(-3f, duration))
        .Append(transform.DORotate(Vector3.zero, 0.5f))
        .Append(transform.DOMoveX(-6f, duration))
        .Append(transform.DORotate(rotation180, 0.5f)).SetLoops(-1, LoopType.Restart);

        tweenRotate = child.DOLocalRotate(rotationValue, 2f).SetLoops(-1, LoopType.Yoyo);
    }

    public void Kill()
    {
        tweenMove.Kill();
        tweenRotate.Kill();
        this.enabled = false;
    }
}
