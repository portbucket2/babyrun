using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class Level_SelfPrank : Task
{
    [SerializeField] private Transform target;
    [SerializeField] private Transform baby;
    [SerializeField] private Animator maleAnim;
    [SerializeField] private Transform[] folder;
    [SerializeField] private Transform gate;

    private List<Vector3> startRotations;
    private Transform male;
    private PlayerController playerController;
    public override void OnInteract()
    {
        playerController.inputEnabled = false;
        playerController.transform.DOJump(target.position, 1f, 1, 0.5f).OnComplete(() =>
        {
            WrapBox();
            MoveParent();
        });
    }

    private void WrapBox()
    {
        folder[0].DORotate(Vector3.zero, 0.25f).OnComplete(() => folder[2].DORotate(Vector3.zero, 0.25f));
        folder[1].DORotate(Vector3.zero, 0.25f).OnComplete(() => folder[3].DORotate(Vector3.zero, 0.25f));
    }

    private void UnWrapBox()
    {
        folder[2].DORotate(startRotations[2], 0.25f).OnComplete(() => folder[0].DORotate(startRotations[0], 0.25f));
        folder[3].DORotate(startRotations[3], 0.25f).OnComplete(() => folder[1].DORotate(startRotations[1], 0.25f));
    }

    private void MoveParent()
    {
        DOTween.Sequence()
            .AppendInterval(1f)
            .Append(gate.DORotate(new Vector3(0f, 90f, 0f), 0.25f))
            .Append(male.DOMoveZ(-12.8f, 1f).OnStepComplete(() => maleAnim.CrossFadeInFixedTime(Const.STARTLE, 0.25f)))
            .AppendInterval(1.8f).OnStepComplete(() =>
            {
                UnWrapBox();
                playerController.transform.DOJump(target.position, 1f, 1, 0.5f);
            })
            .AppendInterval(1f).OnComplete(LevelManager.Instance.ForceFillMeter);
    }

    public override void OnStart()
    {
        playerController = PlayerController.Instance;
        male = maleAnim.transform;

        startRotations = new List<Vector3>();
        
        foreach (var item in folder)
        {
            startRotations.Add(item.rotation.eulerAngles);
        }
    }
}
