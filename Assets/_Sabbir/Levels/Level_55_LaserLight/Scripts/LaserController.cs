using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LaserController : MonoBehaviour
{
    [SerializeField] private LineRenderer line;
    [SerializeField] private Transform startPoint;
    [SerializeField] private Transform endPoint;
    [SerializeField] private Transform pointLight;
    [SerializeField] private LayerMask lightHitLayer;

    private bool started = false;

    private Vector3 positionOnScene;

    private void Start()
    {
        positionOnScene = new Vector3(Screen.width * 0.5f, 0f, 0f);
    }

    private void Update()
    {
        if (!started && LevelManager.Instance.GameStarted)
        {
            started = true;
            LevelManager.Instance.MeterFiller();
            ParentController.Instance.GetStartle();
        }
    }

    private void FixedUpdate()
    {
        if(Physics.Raycast(startPoint.position, endPoint.position - startPoint.position, out RaycastHit hitInfo, 50f, lightHitLayer))
        {
            pointLight.position = hitInfo.point;
        }

        if (endPoint.position.x > 6f)
        {
            endPoint.position = new Vector3(6, endPoint.position.y, endPoint.position.z);
        }
        else if (endPoint.position.x < -6f)
        {
            endPoint.position = new Vector3(-6, endPoint.position.y, endPoint.position.z);
        }

        if (endPoint.position.z > 6f)
        {
            endPoint.position = new Vector3(endPoint.position.x, endPoint.position.y, 6);
        }
        else if (endPoint.position.z < -6f)
        {
            endPoint.position = new Vector3(endPoint.position.x, endPoint.position.y, -6);
        }

        line.SetPosition(0, startPoint.position);
        line.SetPosition(1, pointLight.position);
    }

    private void LateUpdate()
    {
    }
}
