using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Aquarium : Task
{
    [SerializeField] private ParticleSystem particle;
    [SerializeField] private Animator[] fish;
    [SerializeField] private Transform target;

    public override void OnInteract()
    {
        particle.Play();
    }

    public override void OnStart()
    {
        throw new System.NotImplementedException();
    }
}
