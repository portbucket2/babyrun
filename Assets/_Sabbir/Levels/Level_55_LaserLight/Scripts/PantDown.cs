using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class PantDown : Task
{
    [SerializeField] private Animator boyAnim;
    [SerializeField] private Animator catAnim;
    [SerializeField] private Transform pant;

    public override void OnInteract()
    {
        Down();
    }

    public override void OnStart()
    {
        LevelManager.Instance.onLevelFinished += success => catAnim.CrossFadeInFixedTime("flip", 0.25f);
    }

    private void Down()
    {
        catAnim.transform.parent.position = transform.position;
        catAnim.CrossFadeInFixedTime("idle_hanging", 0.25f);
        pant.GetComponent<SkinnedMeshRenderer>().enabled = false;
        pant.GetComponent<MeshRenderer>().enabled = true;

        DOTween.Sequence()
            .Append(pant.DOLocalMoveY(-0.8f, 0.25f).OnComplete(() => boyAnim.CrossFadeInFixedTime("hide_dick", 0.25f)))
            .AppendInterval(1.4f)
            .Append(pant.DOLocalMoveY(0f, 0.25f)).OnComplete(() =>
            {
                pant.GetComponent<MeshRenderer>().enabled = false;
                pant.GetComponent<SkinnedMeshRenderer>().enabled = true;
            });
    }
}
