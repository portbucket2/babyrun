using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class HairCutting : MonoBehaviour
{
    [SerializeField] private Transform sessors;
    [SerializeField] private Transform strand;
    [SerializeField] private SwitchCameraOnInteract cam;
    [SerializeField] private float duration;
    [SerializeField] private RectTransform sleep;
    [SerializeField] private RectTransform angry;
    [SerializeField] private RectTransform barFiller;
    [SerializeField] private Animator anim;
    [SerializeField] private ParticleSystem sleepParticle;

    [SerializeField] private GameObject tutorialPanel;
    //[SerializeField] private Material hairMaterial;

    private LevelManager levelManager;

    void Start()
    {
        screenFactor = 10f / (float)Screen.width;

        min = sleep.localPosition.x;
        max = angry.localPosition.x;
        barPosition = min;
        barInterval = max - min;
        GetStrand();
        Wait();

        levelManager = LevelManager.Instance;

        currentStrand = strandList.Count - 1;
        tutorialPanel.Active();
    }

    private float startPos;
    private float screenFactor;

    private float timer = 0f;
    private int currentStrand;
    internal Tween bar;


    private float barInterval;
    private float barPosition;

    private float min;
    private float max;

    [System.Serializable]
    public struct Strand
    {
        public SkinnedMeshRenderer skinMeshRend;
        public MeshRenderer meshRend;
        public Rigidbody rb;
    }

    private List<Strand> strandList = new List<Strand>();
    private void GetStrand()
    {
        foreach (Transform tr in strand)
        {
            Strand st = new Strand
            {
                rb = tr.GetComponent<Rigidbody>(),
                skinMeshRend = tr.GetComponent<SkinnedMeshRenderer>(),
                meshRend = tr.GetComponent<MeshRenderer>()
            };

            strandList.Add(st);
        }
    }

    private void Wait()
    {
        bar = DOVirtual.Float(0f, 1f, duration, value => levelManager.ui.barFiller.fillAmount = value).OnComplete(() =>
        {
            sleepParticle.gameObject.Inactive();
            levelManager.CheckLevelState();
            cam.SwitchCam(2, 1);
            cam.startPaint = false;
            anim.CrossFadeInFixedTime(Const.THREAT, 0.25f);
            anim.transform.DOLookAt(PlayerController.Instance.transform.position, 0.25f);
            tutorialPanel.Inactive();
        });
        bar.timeScale = 0f;
    }

    void Update()
    {
        if (levelManager.gameEnded) return;

        if(Input.GetMouseButtonDown(0))
        {
            startPos = Input.mousePosition.x;
            bar.timeScale = 1f;
        }

        if(Input.GetMouseButtonUp(0))
        {
            bar.timeScale = 0f;
        }

        if(Input.GetMouseButton(0))
        {
            barPosition = Mathf.Clamp(barPosition + barInterval * Time.deltaTime * 0.5f, min, max);
            //float endPos = (Input.mousePosition.x - startPos) * screenFactor;
            //if(endPos < 0.01f)
            //{
                timer += Time.deltaTime;
            //}
        }
        else
        {
            barPosition = Mathf.Clamp(barPosition - barInterval * Time.deltaTime, min, max);
        }

        if (barPosition < max)
        {
            barFiller.localPosition = new Vector3(barPosition, barFiller.localPosition.y);
        }
        else
        {
            sleepParticle.gameObject.Inactive();
            cam.SwitchCam(2, 1);
            bar.Kill();
            anim.CrossFadeInFixedTime(Const.THREAT, 0.25f);
            anim.transform.DOLookAt(PlayerController.Instance.transform.position, 0.25f);
            levelManager.ReportState(LevelState.Fail);
            levelManager.CheckLevelState();
            tutorialPanel.Inactive();
            this.enabled = false;
        }


        if(timer > 0.25f)
        {
            if (currentStrand < 0) return;

            timer = 0f;
            strandList[currentStrand].rb.useGravity = true;
            strandList[currentStrand].skinMeshRend.enabled = false;
            strandList[currentStrand].meshRend.enabled = true;
            //strandList[currentStrand].meshRend.sharedMaterial = hairMaterial;
            sessors.DOMoveX(sessors.position.x - 0.024f, 0.1f);
            currentStrand--;
        }
    }
}
