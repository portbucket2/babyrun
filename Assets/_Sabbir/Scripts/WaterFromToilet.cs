using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class WaterFromToilet : MonoBehaviour
{
    [SerializeField] private Transform rightHand;
    [SerializeField] private Transform leftHand;
    [SerializeField] private Transform phone;
    [SerializeField] private Transform glass;
    [SerializeField] private Animator parentAnim;
    [SerializeField] private ParticleSystem particle;

    private Sequence walk;

    private LevelManager levelManager;

    private bool waterTaken = false;
    private bool waterPlaced = false;

    void Start()
    {
        levelManager = LevelManager.Instance;
        transform.DOMoveZ(0.25f, 2f).OnComplete(PickPhone);
    }

    private void Walk()
    {
        walk = DOTween.Sequence()
            .Append(transform.DOLocalRotate(new Vector3(0f, 270f, 0f), 0.5f))
            .Append(transform.DOMoveX(0.25f, 3f))
            .Append(transform.DOLocalRotate(new Vector3(0f, 180f, 0f), 0.5f))
            .Append(transform.DOMoveZ(-2.5f, 3f))
            .Append(transform.DOLocalRotate(new Vector3(0f, 0f, 0f), 0.5f))
            .Append(transform.DOMoveZ(0.25f, 3f))
            .Append(transform.DOLocalRotate(new Vector3(0f, 90f, 0f), 0.5f))
            .Append(transform.DOMoveX(2.5f, 3f).OnComplete(Drink))
            .AppendInterval(3f).OnComplete(() => {
                LevelManager.Instance.ForceFillMeter();
            });
    }

    void Update()
    {
        if(!waterTaken && PickableItem.currentItem)
        {
            waterTaken = true;
        }

        if (waterTaken && !PickableItem.currentItem)
        {
            waterPlaced = true;
        }

        if (levelManager.gameEnded)
        {
            walk.Kill();
            parentAnim.Play(Const.STARTLE);
            particle.Play();
            this.enabled = false;
        }
    }

    private void PickPhone()
    {
        phone.SetParent(rightHand);
        phone.localPosition = Vector3.zero;
        phone.localRotation = Quaternion.identity;
        parentAnim.CrossFadeInFixedTime(Const.WALK_MOBILE, 0.25f);
        Walk();
    }

    private void Drink()
    {
        if (waterPlaced)
        {
            glass.SetParent(leftHand);
            glass.localPosition = Vector3.zero;
            glass.localRotation = Quaternion.identity;
            parentAnim.CrossFadeInFixedTime(Const.DRINK, 0.25f);
        }
        else
        {
            walk.Restart();
            //walk.Kill();
            //parentAnim.CrossFadeInFixedTime(StringManager.IDLE, 0.25f);
            //PlayerController.Instance.Lose(false);
            //this.enabled = false;
        }
    }
}
