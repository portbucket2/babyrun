using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PoopGenerate : MonoBehaviour
{
    [SerializeField] internal GameObject poop;
    private int currentPoop = 0;
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.layer == LayerMask.NameToLayer("scratchable"))
        {
            GameObject prefab = poop.transform.GetChild(currentPoop % 5).gameObject;
            Vector3 target = transform.position;
            target.y += 0.3f;
            Vector3 point = other.ClosestPoint(target);
            Instantiate(prefab, point, Quaternion.identity);
            currentPoop++;
        }
    }
}
