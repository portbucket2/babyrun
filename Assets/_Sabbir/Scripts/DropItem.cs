using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEngine.Events;
public class DropItem : Interactable
{
    [SerializeField] internal Collider itemCollider;
    [SerializeField] internal Rigidbody rb;
    [SerializeField] private Transform throwPoint;
    [SerializeField] private float bonusFill;
    [SerializeField] private float angle = 60f;
    [SerializeField] private bool runAfterThrow = false;
    [SerializeField] private float dropDelay;
    [SerializeField] UnityEvent onDropComplete;


    public override void OnInteract()
    {
        if (!(PickItem.currentItem || PickableItem.currentItem)) return;
        PlayerController.Instance.rig.SetBabyAnimation(Const.THROW);
        PlayerController.Instance.transform.LookAt(transform.position.XZ());
        StartCoroutine(DelayStart());
    }

    private IEnumerator DelayStart()
    {
        yield return new WaitForSeconds(0.2f);
        LevelManager.Instance.totalFill += bonusFill;
        if (bonusFill >= 1f)
        {
            LevelManager.Instance.totalFill = 1;
        }
        DropCurrentItem();
        yield return new WaitForSeconds(dropDelay);
        onDropComplete?.Invoke();
    }
    void DropCurrentItem()
    {
        if (PickItem.currentItem) PickItem.currentItem.DropItems(throwPoint, angle);
        if (PickableItem.currentItem) PickableItem.currentItem.DropItems(throwPoint, angle);
    }
}
