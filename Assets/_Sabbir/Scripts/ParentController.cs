using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class ParentController : MonoBehaviour
{
    public static ParentController Instance { get; private set; }

    [SerializeField] internal ParentRigController rig;
    [SerializeField] internal ParentMovementController movement;
    [SerializeField] bool ignoreLevelComplete = false;
    [SerializeField] float startleDelay = 2f;

    private LevelManager levelManager;

    internal bool chasing = false;

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else if (Instance != this)
        {
            Destroy(gameObject);
        }
    }

    void Start()
    {
        levelManager = LevelManager.Instance;
        if(!ignoreLevelComplete) levelManager.onLevelFinished += OnLevelFinished;
    }

    void OnLevelFinished(bool levelSuccess)
    {
        if (chasing)
        {
            EndChase(!levelSuccess);
        }
        else
        {
            movement.TurnTowardsBaby();

            if (levelSuccess)
            {
                rig.GetSurprised();
            }
            else
            {
                rig.GetHappy();
            }
        }
    }

    public void OnBabyCaught()
    {
        levelManager.ReportState(LevelState.Fail);
        levelManager.ForceFillMeter();
    }

    public void StartChase()
    {
        chasing = true;
        PlayerController.Instance.lookAtCameraOnFail = false;
        rig.SetAnimation(Const.CHASING);
    }

    public void EndChase(bool babyCaught)
    {
        chasing = false;
        movement.beforeChase?.Invoke();

        if (babyCaught)
        {
            rig.Grab(movement.target);
        }
        else
        {
            rig.SetAnimation(Const.FAIL);
        }
    }

    public void GetStartle()
    {
        rig.GetSurprised();
        movement.TurnTowardsBaby();
        movement.beforeChase?.Invoke();
        DOVirtual.DelayedCall(startleDelay, StartChase, false);
    }
}

