using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomMovement : MonoBehaviour
{
    [SerializeField] private float radius;

    private Vector3 target;
    private Vector3 startPosition;

    private void Start()
    {
        startPosition = transform.position;
        target = startPosition + new Vector3(Random.Range(-radius, radius), 0f, Random.Range(-radius, radius));
        transform.LookAt(target);
    }
    private void Update()
    {
        if((transform.position - target).sqrMagnitude > 0.0001f)
        {
            transform.position = Vector3.MoveTowards(transform.position, target, Time.deltaTime);
        }
        else
        {
            target = startPosition + new Vector3(Random.Range(-radius, radius), 0f, Random.Range(-radius, radius));
            transform.LookAt(target);
        }
    }
}
