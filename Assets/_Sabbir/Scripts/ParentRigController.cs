using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParentRigController : MonoBehaviour
{
    [SerializeField] private GameObject[] parentFaces;
    [SerializeField] private Transform grabPoint;
    [SerializeField] private ParticleSystem exclamationParticle;
    [SerializeField] private Transform endTarget;

    private enum ParentFace { Normal, Surprised, Happy, Angry }
    private Animator anim;
    private int currentFace = 0;

    private void Start()
    {
        anim = GetComponent<Animator>();

        for (int i = 0; i < parentFaces.Length; i++)
        {
            parentFaces[i].SetActive(i == currentFace);
        }
    }

    public void GetSurprised(bool playAnimation = true)
    {
        ChangeFace(ParentFace.Surprised);
        if (playAnimation) SetAnimation(Const.STARTLE);
        if (exclamationParticle) exclamationParticle.Play();
    }
    
    public void GetHappy()
    {
        ChangeFace(ParentFace.Happy);
    }
    
    public void GetAngry()
    {
        ChangeFace(ParentFace.Angry);
    }

    private void ChangeFace(ParentFace index)
    {
        if (parentFaces.Length <= 0) return;
        parentFaces[currentFace].Inactive();
        currentFace = (int)index;
        parentFaces[currentFace].Active();
    }

    public void Grab(Transform tr, bool walk = true)
    {
        if (endTarget) tr = endTarget;

        tr.SetParent(grabPoint, true);
        tr.SetPositionAndRotation(grabPoint.position, grabPoint.rotation);
        ChangeFace(ParentFace.Happy);

        if (walk) StartCoroutine(CaughtRoutine());
    }

    internal IEnumerator CaughtRoutine()
    {
        SetAnimation(Const.SUCCESS);
        float elapsedTime = 0.0f;
        Vector3 direction = (Camera.main.transform.position.XZ() - transform.position).normalized;

        while (elapsedTime < 1.5f)
        {
            transform.Translate(direction * Time.deltaTime * 1f, Space.World);
            transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.LookRotation(direction, Vector3.up), 3f * Time.deltaTime);
            elapsedTime += Time.deltaTime;
            yield return null;
        }

        anim.enabled = false;
    }

    public void PlayAnimation(string stateName) => anim.Play(stateName);
    public void SetTrigger(string triggerName) => anim.SetTrigger(triggerName);
    public void SetAnimation(string stateName) => anim.CrossFadeInFixedTime(stateName, 0.25f);
}
