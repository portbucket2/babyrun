using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActiveCollider : MonoBehaviour
{
    [SerializeField] private Collider _collider;

    void Start()
    {
        _collider.enabled = true;
    }
}
