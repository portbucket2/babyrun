using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class Trail : MonoBehaviour
{
	[SerializeField] [Min(0)] private float thickness = 0.1f;
	[SerializeField] [Min(0)] private float segmentSize = 0.1f;
	[SerializeField] private Material material;
	[SerializeField] Transform pointer;
	[SerializeField] private float offest;

	[SerializeField] private LayerMask targetLayer;

	internal bool paintSide = false;
	internal Vector3 pointOnSide;

	private MeshFilter meshFilter;
	private MeshRenderer meshRenderer;
	private Mesh mesh;

	private List<Vector3> verts = new List<Vector3>();
	private List<int> triangles = new List<int>();


	private float sqrSegment;
	private int segmentCount = 0;
	private Vector3 previousPosition;

	private Camera cam;

	private Vector3 targetPoint;
	internal Vector3 norm;

	private void Awake()
	{
		AssignComponents();
		cam = Camera.main;
	}

	private void Start()
	{
		GetPoint();
		//targetPoint = pointer.position;
		sqrSegment = segmentSize * segmentSize;
		previousPosition = targetPoint;
		GetVertices(targetPoint, Vector3.zero);
		segmentCount++;
	}

	private void Update()
	{
		GetPoint();
		//targetPoint = pointer.position;
		Vector3 dir = targetPoint - previousPosition;
		if (dir.sqrMagnitude < sqrSegment) return;

		previousPosition = targetPoint;
		GetVertices(targetPoint, dir.normalized);
		GetTriangles(segmentCount);
		CreateMesh();
		segmentCount++;
	}

	private void GetPoint()
    {
		if (paintSide)
		{
			targetPoint = pointOnSide;
		}
		else
		{
			if (Physics.Raycast(pointer.position + Vector3.up, Vector3.down, out RaycastHit hitInfo, 5f, targetLayer))
			{
				targetPoint = hitInfo.point + Vector3.up * offest;
				norm = hitInfo.normal;
			}
		}
    }

	void CreateMesh()
	{
		mesh.Clear();
		mesh.SetVertices(verts);
		mesh.SetTriangles(triangles, 0);
		mesh.RecalculateNormals();
	}

	#region Get Vertices & Triangles
	private void GetVertices(Vector3 center, Vector3 forward)
	{
		Vector3 right = Vector3.Cross(norm, forward);

		var xVal1 = Mathf.Sin(Mathf.PI) * thickness;
		var yVal1 = Mathf.Cos(Mathf.PI) * thickness;

		var point1 = (norm * xVal1) + (right * yVal1) + center;
		verts.Add(point1);

		var xVal2 = Mathf.Sin(0) * thickness;
		var yVal2 = Mathf.Cos(0) * thickness;

		var point2 = (norm * xVal2) + (right * yVal2) + center;
		verts.Add(point2);
	}

	private void GetTriangles(int value)
	{
		int startIndex = 2 * (value - 1);
		triangles.Add(startIndex);
		triangles.Add(startIndex + 2);
		triangles.Add(startIndex + 1);

		triangles.Add(startIndex + 2);
		triangles.Add(startIndex + 3);
		triangles.Add(startIndex + 1);
	}
	#endregion

	#region AssignComponents
	void AssignComponents()
	{
		meshFilter = gameObject.GetComponent<MeshFilter>();
		if (!meshFilter) meshFilter = gameObject.AddComponent<MeshFilter>();

		meshRenderer = gameObject.GetComponent<MeshRenderer>();
		if (!meshRenderer) meshRenderer = gameObject.AddComponent<MeshRenderer>();

		if (!mesh) mesh = new Mesh();
		meshFilter.sharedMesh = mesh;

		if (material) meshRenderer.sharedMaterial = material;
	}
    #endregion
}
