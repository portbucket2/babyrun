using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class ColorLerp : MonoBehaviour
{
    [SerializeField] private SpriteRenderer rend;
    [SerializeField] private Color startColor;
    [SerializeField] private Color endColor;
    [SerializeField] private float duration;

    private void Start() => rend.DOColor(endColor, duration).SetLoops(-1, LoopType.Yoyo);
}
