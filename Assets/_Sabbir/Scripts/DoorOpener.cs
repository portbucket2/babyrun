using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class DoorOpener : Interactable
{
    [SerializeField] internal Transform openingPoint;
    [SerializeField] private GameObject dummyBottle;
    [SerializeField] private GameObject realBottle;
    [SerializeField] internal float openTime = 1f;

    public override void OnInteract()
    {
        //PlayerController.Instance.FridgeBehavior(this);
    }

    public void OpenDoor()
    {
        transform.DOLocalRotate(Vector3.zero, openTime).OnComplete(() =>
        {
            dummyBottle.SwapWith(realBottle);
            FridgeOpenCallback();
        });
    }

    public void FridgeOpenCallback()
    {
        ParentController.Instance.movement.TurnTowardsBaby();
    }
}
