using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class PickableItem : Interactable
{
    public static PickableItem currentItem;
    [Header("Pickable")]
    [SerializeField] private float eventDelay;
    [SerializeField] private UnityEvent onTaskComplete;
    [SerializeField] internal Transform pickingPoint;
    [SerializeField] private bool connectToSystem = true;
    [SerializeField] internal GrabableItem rightHandItem;
    [SerializeField] internal GrabableItem leftHandItem;

    public override void OnInteract()
    {
        if (currentItem) return;
        currentItem = this;

        StartCoroutine(UnityEventRoutine());

        Debug.Log(playerController);
        playerController.PickupBehavior(this.transform, pickType, HoldItem);


        if (!connectToSystem) return;

        levelManager.MeterFiller();
        if (parentController) parentController.GetStartle();
    }

    private IEnumerator UnityEventRoutine()
    {
        yield return new WaitForSeconds(eventDelay);
        onTaskComplete?.Invoke();
    }


    private void HoldItem()
    {
        PlayerController.Instance.SetHolding();
        if (rightHandItem)
        {
            rightHandItem.HoldItem(false);
        }
        if (leftHandItem)
        {
            leftHandItem.HoldItem(true);
        }
        LevelManager.Instance.onLevelFinished += (bool success) =>
        {
            DropItems(null, 0);
        };
    }



    #region Drop

    public void DropItems(Transform target, float angle)
    {
        if (!currentItem) return;
        if (rightHandItem)
        {
            rightHandItem.DropItem(target, angle);
        }
        if (leftHandItem)
        {
            leftHandItem.DropItem(target, angle);
        }
        PlayerController.Instance.rig.SetBabyAnimation(Const.HOLDING, false);
        currentItem = null;
    }


    #endregion

    #region OnStart
    private PlayerController playerController;
    private ParentController parentController;
    private LevelManager levelManager;

    private static string pickStateName;

    [SerializeField] private PickType pickType;
    private enum Hand { Right, Left }
    [SerializeField] private Hand hand;

    public override void OnStart()
    {
        playerController = PlayerController.Instance;
        parentController = ParentController.Instance;
        levelManager = LevelManager.Instance;

        pickStateName = pickType == PickType.Standing ? Const.PICK : Const.GROUND_PICK;
    }
    #endregion
}
public enum PickType { Standing, Bending }
