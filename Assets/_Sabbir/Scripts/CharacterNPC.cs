using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class CharacterNPC : MonoBehaviour
{
    [SerializeField] private Animator anim;
    [SerializeField] private Vector3 position;
    [SerializeField] private Vector3 rotation;

    void Start()
    {
        anim.Play(Const.WALK);
        transform.DOLocalMoveX(0f, 1.5f).OnComplete(() =>
        {
            transform.DOLocalRotate(rotation, 0.2f);
            transform.DOLocalMove(position, 0.2f);
            ChangeAnimation(Const.IDLE_SITTING_SOFA);
        }); 
        LevelManager.Instance.onLevelFinished += OnLevelFinished;
    }
    void OnLevelFinished(bool levelSuccess)
    {
        if (levelSuccess)
        {
            ChangeAnimation(Const.SUCCESS);
        }
        else
        {
            ChangeAnimation(Const.FAIL);
        }
    }

    public void ChangeAnimation(string stateName) => anim.CrossFadeInFixedTime(stateName, 0.25f);
}
