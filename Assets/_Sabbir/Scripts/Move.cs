using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class Move : MonoBehaviour
{
    [SerializeField] private Vector3 targetPoint;
    [SerializeField] private float delay;
    [SerializeField] private float duration;
    private enum Axis { X, Y, Z }
    [SerializeField] private Axis axis;
    [SerializeField] private Animator anim;

    private IEnumerator Start()
    {
        yield return new WaitForSeconds(delay);

        switch(axis)
        {
            case Axis.X: transform.DOMoveX(targetPoint.x, duration); break;
            case Axis.Y: transform.DOMoveY(targetPoint.y, duration); break;
            case Axis.Z: transform.DOMoveZ(targetPoint.z, duration); break;
        }

        if (anim)
        {
            yield return new WaitForSeconds(duration);
            anim.CrossFadeInFixedTime(Const.STARTLE, 0.25f);
        }
    }
}
