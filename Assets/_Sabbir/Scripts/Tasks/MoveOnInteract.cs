using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class MoveOnInteract : Interactable
{
    [SerializeField] private Vector3 target;
    [SerializeField] private float duration;
    [SerializeField] private GameObject activeOnComplete;
    [SerializeField] private GameObject deactiveOnComplete;

    [SerializeField] private bool playerStopMoving;
    private Rigidbody rb;

    public override void OnStart()
    {
        rb = GetComponent<Rigidbody>();
    }

    public override void OnInteract()
    {
        if (playerStopMoving)
        {
            PlayerController.Instance.rb.isKinematic = true;
            PlayerController.Instance.transform.parent = transform;
        }

        rb.DOMove(target, duration).OnComplete(() =>
        {
            if(activeOnComplete) activeOnComplete.Active();
            if(deactiveOnComplete) deactiveOnComplete.Inactive();
            if (playerStopMoving)
            {
                PlayerController.Instance.transform.parent = null;
                PlayerController.Instance.rb.isKinematic = false;
            }
        });
    }
}
