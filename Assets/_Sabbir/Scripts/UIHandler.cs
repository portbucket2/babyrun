using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using DG.Tweening;
using TMPro;
using FRIA;
using PotatoSDK;
public class UIHandler : MonoBehaviour
{
    [SerializeField] internal SuccessPanelController successCon;
    [SerializeField] internal GameObject bar;
    [SerializeField] internal GameObject objective;
    [SerializeField] internal TextMeshProUGUI levelText;
    [SerializeField] internal Transform coinIcon;
    [SerializeField] internal TextMeshProUGUI coinText;
    [SerializeField] internal Image barFiller;
    [SerializeField] internal Button retryButton;
    [SerializeField] internal Button home2Button;
    //[SerializeField] internal GameObject completePanel;
    [SerializeField] internal GameObject failPanel;
    [SerializeField] internal TextMeshProUGUI earnedText;
    [SerializeField] internal int earnedValue;
    [SerializeField] internal GameObject tutorialPanel;
    [SerializeField] internal float movingFillRate = 0.1f;
    [SerializeField] internal float stationaryFillRate = 0.05f;
    

    internal void OnAwake()
    {
        //if (LevelLoader.Instance)
        {
            retryButton.onClick.AddListener(() => {
                retryButton.interactable = false;
                home2Button.interactable = false;
                AnalyticsAssistant.ReportClick(AnalyticClickType.LEVEL_RETRY, LevelLoader.SelectedLevelNumber);
                //LevelProgressionController.Proceed_IsBonus(false);
                if(BuildSpecManager.enableInterstitial) MAXMan.ShowInterstitial();
                LevelLoader.LoadLevelDirect();
            });

            home2Button.onClick.AddListener(()=> {
                retryButton.interactable = false;
                home2Button.interactable = false;
                AnalyticsAssistant.ReportClick(AnalyticClickType.HOME_BUTTON, LevelLoader.SelectedLevelNumber-1, "ON_FAIL"); 
                if (BuildSpecManager.enableInterstitial) MAXMan.ShowInterstitial();
                LevelLoader.LoadHome();
            });
        }
        //else
        //{
        //    retryButton.onClick.AddListener(() => SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex));
        //    nextButton.onClick.AddListener(() => SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex));
        //}

    }

    public void UpdateCoinText(int value)
    {
        coinText.text = $"{value}";
        coinIcon.transform.DOScale(1.3f, 0.05f).SetLoops(2, LoopType.Yoyo);
    }

    public void HideTutorialPanel() => tutorialPanel.Inactive();

    public void ShowFailPanel()
    {
        objective.Inactive();
        bar.Inactive();
        failPanel.Active();
    }

    public void ShowCompletePanel()
    {
        objective.Inactive();
        bar.Inactive();
        successCon.Load();
        //completePanel.Active();
        //StartCoroutine(EarnTransition());
        //"level number increased".Debug("FF0000");
    }

    private IEnumerator EarnTransition()
    {
        //PlayerPrefs.SetInt("coin", LevelManager.Instance.CoinCount + earnedValue);
        //int initCoin = LevelManager.Instance.CoinCount;
        //float timeElapsed = 0f;
        earnedText.text = $"{earnedValue}";

        //while (timeElapsed < 1f)
        //{
        //    timeElapsed += Time.deltaTime;
        //    int coin = Mathf.Clamp(Mathf.FloorToInt(timeElapsed * earnedValue), 0, earnedValue);
        //    coinText.text = $"{initCoin + coin}";
            yield return null;
        //}
        //LevelManager.Instance.CoinCount += earnedValue;
    }
}
