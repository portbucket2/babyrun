using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class MovementHandler : MonoBehaviour
{
    private const float sqrDeadZone = 0.0001f;
    [SerializeField] internal float speed = 2;
    [SerializeField] internal float dragSpeed = 2;
    [SerializeField] internal float dragBackSpeed = -0.1f;
    [SerializeField] internal Vector3 moveDirection = Vector3.zero;

    private VariableJoystick variableJoystick;
    private PlayerController playerController;
    private LevelManager levelManager;
    private Rigidbody rb;
    private Transform cameraTransform;
    private bool climbing = false;

    private void Awake()
    {
        variableJoystick = FindObjectOfType<VariableJoystick>();
        playerController = GetComponent<PlayerController>();
        cameraTransform = Camera.main.transform;
        rb = GetComponent<Rigidbody>();
    }

    private void Start()
    {
        levelManager = LevelManager.Instance;
    }

    #region Basic Input and Movement
    private void Update()
    {
        if (!levelManager.InPlayMode) return;
        if (!playerController.inputEnabled) return;

        Vector3 camForward = cameraTransform.forward.XZ();
        Vector3 camRight = cameraTransform.right.XZ();
        Vector3 direction = camForward * variableJoystick.Vertical + camRight * variableJoystick.Horizontal;

        if (direction.sqrMagnitude > sqrDeadZone)
        {
            moveDirection = direction.normalized;
            if (!playerController.moving)
            {
                playerController.OnMoveStart();
            }
        }
        else
        {
            if (playerController.moving)
            {
                moveDirection = Vector3.zero;
                playerController.OnIdleStart();
            }
        }
    }


    public void FixedUpdate()
    {
        if (!LevelManager.Instance.InPlayMode) return;
        if (!playerController.inputEnabled) return;

        switch (playerController.babyState)
        {
            case BabyState.Normal:
                if (playerController.moving)
                {
                    SetMoveRotation(moveDirection);
                    rb.MovePosition(transform.position + transform.forward * speed * Time.fixedDeltaTime);

                }
                break;
            case BabyState.Interested:
                break;
            case BabyState.Drag:
                if (playerController.moving)
                {
                    SetMoveRotation(moveDirection);
                    rb.MovePosition(transform.position + transform.forward * dragSpeed * Time.fixedDeltaTime);
                }
                else
                {
                    rb.MovePosition(transform.position + transform.forward * dragBackSpeed * Time.fixedDeltaTime);
                }
                break;
            case BabyState.Vehicle:
                SetMoveRotation(moveDirection);
                rb.MovePosition(transform.position + transform.forward * speed * Time.fixedDeltaTime);
                break;
        }
    }
    public void SetMoveRotation(Transform tr)
    {
        SetMoveRotation(tr.position-this.transform.position);
    }
    public void SetMoveRotation(Vector3 direction)
    {
        rb.MoveRotation(Quaternion.LookRotation(direction, Vector3.up));
    }
    #endregion

    #region Advanced Movement
    public IEnumerator Move2Destination(Vector3 destination, System.Action onReachingDestination)
    {
        Vector3 initPosition = transform.position;
        Vector3 direction = destination - transform.position;

        float totalTime = direction.magnitude / speed;

        float elapsedTime = 0f;

        rb.MoveRotation(Quaternion.LookRotation(direction, Vector3.up));

        while (elapsedTime < totalTime)
        {
            rb.MovePosition(Vector3.Lerp(initPosition, destination, (elapsedTime / totalTime)));
            elapsedTime += Time.deltaTime;
            yield return null;
        }

        rb.MovePosition(destination);
        rb.LookAtXZ(Camera.main.transform);
        onReachingDestination?.Invoke();
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Climbable") && !climbing)
        {
            if (transform.position.y > 0.01f) return;
            climbing = true;
            float colliderHeight = collision.collider.bounds.max.y;
            transform.DOMoveY(colliderHeight, colliderHeight * 0.5f).OnComplete(() => climbing = false);
        }
    }
    #endregion
}
