using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class Swimming : MonoBehaviour
{
    private Sequence tween;
    private Vector3 rotationValue = new Vector3(0f, 180f, 0f);

    private void Start()
    {
        tween = DOTween.Sequence()
            .Append(transform.DOMoveZ(6f, 3f).OnComplete(() => transform.Rotate(rotationValue)))
            .Append(transform.DOMoveZ(3.5f, 3f).OnComplete(() => transform.Rotate(rotationValue))).SetLoops(-1);
    }

    public void Kill()
    {
        tween.Kill();
        transform.DOKill();

        StartCoroutine(PoolOutRoutine());
    }

    private IEnumerator PoolOutRoutine()
    {
        float timer = (transform.position.z - 3f) / 1.8f;
        float timeElapsed = 0f;
        transform.LookAt(PlayerController.Instance.transform);

        while (timeElapsed < timer)
        {
            transform.position += Vector3.back * Time.deltaTime * 1.8f;
            timeElapsed += Time.deltaTime;
            yield return null;
        }

        transform.DOMoveY(0f, 0f);
        ParentController.Instance.rig.SetAnimation(Const.POOL_OUT);
    }
}
