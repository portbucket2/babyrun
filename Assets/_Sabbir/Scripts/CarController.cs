using DG.Tweening;
using System.Collections;
using UnityEngine;

public class CarController : MonoBehaviour
{
    private enum StartType { Instant, Tap, Delay }
    [SerializeField] private StartType startType;
    [SerializeField] private float delayTime;
    [SerializeField] private float levelDuration;
    [SerializeField] private VariableJoystick variableJoystick;
    [SerializeField] private Rigidbody rb;
    [SerializeField] private float speed;

    private bool moving;
    private LevelManager levelManager;
    private Vector3 camForward;
    private Vector3 camRight;
    private const float sqrDeadZone = 0.0001f;
    private Vector3 moveDirection;

    private void Start()
    {
        camForward = Camera.main.transform.forward;
        camForward.y = 0f;

        camRight = Camera.main.transform.right;
        camRight.y = 0f;

        moveDirection = transform.forward;

        levelManager = LevelManager.Instance;
        StartCoroutine(StartGame());
    }

    private IEnumerator StartGame()
    {
        switch (startType)
        {
            case StartType.Instant: break;
            case StartType.Tap: yield return new WaitUntil(() => Input.GetMouseButtonDown(0)); break;
            case StartType.Delay: yield return new WaitForSeconds(delayTime); break;
        }

        levelManager.GameStarted = true;

        DOVirtual.Float(0f, 1f, levelDuration, value => LevelManager.Instance.ui.barFiller.fillAmount = value);

        yield return new WaitForSeconds(levelDuration);
        levelManager.CheckLevelState();
    }

    public void Update()
    {
        if (!levelManager.InPlayMode) return;

        Vector3 direction = camForward * variableJoystick.Vertical + camRight * variableJoystick.Horizontal;

        if (direction.sqrMagnitude > sqrDeadZone)
        {
            moveDirection = direction;
            moving = true;
        }
        else
        {
            moveDirection = Vector3.zero;
            moving = false;
        }
    }

    public void FixedUpdate()
    {
        if (!levelManager.InPlayMode) return;

        if (moving)
        {
            rb.MoveRotation(Quaternion.Lerp(transform.rotation, Quaternion.LookRotation(moveDirection, Vector3.up), 20f * Time.fixedDeltaTime));
            rb.MovePosition(transform.position + transform.forward * speed * Time.fixedDeltaTime);
        }
    }
}
