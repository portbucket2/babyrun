using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FallOnInteract : Interactable
{
    [SerializeField] private Rigidbody rb;
    [SerializeField] private GameObject activate;
    [SerializeField] private GameObject deactivate;

    public override void OnInteract()
    {
        rb.useGravity = true;
        if (activate) activate.Active();
        if (deactivate) deactivate.Inactive();
    }
}
