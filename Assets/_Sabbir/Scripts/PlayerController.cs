using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class PlayerController : MonoBehaviour
{
    public static PlayerController Instance { get; private set; }
    private LevelManager levelManager;
    private enum StartType { Instant, Tap, Delay }

    [SerializeField] internal BabyState babyState;
    [SerializeField] internal BabyRigController rig;
    [SerializeField] internal Rigidbody rb;
    [SerializeField] private StartType startType;
    [SerializeField] private float delayTime;
    [SerializeField] internal bool inputEnabled = true;

    internal MovementHandler movementHandler;
    internal bool lookAtCameraOnFail = true;
    internal bool inJumpState = false;
    internal bool moving = false;

    internal Action OnBabyMove;
    internal Action OnBabyStop;

    private void Awake()
    {
        if(Instance == null)
        {
            Instance = this;
        }
        else if (Instance != this)
        {
            Destroy(gameObject);
        }
        movementHandler = GetComponent<MovementHandler>();
    }

    void Start()
    {
        levelManager = LevelManager.Instance;
        StartCoroutine(StartGame());
        LevelManager.Instance.onLevelFinished += OnLevelFinished;
    }

    private void Update()
    {
        if(inJumpState && transform.position.y < 0.1f)
        {
            inJumpState = false;
        }
    }

    void OnLevelFinished(bool levelSuccess)
    {
        if (!levelSuccess)
        {
            Lose();
        }
        else
        {
            Win();
        }
        rig.SetBabyAnimation(Const.HOLDING, false);
    }

    private IEnumerator StartGame()
    {
        switch (startType)
        {
            case StartType.Instant: break;
            case StartType.Tap: yield return new WaitUntil(() => Input.GetMouseButtonDown(0)); break;
            case StartType.Delay: yield return new WaitForSeconds(delayTime); break;
        }

        levelManager.GameStarted = true;
    }

    public void Lose()
    {
        rig.Cry();
        if (lookAtCameraOnFail) transform.LookAt(Camera.main.transform.position.XZ());
        MakeKinematic();
    }

    public void RunForTheWin(Vector3 destination)
    {
        rig.SetBabyAnimation(Const.RUN);
        StartCoroutine(movementHandler.Move2Destination(destination,()=> {
            rig.SetBabyAnimation(Const.WIN);
        }));
    }

    public void Win()
    {
        rig.CelebrateWin();
        transform.LookAt(Camera.main.transform.position.XZ());
    }

    public void MakeKinematic()
    {
        rb.isKinematic = true;
        rb.useGravity = false;
    }

    public void CatCaught()
    {
        babyState = BabyState.Drag;
        rig.ChangeFace(BabyFace.Happy);
        rig.SetBabyAnimation(Const.DRAG);
    }


    public void SetHolding()
    {
        babyState = BabyState.Normal;
        rig.SetBabyAnimation(Const.HOLDING, true);
        rig.SetBabyAnimation(moving ? Const.RUN : Const.IDLE);
    }

    public void Jump(Vector3 target, float power, float duration)
    {
        inputEnabled = false;
        transform.DOJump(target, power, 1, duration).OnComplete(() =>
        {
            inputEnabled = true;
            inJumpState = true;
        });
    }

    public void OnMoveStart()
    {
        if (babyState != BabyState.Normal) return;

        rig.SetBabyAnimation(Const.RUN);
        rig.anim.SetBool(Const.MOVING, true);
        moving = true;
        OnBabyMove?.Invoke();
    }

    public void OnIdleStart()
    {
        if (babyState != BabyState.Normal) return;

        rig.SetBabyAnimation(Const.IDLE);
        rig.anim.SetBool(Const.MOVING, false);
        moving = false;
        OnBabyStop?.Invoke();
    }

    public void PickupBehavior(Transform lookTarget, PickType type, Action HoldItem)
    {
        rig.ChangeFace(BabyFace.Happy);
        babyState = BabyState.Interested;
        if(lookTarget)rb.LookAtXZ(lookTarget);
        switch (type)
        {
            case PickType.Standing:
                {
                    rig.SetBabyAnimation(Const.PICK);
                    FRIA.Centralizer.Add_DelayedMonoAct(this, HoldItem, 0.25f);
                } 
                break;
            case PickType.Bending:
                {
                    rig.SetBabyAnimation(Const.GROUND_PICK);
                    FRIA.Centralizer.Add_DelayedMonoAct(this, HoldItem, 0.31f);
                
                }
                break;
        }
    }

    //public void PickupBehavior(PickType type)
    //{
    //    switch (type)
    //    {
    //        case PickType.Standing: Invoke(nameof(HoldItem), 0.25f); break;
    //        case PickType.Bending: Invoke(nameof(HoldItem), 0.31f); break;
    //    }

    //    playerController.rig.ChangeFace(BabyFace.Happy);
    //    playerController.babyState = BabyState.Interested;
    //    playerController.rb.LookAtXZ(transform);
    //    playerController.rig.SetBabyAnimation(pickStateName);
    //}
    //private void HoldItem()
    //{
    //    switch (hand)
    //    {
    //        case Hand.Right: transform.SetParent(playerController.rig.rightHand, true); break;
    //        case Hand.Left: transform.SetParent(playerController.rig.leftHand, true); break;
    //    }

    //    transform.localPosition = pickingPoint.InverseTransformPointUnscaled(transform);
    //    transform.localRotation = Quaternion.Inverse(pickingPoint.rotation) * transform.rotation;
    //    playerController.babyState = BabyState.Normal;
    //    playerController.rig.SetBabyAnimation(Const.HOLDING, true);
    //    playerController.rig.SetBabyAnimation(playerController.moving ? Const.RUN : Const.IDLE);

    //    if (leftHand) leftHand.HoldItem();
    //}
}
