using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class LocalMove2Zero : MonoBehaviour
{
    [SerializeField] private float delay;
    [SerializeField] private float duration;
    [SerializeField] private Animator anim;

    private IEnumerator Start()
    {
        yield return new WaitForSeconds(delay);
        transform.DOLocalMove(Vector3.zero, duration);

        if (anim)
        {
            yield return new WaitForSeconds(duration);
            anim.CrossFadeInFixedTime(Const.STARTLE, 0.25f);
        }
    }
}
