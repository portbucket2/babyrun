using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using LionStudios.Suite.Analytics;

public class LevelManager : MonoBehaviour
{
    public Action<bool> onLevelFinished;
    public Action beforeLevelFinished;
    public static LevelManager Instance { get; private set; }

    [SerializeField] internal LevelState state = LevelState.Undecided;
    [SerializeField] internal UIHandler ui;
    [SerializeField] internal float failScreenDelay = 2f;
    [SerializeField] internal float successScreenDelay = 2f;

    internal int itemCount = 0;

    internal float totalFill;
    internal float fillFactor;


    private int currentLevelNumber;
    private bool eventSent = false;

    private bool gameStarted;
    public bool GameStarted
    {
        get => gameStarted;
        set
        {
            gameStarted = value;
            if (ui.tutorialPanel) ui.tutorialPanel.Inactive();
        }
    }
    internal bool gameEnded = false;
    public bool InPlayMode => gameStarted && !gameEnded;

    //private int coinCount;
    //public int CoinCount
    //{
    //    get => coinCount;
    //    set
    //    {
    //        coinCount = value;
    //        ui.UpdateCoinText(coinCount);
    //    }
    //}

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else if (Instance != this)
        {
            Destroy(this.gameObject);
        }

        ui.OnAwake();
    }

    private void Start()
    {
        currentLevelNumber = LevelLoader.SelectedLevelNumber;
        //CoinCount = PlayerPrefs.GetInt("coin", 0);
        ui.levelText.text = LevelLoader.Instance.GetDisplayTextForLevelNumber(currentLevelNumber);

        AnalyticsAssistant.LevelStarted(currentLevelNumber);
        fillFactor = 1f;
    }

    public void MeterFiller()
    {

        meterRoutine = StartCoroutine(MeterFillerRoutine()); 
    
    }

    Coroutine meterRoutine;
    private IEnumerator MeterFillerRoutine()
    {
        ui.objective.Inactive();
        PlayerController playerController = PlayerController.Instance;

        while (totalFill < 1f)
        {
            totalFill += Time.deltaTime * fillFactor * (playerController.moving ? ui.movingFillRate : ui.stationaryFillRate);
            ui.barFiller.fillAmount = totalFill;
            yield return null;
        }

        totalFill = 1f;
        ui.barFiller.fillAmount = totalFill;

        CheckLevelState();
    }

    public void ForceFillMeter()
    {
        totalFill = 1f;
        ui.barFiller.fillAmount = totalFill;
        CheckLevelState();
        if (meterRoutine != null) StopCoroutine(meterRoutine);
    }


    public void ReportState(LevelState reportedState)
    {
        state = reportedState;
    }


    public void CheckLevelState()
    {
        if (gameEnded) return;
        gameEnded = true;

        switch (state)
        {
            default:
            case LevelState.Success:
            case LevelState.Undecided:
                state = LevelState.Success;
                OnLevelComplete(true);
                break;
            case LevelState.Fail:
                OnLevelComplete(false);
                break;
        }
    }

    public void OnLevelComplete(bool success)
    {
        beforeLevelFinished?.Invoke();
        onLevelFinished?.Invoke(success);
        Debug.Log("levelComplete");
        if (eventSent) return;
        if (success)
        {
            EnergyTracker.ConsumeEnergy();
            FRIA.Centralizer.Add_DelayedMonoAct(this, ui.ShowCompletePanel, successScreenDelay);
            AnalyticsAssistant.LevelCompleted(currentLevelNumber);
        }
        else
        {
            FRIA.Centralizer.Add_DelayedMonoAct(this, ui.ShowFailPanel, failScreenDelay);
            AnalyticsAssistant.LevelFailed(currentLevelNumber);
        }
        eventSent = true;

    }
}
public enum LevelState { Undecided = 0, Fail = 1, Success = 2 }