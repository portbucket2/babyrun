using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class WaterFloor : MonoBehaviour
{
    [SerializeField] private Vector3 targetPoint;
    [SerializeField] private float delay;
    [SerializeField] private float duration;
    [SerializeField] private Material mat;

    private Color waterColor;

    private IEnumerator Start()
    {
        waterColor = mat.GetColor("_BaseColor");
        DOVirtual.Float(0f, 0.6f, delay, ChangeMaterialColor);
        yield return new WaitForSeconds(delay);
        transform.DOLocalMove(targetPoint, duration);
    }

    private void ChangeMaterialColor(float value)
    {
        waterColor.a = value;
        mat.SetColor("_BaseColor", waterColor);
    }
}
