using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowTransform : MonoBehaviour
{
    [SerializeField] private Vector3 offset;
    [SerializeField] private Transform target;

    void Start()
    {
        LevelManager.Instance.onLevelFinished += ActivePlayer;
        transform.Inactive();
    }

    private void ActivePlayer(bool success)
    {
        if (success)
        {
            transform.position = target.position + offset;
            transform.Active();
        }
    }
}
