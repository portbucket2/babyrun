using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BonusFill : Interactable
{
    [SerializeField] private float bonusFillValue = 0.1f;

    public override void OnInteract()
    {
        LevelManager.Instance.totalFill += bonusFillValue;
    }
}
