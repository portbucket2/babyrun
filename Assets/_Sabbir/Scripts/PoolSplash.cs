using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PoolSplash : MonoBehaviour
{
    [SerializeField] private ParticleSystem particle;

    private void OnCollisionEnter(Collision collision)
    {
        if (!collision.gameObject.CompareTag(Const.PLAYER)) return;

        if (!particle.isPlaying)
        {
            particle.Play();
        }
    }
}
