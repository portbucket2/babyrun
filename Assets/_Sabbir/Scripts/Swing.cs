using UnityEngine;
using DG.Tweening;

public class Swing : MonoBehaviour
{
    [SerializeField] private float duration = 0.5f;
    [SerializeField] private float distance = 0.8f;

    private void Start() => transform.DOLocalMoveX(distance, duration).SetLoops(-1, LoopType.Yoyo);
}
