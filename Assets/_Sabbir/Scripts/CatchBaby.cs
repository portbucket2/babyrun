using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CatchBaby : Interactable
{
    [SerializeField] private Transform baby;
    [SerializeField] private Transform parent;
    [SerializeField] private Transform parentHand;
    [SerializeField] private Animator parentAnim;
    [SerializeField] private MonoBehaviour[] monoBehaviours;

    public override void OnInteract()
    {
        StartCoroutine(EndRoutine());
    }

    private IEnumerator EndRoutine()
    {
        foreach (var item in monoBehaviours) item.enabled = false;
        PlayerController.Instance.inputEnabled = false;
        parentAnim.CrossFadeInFixedTime(Const.CHASING, 0.25f);

        Vector3 targetDirection = baby.position - parent.position;

        while (targetDirection.sqrMagnitude > 0.1f)
        {
            targetDirection = baby.position - parent.position;
            parent.rotation = Quaternion.Slerp(parent.rotation, Quaternion.LookRotation(targetDirection, Vector3.up), 3f * Time.deltaTime);
            parent.Translate(parent.forward * Time.deltaTime * 2f, Space.World);
            yield return null;
        }

        PlayerController.Instance.MakeKinematic();
        LevelManager.Instance.ForceFillMeter();

        baby.SetParent(parentHand, true);
        baby.SetPositionAndRotation(parentHand.position, parentHand.rotation);

      

        parentAnim.CrossFadeInFixedTime(Const.SUCCESS, 0.25f);

        float elapsedTime = 0.0f;
        Vector3 direction = (Camera.main.transform.position.XZ() - transform.position).normalized;

        while (elapsedTime < 1.5f)
        {
            parent.Translate(direction * Time.deltaTime, Space.World);
            parent.rotation = Quaternion.Slerp(parent.rotation, Quaternion.LookRotation(direction, Vector3.up), 3f * Time.deltaTime);
            elapsedTime += Time.deltaTime;
            yield return null;
        }

        parentAnim.enabled = false;
    }
}
