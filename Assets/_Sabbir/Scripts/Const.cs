public enum StartAnimation
{
    idle_standing,
    idle_sitting_pc,
    idle_sitting_sofa,
    idle_sitting_painting,
    idle_ironing, walk_cart,
    swimming,
    idle_sitting_poop,
}

public static class Const
{
    public const string ANNOYED = "annoyed";
    public const string CAUGHT = "caught";
    public const string CHASING = "chasing";
    public const string CLAP = "clap";
    public const string CRY = "cry";
    public const string DRAG = "drag";
    public const string DRINK = "drink";
    public const string ENRAGED = "enraged";
    public const string FAIL = "fail";
    public const string FLOATING_SCARE = "floating_scare";
    public const string GROUND_PICK = "ground_pick";
    public const string HAPPY = "happy";
    public const string HIDE_BOOBS = "hide_boobs";
    public const string HOLDING = "agroholding";
    public const string IDLE = "idle_standing";
    public const string IDLE_SITTING_PAINTING = "idle_sitting_painting";
    public const string IDLE_SITTING_PC = "idle_sitting_pc";
    public const string IDLE_SITTING_POOP = "idle_sitting_poop";
    public const string IDLE_SITTING_SOFA = "idle_sitting_sofa";
    public const string JUMP = "jump";
    public const string MOVING = "moving";
    public const string NEXT = "next";
    public const string OPEN = "open";
    public const string PARENT = "Parent";
    public const string PICK = "pick";
    public const string PICK_IDLE = "pick_idle";
    public const string PLAYER = "Player";
    public const string POOL_OUT = "pool_out";
    public const string RUN = "run";
    public const string SAD = "sad";
    public const string SHAKE = "shake";
    public const string SHY = "shy";
    public const string SIT = "sit";
    public const string SLAP = "slap";
    public const string SLIP = "slip";
    public const string STARTLE = "omg";
    public const string SUCCESS = "success";
    public const string THREAT = "threat";
    public const string THROW = "throw";
    public const string WAKEUP = "wakeup";
    public const string WALK = "walk";
    public const string WALK_BABY = "walk_baby";
    public const string WALK_CART = "walk_cart";
    public const string WALK_MOBILE = "walk_mobile";
    public const string WIN = "win";
}
