using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelEndPoint : Interactable
{
    [SerializeField] internal Transform leftHand;
    [SerializeField] internal Transform pickingPoint;
    //[SerializeField] internal Rigidbody rb;
    //[SerializeField] private Transform throwPoint;
    //[SerializeField] private float bonusFill;
    public override void OnInteract()
    {
        PickableItem tr = PickableItem.currentItem;
        //LevelManager.Instance.OnLevelComplete(throwPoint.position);
        LevelManager.Instance.totalFill = 1f;
        tr.transform.SetParent(leftHand, true);

        tr.transform.localPosition = tr.pickingPoint.InverseTransformPointUnscaled(tr.transform);
        tr.transform.localRotation = Quaternion.Inverse(tr.pickingPoint.rotation) * tr.transform.rotation;
    }
}
