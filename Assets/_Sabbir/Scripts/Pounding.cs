using UnityEngine;
using DG.Tweening;

public class Pounding : MonoBehaviour
{
    [SerializeField] private float duration = 0.5f;
    [SerializeField] private float size = 0.8f;

    private void Start() => transform.DOScale(size, duration).SetLoops(-1, LoopType.Yoyo);
}
