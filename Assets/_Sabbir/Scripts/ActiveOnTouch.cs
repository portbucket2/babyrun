using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActiveOnTouch : Interactable
{
    [SerializeField] private GameObject activeItem;
    [SerializeField] private GameObject inactiveItem;
    [SerializeField] private bool kinemetic;

    public override void OnInteract()
    {
        activeItem.Active();
        if(kinemetic) activeItem.GetComponent<Rigidbody>().isKinematic = false;
        if(inactiveItem) inactiveItem.Inactive();

        if (ParentController.Instance.chasing) return;

        LevelManager.Instance.MeterFiller();
        PlayerController.Instance.rig.ChangeFace(BabyFace.Happy);
        ParentController.Instance.GetStartle();
    }
}
