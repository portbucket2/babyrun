using UnityEngine;
using Cinemachine;
using DG.Tweening;

public class ZoomCamera : MonoBehaviour
{
    [SerializeField] private CinemachineVirtualCamera vCam;
    [SerializeField] private float targetFOV;
    [SerializeField] private float duration;

    void Start()
    {
        float currentFOV = vCam.m_Lens.FieldOfView;
        DOVirtual.Float(currentFOV, targetFOV, duration, value => vCam.m_Lens.FieldOfView = value).SetEase(Ease.OutQuad);
    }
}
