using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PathCreation;

public class CartMover : CartLevel
{
    protected override void OnStart()
    {
        base.OnStart();
        StartCoroutine(StartGame());
    }

    void Update()
    {
        if (levelManager.InPlayMode) UpdateDistance();
    }

    private IEnumerator StartGame() 
    {
        yield return StartControl();
        parentController.rig.PlayAnimation(Const.WALK_CART);

        yield return new WaitForSeconds(levelDuration);
        CheckLevelEnd();
    }

    private void CheckLevelEnd()
    {
        if (levelManager.itemCount <= 2)
        {
            levelManager.ReportState(LevelState.Fail);
            parentController.rig.PlayAnimation(Const.IDLE);
        }
        else
        {
            parentController.transform.LookAt(playerController.transform.position.XZ());
            parentController.rig.PlayAnimation(Const.FAIL);
        }

        levelManager.CheckLevelState();
    }
}
