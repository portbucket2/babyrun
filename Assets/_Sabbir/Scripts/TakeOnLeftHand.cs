using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class TakeOnLeftHand : MonoBehaviour
{
    [SerializeField] private Collider itemCollider;
    [SerializeField] private Rigidbody rb;
    [SerializeField] private Transform pickingPoint;

    public void OnInteract()
    {
        PickupBehavior();
    }

    public void PickupBehavior()
    {

        playerController.babyState = BabyState.Interested;
        playerController.rb.LookAtXZ(transform);
        playerController.rig.SetBabyAnimation(pickStateName);
        Invoke(nameof(HoldItem), pickingDelay);
    }

    public void DropBehavior()
    {
        playerController.babyState = BabyState.Interested;
        playerController.rig.SetBabyAnimation(pickStateName);
    }

    public void HoldItem()
    {
        transform.SetParent(playerController.rig.leftHand, true);

        transform.localPosition = pickingPoint.InverseTransformPointUnscaled(transform);
        transform.localRotation = Quaternion.Inverse(pickingPoint.rotation) * transform.rotation;
        playerController.babyState = BabyState.Normal;
        playerController.rig.SetBabyAnimation(Const.HOLDING, true);
        playerController.rig.SetBabyAnimation(playerController.moving ? Const.RUN : Const.IDLE);
    }

    #region Drop
    public void DropItem()
    {
        Drop();
        rb.AddForce(1f * Vector3.up, ForceMode.Impulse);
    }

    public void DropItem(Vector3 target, float angle)
    {
        Drop();
        rb.Throw(target, angle);
    }

    private void Drop()
    {
        PickableItem.currentItem = null;
        transform.parent = null;
        rb.isKinematic = false;
        rb.useGravity = true;
    }
    #endregion

    #region OnStart
    private PlayerController playerController;
    private ParentController parentController;
    private LevelManager levelManager;

    private static string pickStateName;

    private float pickingDelay;
    private enum PickType { Standing, Bending }
    [SerializeField] private PickType pickType;

    public void Start()
    {
        playerController = PlayerController.Instance;
        parentController = ParentController.Instance;
        levelManager = LevelManager.Instance;

        pickingDelay = pickType == PickType.Standing ? 0.25f : 0.31f;
        pickStateName = pickType == PickType.Standing ? Const.PICK : Const.GROUND_PICK;
    }
    #endregion
}
