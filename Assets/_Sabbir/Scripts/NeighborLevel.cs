using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NeighborLevel : Interactable
{
    [SerializeField] private Animator anim;

    public override void OnInteract()
    {
        anim.CrossFadeInFixedTime(Const.SAD, 0.25f);
        ParentController.Instance.rig.SetAnimation(Const.THREAT);
        LevelManager.Instance.totalFill += 1f;
    }
}
