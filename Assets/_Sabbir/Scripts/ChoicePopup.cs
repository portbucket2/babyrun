using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChoicePopup : Interactable
{
    public ChoiceLevelUI choiceScreen;
    public override void OnInteract()
    {
        PlayerController.Instance.inputEnabled = false;
        PlayerController.Instance.OnIdleStart();
        choiceScreen.ShowChoicePopup();
    }
}
