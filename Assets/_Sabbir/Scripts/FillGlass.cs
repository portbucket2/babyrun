using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class FillGlass : Interactable
{
    [SerializeField] private GameObject filler;

    PlayerController playerController;

    public override void OnStart()
    {
        playerController = PlayerController.Instance;
    }

    public override void OnInteract()
    {
        if (!PickableItem.currentItem) return;
        playerController.rig.SetBabyAnimation(Const.GROUND_PICK);
        StartCoroutine(DelayStart());
    }

    private IEnumerator DelayStart()
    {
        yield return new WaitForSeconds(0.2f);
        playerController.rig.SetBabyAnimation(playerController.moving ? Const.RUN : Const.IDLE);
        filler.SetActive(true);
    }
}
