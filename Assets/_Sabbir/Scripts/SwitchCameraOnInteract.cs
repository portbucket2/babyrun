using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;
using DG.Tweening;

public class SwitchCameraOnInteract : Interactable
{
    [SerializeField] private CinemachineVirtualCamera[] vCams;
    [SerializeField] private GameObject activate;
    [SerializeField] private float delay;

    internal bool startPaint;

    public override void OnInteract()
    {
        startPaint = true;
        SwitchCam(1, 2);
        PlayerController.Instance.inputEnabled = false;

        if (activate) StartCoroutine(DelayActive());
    }

    public void SwitchCam(int from, int to)
    {
        int priority = vCams[to - 1].Priority;
        vCams[to - 1].Priority = vCams[from - 1].Priority;
        vCams[from - 1].Priority = priority;
    }

    private IEnumerator DelayActive()
    {
        yield return new WaitForSeconds(delay);
        activate.Active();
        activate.transform.DOScale(1f, 0.25f);
    }
}
