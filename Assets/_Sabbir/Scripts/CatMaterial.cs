using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class CatMaterial : MonoBehaviour
{
    [SerializeField] private SkinnedMeshRenderer rend;
    [SerializeField] private Material currentMat;
    [SerializeField] private Material targetMat;
    [SerializeField] private float duration;
    [SerializeField] private Animator anim;
    [SerializeField] private GameObject indicator;

    private bool catRun = false;

    private void OnTriggerEnter(Collider other)
    {
        if (!indicator.activeSelf) return;

        if (other.CompareTag(Const.PLAYER))
        {
            if(!catRun)
            {
                rend.material = targetMat;
                catRun = true;
                anim.Play(Const.RUN);
                transform.DOMoveZ(10f, 5f);
                indicator.Inactive();
            }
        }
    }
}
