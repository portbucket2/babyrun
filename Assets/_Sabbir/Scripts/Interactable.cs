using UnityEngine;
using DG.Tweening;

public class Interactable : MonoBehaviour
{
    [Header("Interactable")]
    [SerializeField] internal bool interactable = true;
    [SerializeField] internal Collider interactCollider;
    private enum TriggerType { Enter = 0, Stay = 1 }
    [SerializeField] private TriggerType triggerType;
    [SerializeField] internal GameObject indicator;
    [SerializeField] internal GameObject activeOnInteract;
    [SerializeField] internal ParticleSystem particle;
    [SerializeField] internal float particleDelay;
    private void Start() => OnStart();
    public virtual void OnStart() { }
    public virtual void OnInteract() { }

    private void OnTriggerEnter(Collider other)
    {
        if (!interactable || triggerType == TriggerType.Stay || !LevelManager.Instance.InPlayMode) return;
        if (other.CompareTag(Const.PLAYER)) OnTrigger();
    }

    private void OnTriggerStay(Collider other)
    {
        if (!interactable || triggerType == TriggerType.Enter || !LevelManager.Instance.InPlayMode) return;
        if (other.CompareTag(Const.PLAYER) && ( Input.GetMouseButton(0))) OnTrigger();
    }

    private void OnTrigger()
    {
        if (activeOnInteract) activeOnInteract.Active();
        if (particle) DOVirtual.DelayedCall(particleDelay, particle.Play, false);
        if (interactCollider) interactCollider.enabled = false;
        OnInteract();
        LevelManager.Instance.itemCount++;
        interactable = false;
        if (indicator) indicator.SetActive(false);
    }
}
