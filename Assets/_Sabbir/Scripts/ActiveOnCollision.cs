using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActiveOnCollision : MonoBehaviour
{
    [SerializeField] private GameObject activate;
    [SerializeField] private GameObject deactivate;

    private bool interactable = true;
    private Rigidbody rb;

    private void Awake()
    {
        rb = GetComponent<Rigidbody>();
    }
    private void OnCollisionEnter(Collision collision)
    {
        if (!interactable || !collision.gameObject.CompareTag(Const.PLAYER)) return;

        interactable = false;
        rb.isKinematic = false;
        if (activate) activate.Active();
        if (deactivate) deactivate.Inactive();
    }
}
