using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PathCreation;
using DG.Tweening;

public class CartLevel : MonoBehaviour
{
    [SerializeField] protected PathCreator cartPath;
    [SerializeField] protected float levelDuration;
    [SerializeField] protected Transform pickingPoint;
    protected enum StartType { Instant, Tap, Delay }
    [SerializeField] protected StartType startType;
    [SerializeField] protected float delayTime;

    protected LevelManager levelManager;
    protected PlayerController playerController;
    protected ParentController parentController;

    private float distanceTravelled = 0;
    private float speed;
    private EndOfPathInstruction endOfPath = EndOfPathInstruction.Stop;

    void Start() => OnStart();

    protected virtual void OnStart()
    {
        speed = 1f / levelDuration;
        levelManager = LevelManager.Instance;
        playerController = PlayerController.Instance;
        parentController = ParentController.Instance;
    }

    protected IEnumerator StartControl()
    {
        switch (startType)
        {
            case StartType.Instant: break;
            case StartType.Tap: yield return new WaitUntil(() => Input.GetMouseButtonDown(0)); break;
            case StartType.Delay: yield return new WaitForSeconds(delayTime); break;
        }

        levelManager.GameStarted = true;
        DOVirtual.Float(0f, 1f, levelDuration, value => LevelManager.Instance.ui.barFiller.fillAmount = value);
    }

    protected void UpdateDistance()
    {
        distanceTravelled += Time.deltaTime * speed;
        Vector3 position = cartPath.path.GetPointAtTime(distanceTravelled, endOfPath);
        Quaternion rotation = cartPath.path.GetRotation(distanceTravelled, endOfPath);
        transform.SetPositionAndRotation(position, rotation);
    }
}
