using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColorOnCanvas : MonoBehaviour
{
    [SerializeField] private LayerMask layer;
    [SerializeField] private Trail trail;

    private void OnTriggerStay(Collider other)
    {
        if (other.gameObject.layer == layer)
        {
            Vector3 target = transform.position;
            target.y += 0.3f;
            trail.pointOnSide = other.ClosestPoint(target);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.layer == layer)
        {
            trail.paintSide = true;
            trail.norm = other.transform.right;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.layer == layer)
        {
            trail.paintSide = false;
        }
    }
}
