using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class CatPeeing : Task
{
    [SerializeField] private Animator catAnim;
    [SerializeField] private Animator dogAnim;
    [SerializeField] private ParticleSystem particle;

    public override void OnInteract()
    {
        LevelManager.Instance.MeterFiller();
        PlayerController.Instance.inputEnabled = false;
        PlayerController.Instance.transform.GetComponent<MovementHandler>().enabled = false;
        StartCoroutine(PeeRoutine());
    }

    private IEnumerator PeeRoutine()
    {
        catAnim.CrossFadeInFixedTime("pissing", 0.25f);
        PlayerController.Instance.transform.DORotate(new Vector3(0f, 115f, 0f), 0.25f);
        yield return new WaitForSeconds(0.5f);
        particle.Play();
    }

    public override void OnStart()
    {
        LevelManager.Instance.onLevelFinished += OnLevelFinished;
    }

    private void OnLevelFinished(bool success)
    {
        dogAnim.CrossFadeInFixedTime("bark", 0.25f);
    }
}
