﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class WaterHoseController : MonoBehaviour
{
    [SerializeField] private PipeBulger bulger;
    [SerializeField] private SprayController spray;
    [SerializeField] private Animator gardener;
    [SerializeField] private Image filler;
    [SerializeField] private GameObject indicator;

    [SerializeField] private float clogMaxValue = 10;
    [SerializeField] private float clogRate = 25;
    [SerializeField] private float unclogRate = 20;
    [SerializeField] private ParticleSystem waterSplash;
    [SerializeField] private ParticleSystem exclemParticle;
    [SerializeField] private Vector3 offset;

    private Transform player;
    private float potentialClog;
    private bool clog = false;
    private bool maxReached = false;

    private bool clogStarted = false;

    private void Update()
    {
        potentialClog += ((clog ? 1 : 0) * clogRate - unclogRate) * Time.deltaTime;

        if(!clogStarted && potentialClog > 0f)
        {
            clogStarted = true;
            PlayerController.Instance.inputEnabled = false;
            PlayerController.Instance.rig.SetBabyAnimation(Const.GROUND_PICK);
            if (indicator) indicator.Inactive();
        }

        if (potentialClog > clogMaxValue)
        {
            if (maxReached) return;
            maxReached = true;

            gardener.CrossFadeInFixedTime("check", 0.25f);
            exclemParticle.Play();
            DOVirtual.DelayedCall(1f, Fail, false);

        }
        else if (potentialClog < 0)
        {
            potentialClog = 0;
        }

        float fill = potentialClog / clogMaxValue;

        if(!maxReached) filler.fillAmount = fill;

        bulger.bulgeScale = (fill) * 5f;

        spray.SetSprayForce((1 - fill) * spray.emissionRate * (maxReached ? 15 : 1), (1 - fill) * spray.emissionAngle * (maxReached ? 3 : 1));

        if (player)
        {
            Vector3 relativeTrackPosition = bulger.transform.InverseTransformPoint(player.position);
            bulger.transform.Translate(0, 0, relativeTrackPosition.z, Space.Self);
            waterSplash.transform.localPosition = bulger.transform.localPosition + offset;
        }
    }

    private void Fail()
    {
        gardener.CrossFadeInFixedTime("fall", 0.25f);
        LevelManager.Instance.ForceFillMeter();
        exclemParticle.Stop();
        spray.gameObject.AddComponent<Rigidbody>();
        Camera.main.GetComponent<SwitchVirtualCamera>().SwitchCam(1, 2);
    }

    private IEnumerator AnimationRoutine()
    {
        yield return new WaitForSeconds(0.5f);
    }


    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag(Const.PLAYER))
        {
            clog = true;
            player = other.attachedRigidbody.transform;
            waterSplash.Play();
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag(Const.PLAYER))
        {
            clog = false;
            player = null;
            waterSplash.Stop();
        }
    }
}
