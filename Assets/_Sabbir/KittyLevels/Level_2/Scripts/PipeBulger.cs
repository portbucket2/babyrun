﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PipeBulger : MonoBehaviour
{
    [SerializeField] private MeshFilter meshFilter;
    [SerializeField] private AnimationCurve bulgeCurve;

    public float affectedWake = 1;
    public float bulgeScale = 1;

    private Vector3[] initverts;
    private Mesh mesh;

    private void Start()
    {
        mesh = meshFilter.mesh;
        initverts = new Vector3[mesh.vertices.Length];
        initverts = mesh.vertices;
        for (int i = 0; i < initverts.Length; i++)
        {
            initverts[i] = mesh.vertices[i];
        }
    }

    private void Update()
    {
        OperateBulge();
    }

    private void OperateBulge()
    {
        Vector3[] verts = new Vector3[initverts.Length];
        Vector3[] transvertsVerts = new Vector3[initverts.Length];

        for (int i = 0; i < initverts.Length; i++)
        {
            verts[i] = transform.InverseTransformPoint(meshFilter.transform.TransformPoint(initverts[i]));

            Vector2 vec2 = (new Vector2(verts[i].x, verts[i].y)) * (1 + ((verts[i].z > 0 || verts[i].z < (-affectedWake)) ? 0 : bulgeCurve.Evaluate((-verts[i].z) / (affectedWake)) * bulgeScale));

            verts[i] = new Vector3(vec2.x, vec2.y, verts[i].z);

            transvertsVerts[i] = meshFilter.transform.InverseTransformPoint(transform.TransformPoint(verts[i]));
        }

        mesh.vertices = transvertsVerts;
    }
}
