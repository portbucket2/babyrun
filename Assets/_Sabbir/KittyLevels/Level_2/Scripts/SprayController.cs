﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SprayController : MonoBehaviour
{
    internal float emissionRate;
    internal float emissionAngle;

    private ParticleSystem particle;
    private ParticleSystem.EmissionModule emission;
    private ParticleSystem.ShapeModule shape;

    void Start()
    {
        particle = GetComponent<ParticleSystem>();
        emission = particle.emission;
        shape = particle.shape;
        emissionRate = emission.rateOverTime.constant;
        emissionAngle = shape.angle;
    }

    public void SetSprayForce(float sprayRate, float sprayAngle)
    {
        emission.rateOverTime = new ParticleSystem.MinMaxCurve(sprayRate);
        shape.angle = sprayAngle;
    }
}
