﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[RequireComponent(typeof(Rigidbody))]
public class Throwable : MonoBehaviour
{
    [SerializeField] private float force = 1f;
    [SerializeField] private ParticleSystem particle;
    [SerializeField] private UnityEvent endEvent;

    private Rigidbody itemRigidbody;
    private Collider itemCollider;

    private bool interactable = true;

    private void Start()
    {
        itemRigidbody = GetComponent<Rigidbody>();
        itemCollider = GetComponent<Collider>();
    }


    private void OnTriggerEnter(Collider other)
    {
        if (interactable && other.CompareTag(Const.PLAYER))
        {
            Throw();
        }
    }

    public void Throw()
    {
        interactable = false;
        itemCollider.isTrigger = false;
        itemRigidbody.isKinematic = false;
        itemRigidbody.useGravity = true;
        itemRigidbody.AddForce(transform.forward * force, ForceMode.Impulse);

        if (particle) particle.Play();
        endEvent?.Invoke();
    }

    


}
