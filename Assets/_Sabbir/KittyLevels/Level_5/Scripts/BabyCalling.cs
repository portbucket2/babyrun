using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BabyCalling : MonoBehaviour
{
    [SerializeField] private Vector3 offset;
    [SerializeField] private Transform target;

    void Start()
    {
        LevelManager.Instance.onLevelFinished += ActivePlayer;
        transform.Inactive();
    }

    private void Update()
    {
        transform.position = target.position + offset;
    }

    private void ActivePlayer(bool success)
    {
        if (success)
        {
            transform.LookAt(Camera.main.transform);
            transform.Active();
            Destroy(gameObject, 2f);
        }
    }
}
