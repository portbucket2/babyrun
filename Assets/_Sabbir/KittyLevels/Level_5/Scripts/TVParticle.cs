using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class TVParticle : MonoBehaviour
{
    [SerializeField] private Material mat;

    private Color matColor;

    void Start()
    {
        matColor = mat.color;
    }

    // Update is called once per frame
    void Update()
    {
        matColor.a = 0.5f + Mathf.PingPong(Time.time, 0.5f);
        mat.SetColor("_Color", matColor);
    }
}
