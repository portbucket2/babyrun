using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEngine.Events;

public class TextureColor : MonoBehaviour
{
	[SerializeField] private Camera canvasCam;
	[SerializeField] private Transform brushContainer;
	[SerializeField] private GameObject brushPrefab;
	[SerializeField] private Color[] brushColors;
	[SerializeField] private float brushSize = 1.0f;
	[SerializeField] private LayerMask paintLayer;
	[SerializeField] private float duration;
	[SerializeField] private SwitchCameraOnInteract cam;
	[SerializeField] private GameObject deactiveOnComplete;
	[SerializeField] private Animator anim;
	[SerializeField] private GameObject coin;
	[SerializeField] private UnityEvent onColorComplete;

	private Camera sceneCamera;
	private Vector2 texCoord = Vector2.zero;
	private Color brushColor;
	private float orthographicSize;
	private int brushCounter = 0;
    private readonly int maxBrushCount = 3000;
	private bool taskCompleted = false;
	internal Tween bar;

	private float timer = 0f;

	private void Start()
	{
		sceneCamera = Camera.main;
		brushColor = brushColors[0];
		orthographicSize = canvasCam.orthographicSize;
		StartCoroutine(Wait());
	}

	private IEnumerator Wait()
    {
		yield return new WaitUntil(() => cam.startPaint);

		bar = DOVirtual.Float(0f, 1f, duration, value => LevelManager.Instance.ui.barFiller.fillAmount = value).OnComplete(() =>
		{
			onColorComplete?.Invoke();
			cam.SwitchCam(2, 1);
			if (deactiveOnComplete) deactiveOnComplete.Inactive();
			LevelManager.Instance.ForceFillMeter();
			anim.CrossFadeInFixedTime(Const.WAKEUP, 0.25f);
			cam.startPaint = false;
		});
		bar.timeScale = 0f;
	}

    private void Update()
    {
		if (!cam.startPaint) return;

		if(Input.GetMouseButtonDown(0))
        {
			bar.timeScale = 1f;
        }

		if (Input.GetMouseButtonUp(0))
		{
			bar.timeScale = 0f;
		}

		if (Input.GetMouseButton(0))
        {
			Paint();
        }
    }

	public void SelectBrushColor(int index)
    {
		brushColor = brushColors[index];
    }

    public void Paint()
	{
		if (taskCompleted) return;

		timer += Time.deltaTime;
		if (timer > 0.5f)
		{
			timer = 0f;
			if (Physics.Raycast(sceneCamera.ScreenPointToRay(Input.mousePosition), out RaycastHit hitInfo, 100f, paintLayer))
			{
				Instantiate(coin, hitInfo.point, Quaternion.identity);
			}
		}


		Vector3 uvWorldPosition = Vector3.zero;

		if (HitTestUVPosition(ref uvWorldPosition))
		{

			GameObject brushObj = Instantiate(brushPrefab, brushContainer);
			brushObj.GetComponent<SpriteRenderer>().color = brushColor;
			brushObj.transform.localPosition = uvWorldPosition;
			brushObj.transform.localScale = Vector3.one * brushSize;
			brushCounter++;
			
		}

		if (brushCounter >= maxBrushCount)
        {
			taskCompleted = true;
        }
	}

	private bool HitTestUVPosition(ref Vector3 uvWorldPosition)
	{
		if (Physics.Raycast(sceneCamera.ScreenPointToRay(Input.mousePosition), out RaycastHit hitInfo, 100f, paintLayer))
		{
			if ((hitInfo.textureCoord - texCoord).sqrMagnitude < 0.0001f) return false;

			texCoord = hitInfo.textureCoord;
			uvWorldPosition.x = texCoord.x - orthographicSize;
			uvWorldPosition.y = texCoord.y - orthographicSize;
			uvWorldPosition.z = 0.0f;
			return true;
		}
		else
		{
			return false;
		}
	}
}
