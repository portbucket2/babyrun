﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotatingRays : MonoBehaviour
{
    public float speed=90;

    // Update is called once per frame
    void Update()
    {
        transform.Rotate(transform.forward, speed * Time.deltaTime, Space.World);
    }
}
