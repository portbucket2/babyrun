using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FRIA;

public class Annoy_MaterialFillByTurns : MonoBehaviour
{
    public List<GameObject> mermaidMeshes;
    public float stepCompletionPoint = 1.4f;
    public float stepLength = 1.5f;

    public AdultStateController adult;
    public Transform playerLookTarget;
    public ParticleSystem sandDropParticle;

    public void OnPick()
    {
        this.Add_DelayedAct(() =>
        {
            PlayerController.Instance.inputEnabled = false;
            PlayerController.Instance.moving = false;
            PlayerController.Instance.rig.anim.SetBool("moving",false);
            PlayerController.Instance.rig.SetBabyAnimation("pick idle");
        }, 0.2f);
    }

    void Start()
    {
        adult.rig.anim.SetLayerWeight(1,0);
        AnnoyanceController.Instance.onAnnoyanceChanged += (float v)=> 
        {
            if (v >= 1)
            {
                active = false;
                StopAllCoroutines();

                foreach (var item in mermaidMeshes)
                {
                    item.SetActive(false);
                    ParticleSystem ps = item.GetComponentInChildren<ParticleSystem>();
                    if (ps)
                    {
                        ps.transform.SetParent(null);
                        ps.Play();
                    }
                }
            }


        };
        for (int i = 0; i < mermaidMeshes.Count; i++)
        {
            mermaidMeshes[i].SetActive(false);
        }
        AnnoyanceController.Instance.onInitiated += () =>
        {
            PlayerController.Instance.inputEnabled = false;
            PlayerController.Instance.OnIdleStart();
            PlayerController.Instance.rig.ChangeFace(BabyFace.Naughty);
            PlayerController.Instance.MakeKinematic();
            PlayerController.Instance.transform.LookAt(playerLookTarget,Vector3.up);
            //PlayerController.Instance.movementHandler.SetMoveRotation(playerLookTarget);
            active = true;
        };
    }
    bool active = false;

    bool working = false;
    float stepProgress = 0;
    bool stepAwarded = false;
    int stepIndex = 0;
    private void Update()
    {
        if (!active) return;

        if (Input.GetMouseButton(0))
        {
            if (!working)
            {
                working = true;
                PlayerController.Instance.rig.anim.CrossFadeInFixedTime("shovel",0);
            }
            stepProgress += Time.deltaTime;
        }
        else
        {
            if (working)
            {
                ResetStep();
            }
        }
        if (stepProgress >= stepCompletionPoint&& !stepAwarded)
        {
            stepAwarded = true;
            OnProgressStep(stepIndex);
        }
        if (stepProgress >= stepLength)
        {
            ResetStep();
        }
    }
    void ResetStep()
    {
        working = false;
        stepProgress = 0;
        PlayerController.Instance.rig.SetBabyAnimation("pick idle");
        if (stepAwarded)
        {
            stepAwarded = false;
            OnStepCompleted(stepIndex);
            stepIndex++;
        }
    }

    void OnProgressStep(int step)
    {
        if (step == mermaidMeshes.Count - 1)
        {
            adult.rig.anim.SetLayerWeight(1, 1);
        }
        
        SetMesh(stepIndex, disableOthers: (step == mermaidMeshes.Count - 1));
    }
    void OnStepCompleted(int step)
    {
        int stepCount = mermaidMeshes.Count;
        float progress =  (step+1)*1.0f/ stepCount;
        //(progress.ToString()).Debug("FF00FF");
        if ((progress - targetP) > 0)
        {
            //float T = (progress - p) * stepCount*stepLength;
            //(T.ToString()).Debug("FF00FF");
            if (barFiller != null) StopCoroutine(barFiller);
            barFiller = StartCoroutine(BarFillRoutine(FILL_TIME, progress));
        }
        if (step == mermaidMeshes.Count - 1)
        {
            active = false;
            Centralizer.Add_DelayedMonoAct(this, LevelManager.Instance.ForceFillMeter, FILL_TIME);;
        }
    }

    public const float FILL_TIME = 0.25f;

    void SetMesh(int step, bool disableOthers)
    {
        for (int i = 0; i < mermaidMeshes.Count; i++)
        {
            if (i == step)
            {
                mermaidMeshes[i].SetActive(true);
                ParticleSystem ps = mermaidMeshes[i].GetComponentInChildren<ParticleSystem>();
                if (ps)
                {
                    ps.Play();
                }
            }
            else if (disableOthers)
            {
                mermaidMeshes[i].SetActive(false);
            }
        }
    }


    Coroutine barFiller;
    float p;
    float targetP;
    IEnumerator BarFillRoutine(float T, float targetValue)
    {
        float sT = Time.time;
        float startingP = p;
        targetP = targetValue;
        while (Time.time < sT + T)
        {
            float routineProg = Mathf.Clamp01((Time.time - sT) / T);
            p = Mathf.Lerp(startingP, targetP, routineProg);
            LevelManager.Instance.ui.barFiller.fillAmount = p;
            yield return null;
        }
        p = targetP;
        LevelManager.Instance.ui.barFiller.fillAmount = p;
    }
}
