using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlierManage_54 : MonoBehaviour
{
    public Transform visualItemRoot;
    public Animator animl;
    public float hitCountTarget = 3;

    private void Start()
    {
        inFlight = false;
    }
    public void StartFlight()
    {
        visualItemRoot.gameObject.SetActive(true);
        animl.SetTrigger("reset");
        inFlight = true;
    }
    public static bool inFlight;
    public void EndFlight()
    {
        visualItemRoot.gameObject.SetActive(false);
        hitCount++;

        LevelManager.Instance.ui.barFiller.fillAmount = hitCount / hitCountTarget;
        if (hitCount >= hitCountTarget)
        {
            LevelManager.Instance.ForceFillMeter();
        }
        
        inFlight = false;
    }
    int hitCount;

}
