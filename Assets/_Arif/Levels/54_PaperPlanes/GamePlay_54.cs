using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FRIA;

public class GamePlay_54 : MonoBehaviour
{
    public Transform sitTargetPoint;
    public float sitTime = 0.25f;
    public string babySitAnimID = "idle_sitting";
    public GameObject planeItemPref;
    public float postPickDelay;
    public FlierManage_54 flier;
    public AdulTeacher_54 adult;

    public GameObject pickTut;
    public GameObject throwTut;

    private void Start()
    {
        LevelManager.Instance.onLevelFinished += (bool success) =>
        {
            PlayerController.Instance.rig.anim.SetLayerWeight(1, 0);

            CamSwapController.SetCamera(success ? CamID.celebration : CamID.deathcam);
        };
    }

    public void SitTheBaby()
    {
        PlayerController.Instance.babyState = BabyState.Interested;
        PlayerController.Instance.inputEnabled = false;
        PlayerController.Instance.rig.SetBabyAnimation(babySitAnimID);
        StartCoroutine(PlayerController.Instance.transform.ReTransform(sitTargetPoint, sitTime, onComplete: ()=>{
            PlayerController.Instance.rig.anim.SetLayerWeight(1,1);
            //PlayerController.Instance.rig.anim.SetLayerWeight(2, 1);
            babyisSitting = true;
            PlayerController.Instance.GetComponent<Rigidbody>().isKinematic = true;
            StartCoroutine(SittingRoutine());
        }));
    }

    bool babyisSitting;

    bool hasPlane;
    private void Update()
    {
        pickTut.SetActive(babyisSitting && !adult.isAlert && !FlierManage_54.inFlight &&  !hasPlane);
        throwTut.SetActive(babyisSitting && !adult.isAlert && !FlierManage_54.inFlight &&  hasPlane);
    }

    IEnumerator SittingRoutine()
    {
        while (babyisSitting)
        {
            if (!hasPlane)
            {
                if (Input.GetMouseButtonDown(0) && !FlierManage_54.inFlight)
                {
                    Pick();
                    yield return new WaitForSeconds(postPickDelay);
                    hasPlane = true;
                }
            }
            else
            {
                if (Input.GetMouseButtonDown(0) && !adult.isAlert)
                {
                    yield return StartCoroutine(ThrowRoutine());
                }
            }
            yield return null;
        }
    }
    IEnumerator FixPosition(float afterSeconds)
    {
        yield return new WaitForSeconds(afterSeconds);
        yield return PlayerController.Instance.transform.ReTransform(sitTargetPoint, sitTime);
    }


    private PickItem planeItem;
    public void Pick()
    {
        planeItem = Instantiate(planeItemPref).GetComponent<PickItem>();
        planeItem.gameObject.SetActive(true);
        PickItem.currentItem = planeItem;
        PlayerController.Instance.PickupBehavior(null, PickType.Standing, Hold);
        //StartCoroutine(FixPosition(0.2f));

    }
    
    public void Hold()
    {
        planeItem.rightHandItem.HoldItem(false);
        PlayerController.Instance.rig.SetBabyAnimation(Const.HOLDING, true);
        PlayerController.Instance.rig.SetBabyAnimation(Const.IDLE);
    }

    private IEnumerator ThrowRoutine()
    {
        PlayerController.Instance.rig.SetBabyAnimation(Const.THROW);
        yield return new WaitForSeconds(0.2f);
        if (adult.isAlert)
        {

            PlayerController.Instance.rig.SetBabyAnimation(Const.IDLE);
        }
        else
        {
            hasPlane = false;
            planeItem.DropItems(null, 0);
            Destroy(planeItem.rightHandItem.rb.gameObject);
            flier.StartFlight();
        }



    }


}
