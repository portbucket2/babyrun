using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FRIA;
using System;

public class AdulTeacher_54 : MonoBehaviour
{


    public List<Transform> path;
    public bool revertPath = false;
    public float turnTime=0.25f;
    public string obliviousState = "busy";
    public string vigilantState = "watch";
    public string hitByPlane = "bum_hit";
    public float waitTimeAtColdPoint = 2;
    public float waitTimeAtHotPoint = 1;
    public float bumHitTime = 1;
    AdultStateController stateCon;
    Coroutine mainRoutine;

    public bool isAlert;
    void Start()
    {
        mainRoutine = StartCoroutine(MainRoutine());
    }
    IEnumerator MainRoutine()
    {
        if (revertPath) path.Reverse();
        stateCon = GetComponent<AdultStateController>();
        stateCon.stateMate.AddStateEntryCallback(AdultCharState.win, () =>
        {
            StopAllCoroutines();
            stateCon.rig.SetTrigger(Const.FAIL);
            stateCon.rig.ChangeFace(AdultFace.Surprised);
            stateCon.rig.PlayExclamation();
        });
        stateCon.stateMate.AddStateEntryCallback(AdultCharState.lose, () =>
        {
            StopAllCoroutines();
            StartCoroutine(transform.RotateTowards(PlayerController.Instance.transform.position, 0.15f));
            stateCon.rig.SetTrigger(Const.STARTLE);
            stateCon.rig.ChangeFace(AdultFace.Surprised);
            stateCon.rig.PlayExclamation();
        });

        while (true)
        {
            isAlert = true;
            stateCon.rig.anim.CrossFadeInFixedTime(vigilantState, turnTime);
            yield return CSharpExtender.Iter(TransformToPoint, 1, path.Count - 1);
            yield return new WaitForSeconds(waitTimeAtHotPoint);

            isAlert = false;
            stateCon.rig.anim.CrossFadeInFixedTime(obliviousState, turnTime);
            yield return CSharpExtender.Iter(TransformToPoint, path.Count - 2, 0);
            yield return new WaitForSeconds(waitTimeAtHotPoint);
            
            while (FlierManage_54.inFlight)
            {
                wasInFlight = true;
                yield return null;
            }
            if (wasInFlight)
            {
                isAlert = true;
                stateCon.rig.anim.CrossFadeInFixedTime(hitByPlane, turnTime);
                yield return new WaitForSeconds(bumHitTime);
                wasInFlight = false;
            }

        }
    }
    bool wasInFlight = false;

    IEnumerator TransformToPoint(int i)
    {
        yield return StartCoroutine(transform.ReTransform(path[i], turnTime / path.Count, false, true));
    }

    bool plainShot;
    public void PlaneShot()
    {
        plainShot = true;
    }
}
