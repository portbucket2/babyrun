using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FRIA;

public class DeliveryManDogController : MonoBehaviour
{
    public System.Action onDogFinalPointReached;
    public Animator dogAnim;
    public float wakeUpDelay = 0.5f;
    public float wakingTime = 0.5f;
    public float maxSpeed = 2;
    public float acceleration = 10;
    public float rotationLerpRate = 5;

    public Transform initialBarkPoint;
    public Transform finalPoint;
    public FixedJoint joint;
    [Header("debug")]
    public bool isChained;
    IEnumerator Start()
    {
        yield return new WaitForSeconds(wakeUpDelay);
        dogAnim.SetTrigger("wake");
        yield return new WaitForSeconds(wakingTime);
        yield return StartCoroutine(transform.MoveToPoint(initialBarkPoint.position,speed,acceleration,maxSpeed,rotationLerpRate,null,(float speed)=> {
            this.speed = speed;
            dogAnim.SetFloat("speed",speed);
        }));

        while (isChained)
        {
            yield return null;
        }
        yield return StartCoroutine(transform.MoveToPoint(finalPoint.position, speed, acceleration, maxSpeed, rotationLerpRate, null, (float speed) => {
            this.speed = speed;
            dogAnim.SetFloat("speed", speed);
        }));
        onDogFinalPointReached?.Invoke();
    }

    float speed;
    public void ReleaseTheDog()
    {
        isChained = false;
        Destroy(joint);
    }
}
