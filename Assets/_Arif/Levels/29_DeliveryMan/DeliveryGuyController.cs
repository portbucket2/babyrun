using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FRIA;

public class DeliveryGuyController : MonoBehaviour
{
    public System.Action onDogFinalPointReached;
    public Animator anim;
    public float speed = 1;
    public float rotationLerpRate = 5;
    public DeliveryManDogController dogRef;

    public List<Transform> targetPoints;
    public DeliveryGuyTVBreaker tvBreaker;
    IEnumerator Start()
    {
        dogRef.onDogFinalPointReached += GetFreakedOut;
        foreach (var item in targetPoints)
        {
            yield return StartCoroutine(transform.SteadyWalk(item.position,speed,rotationLerpRate));
        }
        anim.SetTrigger("walkend");
    }

    //IEnumerator WalkToPoint(Transform tr)
    //{
    //    Vector3 directionVec = (tr.position - transform.position).normalized;
    //    Quaternion targetRotation = Quaternion.LookRotation(directionVec, Vector3.up);
    //    Vector3 currentDistVec;
    //    do
    //    {

    //        transform.rotation = Quaternion.Lerp(transform.rotation, targetRotation, rotationLerpRate * Time.deltaTime);

    //        currentDistVec = tr.position - transform.position;

    //        this.transform.Translate(directionVec * speed * Time.deltaTime, Space.World);
    //        yield return null;

    //    } while (currentDistVec.magnitude > 0.2f && Vector3.Dot(currentDistVec, directionVec) > 0);
    //}
    public void GetFreakedOut()
    {
        StopAllCoroutines();
        StartCoroutine(transform.RotateTowards(dogRef.transform.position,0.2f,()=> {
            anim.SetTrigger("fail");
            this.GetComponent<AdultStateController>().rig.PlayExclamation();
            tvBreaker.OnDrop((dogRef.transform.position-this.transform.position).normalized);
            LevelManager.Instance.ForceFillMeter();

        }));

    }
    //IEnumerator RotateTowards(Transform tr, float T)
    //{
    //    Vector3 directionVec = (tr.position - transform.position).normalized;
    //    Quaternion targetRotation = Quaternion.LookRotation(tr.position-this.transform.position, Vector3.up);
    //    float startTime = Time.time;
    //    Quaternion startRotation = transform.rotation;
    //    while (Time.time<startTime + T)
    //    {
    //        float p = Mathf.Clamp01((Time.time - startTime) / T);
    //        this.transform.rotation = Quaternion.Lerp(startRotation, targetRotation, p);
    //        yield return null;
    //    }

    //}
}
