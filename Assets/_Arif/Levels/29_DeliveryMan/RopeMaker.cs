using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FRIA;
#if UNITY_EDITOR
using UnityEditor;
#endif
public class RopeMaker : MonoBehaviour
{
    public List<Rigidbody> connectors;
    // Start is called before the first frame update
    public void AttachBodies()
    {

        for (int i = 0; i < connectors.Count-1; i++)
        {
            Joint j = connectors[i+1].GetComponent<Joint>();
            j.connectedBody = connectors[i];
        }
        Debug.Log("attaching bodies");
    }
}
#if UNITY_EDITOR
[CustomEditor(typeof(RopeMaker))]
public class RopeMaker_Editor: Editor
{
    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();
        if (GUILayout.Button("Attach bodies"))
        {
            (target as RopeMaker).AttachBodies();
            EditorFix.SetObjectDirty(target as RopeMaker);
        }
    }
}
#endif