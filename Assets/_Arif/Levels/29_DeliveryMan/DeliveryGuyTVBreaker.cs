using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FRIA;

public class DeliveryGuyTVBreaker : MonoBehaviour
{
    public List<Rigidbody> rgbds;
    public Rigidbody mainBody;
    public float breakDelay = 0.8f;
    public Vector2 forceMult = new Vector2(1, 2);
    public void OnDrop(Vector3 directionVec)
    {
        transform.SetParent(null);
        mainBody.isKinematic = false;
        mainBody.useGravity = true;
        Vector3 force = directionVec * forceMult.x + Vector3.up * forceMult.y;
        mainBody.AddForce(force, ForceMode.Impulse);
        Centralizer.Add_DelayedMonoAct(this,()=> {
            foreach (var item in rgbds)
            {
                item.isKinematic = false;
                item.transform.SetParent(null);
            }
            this.gameObject.SetActive(false);

        },breakDelay);
    }

}
