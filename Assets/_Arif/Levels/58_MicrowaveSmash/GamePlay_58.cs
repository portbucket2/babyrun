using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FRIA;

public class GamePlay_58 : MonoBehaviour
{
    public static bool isPlanted;
    public DoorControl microwaveDoor;
    public Transform throwable;
    public Transform target;
    public float beforeAdjustment=0.25f;
    public float adjustTime = 0.25f;
    public float afterAdjustment = 0.25f;

    private void Start()
    {
        isPlanted = false;
    }
    public void OnCakePickUp()
    {
        PlayerController.Instance.rig.anim.SetLayerWeight(1,1);
    }

    public void OnTargetPoint()
    {
        StartCoroutine(OnTargetRoutine());
    }
    IEnumerator OnTargetRoutine()
    {
        PlayerController.Instance.babyState = BabyState.Interested;
        PlayerController.Instance.inputEnabled = false;

        yield return microwaveDoor.Open();
        yield return new WaitForSeconds(0.15f);
        PlayerController.Instance.rig.anim.SetLayerWeight(1, 0);
        yield return PickItem.currentItem.DefaultDropRoutine();
        yield return new WaitForSeconds(beforeAdjustment);
        StartCoroutine(throwable.ReTransform(target,adjustTime));
        yield return new WaitForSeconds(adjustTime);
        isPlanted = true;
        yield return microwaveDoor.Close();
        PlayerController.Instance.babyState = BabyState.Normal;
        PlayerController.Instance.inputEnabled = true;
    }
}
