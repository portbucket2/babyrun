using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FRIA;
public class Adult_58 : MonoBehaviour
{
    public List<Transform> path;
    public float waitTimeAtStove = 2;
    public float waitTimeAtRacks = 1;
    public float timeToGrab = 0.5f;
    public float speed = 1;
    public float bombDelay = 0.1f;


    public GameObject item_to_carry;
    AdultStateController stateCon;
    public ParticleSystem bombParticle;
    public GameObject pickedBombReference;
    public List<GameObject> enableOnExplodeList;
    public List<GameObject> disableOnExplodeList;
    public Material sootyMaterial;
    public List<Renderer> tobeSootedRenderers;
    // Start is called before the first frame update

    public DoorControl fridgeDoor;
    public DoorControl microwaveDoor;
    IEnumerator Start()
    {
        //prc_30.onBombPlanted += SetCharge;
        stateCon = GetComponent<AdultStateController>();
        stateCon.stateMate.AddStateEntryCallback(AdultCharState.win, () =>
        {
            StopAllCoroutines();
            stateCon.rig.SetTrigger(Const.STARTLE);
            stateCon.rig.ChangeFace(AdultFace.Surprised);
            stateCon.rig.PlayExclamation();
        });
        stateCon.rig.onAnimationGrab += OnAnimationGrab;



        //yield return StartCoroutine(transform.SteadyWalk(path[0].position, speed, 5));
        while (isOnLoop)
        {
            for (int i = path.Count - 2; i >= 0; i--)
            {
                yield return StartCoroutine(transform.SteadyWalk(path[i].position, speed, 5));

            }
            transform.LookAt(path[0].position + path[0].forward);
            stateCon.rig.anim.CrossFadeInFixedTime("idle", .1f);
            if (GamePlay_58.isPlanted)
            {
                CamSwapController.SetCamera(CamID.celebration);
            }
            isExplodibleGrab = true;
            yield return MicroWaveRoutine();
            yield return new WaitForSeconds(waitTimeAtStove);
            isExplodibleGrab = false;
            yield return MicroWaveRoutine();
            stateCon.rig.anim.CrossFadeInFixedTime("mobile_walk", .1f);

            for (int i = 1; i < path.Count; i++)
            {
                yield return StartCoroutine(transform.SteadyWalk(path[i].position, speed, 5));

                "walking the path".Debug("FF00FF");
            }
            transform.LookAt(path[path.Count-1].position + path[path.Count-1].forward);
            stateCon.rig.anim.CrossFadeInFixedTime("idle", .1f);

            isExplodibleGrab = false;
            yield return  FridgeRoutine();

            stateCon.rig.anim.CrossFadeInFixedTime("mobile_walk", .1f);


        }
    }

    IEnumerator MicroWaveRoutine()
    {
        //open;
        yield return microwaveDoor.Open();
        stateCon.rig.anim.CrossFadeInFixedTime("grab", .1f);
        yield return new WaitForSeconds(timeToGrab);
        yield return microwaveDoor.Close();
        yield return new WaitForSeconds(0.35f);
        //close;
    }
    IEnumerator FridgeRoutine()
    {
        //open;
        yield return fridgeDoor.Open();
        stateCon.rig.anim.CrossFadeInFixedTime("grab", .1f);
        yield return new WaitForSeconds(waitTimeAtRacks); 
        stateCon.rig.anim.CrossFadeInFixedTime("grab", .1f);
        yield return new WaitForSeconds(timeToGrab);
        yield return fridgeDoor.Close();
        yield return new WaitForSeconds(0.35f);
        //close; 
    }
    bool isExplodibleGrab = false;
    void OnAnimationGrab()
    {
        if (isExplodibleGrab && GamePlay_58.isPlanted )
        {
            OnExplode();
        }
        else
        {

            item_to_carry.SetActive(!item_to_carry.activeSelf);
        }
    }
    void OnExplode()
    {
        isOnLoop = false;
        StopAllCoroutines();
        //bomato.SetActive(false);
        item_to_carry.SetActive(false);
        AdultStateController asc = GetComponent<AdultStateController>();
        asc.rig.anim.enabled = false;
        asc.rig.ChangeFace(AdultFace.Surprised);
        asc.transform.RotateTowards(Camera.main.transform.position, 0.25f);
        bombParticle?.Play();
        LevelManager.Instance.ForceFillMeter();
        enableOnExplodeList.ActivateAll();
        disableOnExplodeList.DeactivateAll();
        foreach (var item in tobeSootedRenderers)
        {
            item.material = sootyMaterial;
        }

    }
    public bool isOnLoop = true;
}
