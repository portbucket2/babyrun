using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FRIA;

public class Adult_34 : MonoBehaviour
{
    public List<Transform> path;
    public float waitTime1 = 2;
    public float waitTime2 = 1;
    public float timeToGrab = 0.5f;
    public float speed = 1;
    public float bombDelay = 0.1f;

    AdultStateController stateCon;
    public Transform merryGoTrans;
    IEnumerator Start()
    {
        //prc_30.onBombPlanted += SetCharge;
        stateCon = GetComponent<AdultStateController>();
        stateCon.stateMate.AddStateEntryCallback(AdultCharState.win, () =>
        {
            "Will adult call fail".Debug("FF00FF");
            StopAllCoroutines();
            stateCon.rig.SetTrigger(Const.FAIL);
            stateCon.rig.ChangeFace(AdultFace.Surprised);
            stateCon.rig.PlayExclamation();
        });
        stateCon.stateMate.AddStateEntryCallback(AdultCharState.lose, () =>
        {
            "Will adult call win".Debug("FF00FF");
            StopAllCoroutines();
            stateCon.rig.SetTrigger(Const.STARTLE);
            stateCon.rig.ChangeFace(AdultFace.Surprised);
            StartCoroutine( transform.RotateTowards(merryGoTrans.position, 0.5f));
            stateCon.rig.PlayExclamation();
        });
        //yield return StartCoroutine(transform.SteadyWalk(path[0].position, speed, 5));
        while (isOnLoop)
        {
            for (int i = path.Count - 2; i >= 0; i--)
            {
                yield return StartCoroutine(transform.SteadyWalk(path[i].position, speed, 5));

            }
            transform.LookAt(path[0].position + path[0].forward);
            stateCon.rig.anim.CrossFadeInFixedTime("idle", .25f);
            yield return new WaitForSeconds(waitTime1);
            stateCon.rig.anim.CrossFadeInFixedTime("mobile_walk", .25f);

            for (int i = 1; i < path.Count; i++)
            {
                yield return StartCoroutine(transform.SteadyWalk(path[i].position, speed, 5));
            }
            transform.LookAt(path[path.Count - 1].position + path[path.Count - 1].forward);
            stateCon.rig.anim.CrossFadeInFixedTime("idle", .25f);
            yield return new WaitForSeconds(waitTime2);
            stateCon.rig.anim.CrossFadeInFixedTime("mobile_walk", .25f);


        }
    }

    //IEnumerator MicroWaveRoutine()
    //{
    //    //open;
    //    yield return microwaveDoor.Open();
    //    stateCon.rig.anim.CrossFadeInFixedTime("grab", .1f);
    //    yield return new WaitForSeconds(timeToGrab);
    //    yield return microwaveDoor.Close();
    //    yield return new WaitForSeconds(0.35f);
    //    //close;
    //}
    //IEnumerator FridgeRoutine()
    //{
    //    //open;
    //    yield return fridgeDoor.Open();
    //    stateCon.rig.anim.CrossFadeInFixedTime("grab", .1f);
    //    yield return new WaitForSeconds(waitTimeAtRacks);
    //    stateCon.rig.anim.CrossFadeInFixedTime("grab", .1f);
    //    yield return new WaitForSeconds(timeToGrab);
    //    yield return fridgeDoor.Close();
    //    yield return new WaitForSeconds(0.35f);
    //    //close; 
    //}
    //bool isExplodibleGrab = false;
    
    //void OnExplode()
    //{
    //    isOnLoop = false;
    //    StopAllCoroutines();
    //    //bomato.SetActive(false);
    //    item_to_carry.SetActive(false);
    //    AdultStateController asc = GetComponent<AdultStateController>();
    //    asc.rig.anim.enabled = false;
    //    asc.rig.ChangeFace(AdultFace.Surprised);
    //    asc.transform.RotateTowards(Camera.main.transform.position, 0.25f);
    //    bombParticle?.Play();
    //    LevelManager.Instance.ForceFillMeter();
    //    enableOnExplodeList.ActivateAll();
    //    disableOnExplodeList.DeactivateAll();
    //    foreach (var item in tobeSootedRenderers)
    //    {
    //        item.material = sootyMaterial;
    //    }

    //}
    public bool isOnLoop = true;
}
