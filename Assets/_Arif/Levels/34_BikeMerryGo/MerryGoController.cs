using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FRIA;
using DG.Tweening;

public class MerryGoController : MonoBehaviour
{

    public float TimeBudget;
    public Animator merryAnim;
    public Animator bikeAnim;

    public List<Rider_34> riders;
    public int spinCount = 3;
    [Range(0,1)]
    public float throwSpeed = 0.9f;
    public float initialSpeed = 0.1f;
    public ParticleSystem ps;
    private void Start()
    {
        LevelManager.Instance.onLevelFinished += (bool success) => {
            if (!success)
            {
                ps.Pause();
                StopAllCoroutines();
                StartCoroutine(KillSpeed());

                foreach (var item in riders)
                {
                    item.anim.CrossFadeInFixedTime("idle",0.25f);
                }
            }            
        };
    }
    public void Accellerate()
    {
        bikeAnim.SetTrigger("start");
        StartCoroutine(SpeedUpRoutine());
       
        this.Add_DelayedAct(() =>
        {
            if (LevelManager.Instance.state == LevelState.Fail) return;
            
            foreach (var item in riders)
            {
                item.anim.SetTrigger("fail");
            }
        }, 0.25f);
    }
    public float speedBlend;
    IEnumerator SpeedUpRoutine()
    {
        float sT = Time.time;
        while (Time.time < sT + TimeBudget && LevelManager.Instance.state != LevelState.Fail)
        {
            float p = Mathf.Clamp01((Time.time-sT)/TimeBudget);
            speedBlend = Mathf.Lerp(initialSpeed, 1, p);
            if (speedBlend > throwSpeed)
            {
                if (!ps.isPlaying) ps.Play();
            }
            merryAnim.SetFloat("blend", speedBlend);
            yield return null;
        }
        //speedBlend = 1;
        //merryAnim.SetFloat("blend", speedBlend);
        
    }
    int spC;
    bool throwcomplete;
    public void OnThrowPoint()
    {
        if (speedBlend >= throwSpeed && LevelManager.Instance.state != LevelState.Fail)
        {
            if (spC < spinCount)
            {
                spC++;
            }
            else if (!throwcomplete)
            {
                throwcomplete = transform;
                "throw them off".Debug("FF00FF");
                foreach (var item in riders)
                {
                    item.DoThrow(this);
                }
            }

        }
    }
    IEnumerator KillSpeed()
    {
        float sT = Time.time;
        float startSpeed = speedBlend;
        while (Time.time < sT + TimeBudget)
        {
            float p = Mathf.Clamp01((Time.time - sT) / TimeBudget);
            speedBlend = Mathf.Lerp(startSpeed, initialSpeed, p);
            merryAnim.SetFloat("blend", speedBlend);
            yield return null;
        }
        speedBlend = initialSpeed;
        merryAnim.SetFloat("blend", speedBlend);

    }
}

[System.Serializable]
public class Rider_34
{
    public Transform root;
    public Animator anim;
    public Transform throwPoint;
    public float time;
    public float jumpPower;
    public List<GameObject> enableAfterThrw;

    public void DoThrow(MonoBehaviour mono)
    {
        root.SetParent(null);
        root.DOJump(throwPoint.position, jumpPower, 1, time);
        mono.StartCoroutine(root.ReTransform(throwPoint, time, false, onComplete: () => {
            anim.enabled = false;
            enableAfterThrw.ActivateAll();
        }));
    }
}