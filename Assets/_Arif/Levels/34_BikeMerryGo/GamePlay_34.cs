using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FRIA;

public class GamePlay_34 : MonoBehaviour
{
    public Adult_34 adult;
    public PickItem blankPick;
    public float estimatedPickTime;
    public float winDelay= 2;
    public void OnTargetPoint()
    {
        StartCoroutine(OnTargetRoutine());
    }
    IEnumerator OnTargetRoutine()
    {
        blankPick.OnInteract();
        yield return new WaitForSeconds(estimatedPickTime);

        PlayerController.Instance.babyState = BabyState.Interested;
        PlayerController.Instance.inputEnabled = false;
        yield return blankPick.DefaultDropRoutine();
        if (LevelManager.Instance.InPlayMode)
        {
            "Will call player fail".Debug("FF00FF");

            adult.GetComponent<AdultStateController>().stateMate.SwitchState(AdultCharState.lose);
        }
        yield return new WaitForSeconds(winDelay);
        LevelManager.Instance.ForceFillMeter();
        CamSwapController.SetCamera(CamID.celebration);

    }

}
