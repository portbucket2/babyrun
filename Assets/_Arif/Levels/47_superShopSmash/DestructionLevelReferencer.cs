using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestructionLevelReferencer : MonoBehaviour
{
    public static DestructionLevelReferencer Instance { get; private set; }

    public GameObject coinPrefab;

    void Awake()
    {
        Instance = this;
    }

}
