using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FRIA;

public class CoinCollection_Pooled : CoinCollection
{
    public override void DestroyThis()
    {
        Pool.Destroy(this.gameObject);
    }
}
