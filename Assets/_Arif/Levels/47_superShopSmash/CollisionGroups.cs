using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FRIA;

public class CollisionGroups : MonoBehaviour
{
    public static CollisionGroups Instance { get; private set; }
    public List<CollisionGroup> allGroups;

    public float travelSpeed = 0.5f;

    private void Awake()
    {
        Instance = this;
    }

    public void InformGroupOfCollision(GameObject current)
    {
        foreach (CollisionGroup group in allGroups)
        {
            if (group.items.Contains(current))
            {
                foreach (GameObject item in group.items)
                {
                    Trig(item, current);
                }
            }
        }
    }
    public void Trig(GameObject item, GameObject current)
    {
        CoinOnCollideController cocc = item.GetComponent<CoinOnCollideController>();


        if (cocc)
        {
            float dist = Vector3.Distance(current.transform.position, item.transform.position);
            float T = dist / travelSpeed;
            this.Add_DelayedAct(() =>
            {
                cocc.TriggerThis(isChainReaction: true);
            }, T);
        }
    }
}
[System.Serializable]
public class CollisionGroup
{
    public string title;
    public List<GameObject> items;
}