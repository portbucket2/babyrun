using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FRIA;

public class CoinOnCollideController : MonoBehaviour
{
    private bool unrewarded = true;
    private Rigidbody rb;
    public Vector3 spawnPoint;
    public Vector3 spawnScale = Vector3.one;
    public int coinValue = 1;

    private void Awake()
    {
        rb = GetComponent<Rigidbody>();
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (!unrewarded || !collision.gameObject.CompareTag(Const.PLAYER)) return;
        TriggerThis(false);
    }
    public void TriggerThis(bool isChainReaction)
    {
        if (!unrewarded) return;

        MakeAcitve(isChainReaction);
        if(!isChainReaction)ClaimReward();
    }

    public void MakeAcitve(bool solo)
    {
        rb.isKinematic = false; 
        if (!solo)
        {
            CollisionGroups.Instance.InformGroupOfCollision(gameObject);
        }
    }
    public void ClaimReward()
    {
        unrewarded = false;
        GameObject go = Pool.Instantiate(DestructionLevelReferencer.Instance.coinPrefab);
        go.transform.SetParent(this.transform);
        go.transform.localPosition = spawnPoint;
        go.transform.localScale = spawnScale;
        go.GetComponent<CoinCollection>().SetValue(coinValue);
    }
}