using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FRIA;

public class FallOff : MonoBehaviour
{
    AdultStateController adultState;
    public Animator treadmillAnim;

    // Start is called before the first frame update
    void Start()
    {
        adultState = GetComponent<AdultStateController>();
        adultState.stateMate.AddStateEntryCallback(AdultCharState.lose,()=> {
            Centralizer.Add_DelayedMonoAct(this,()=> {
                treadmillAnim.enabled = true;
                adultState.rig.SetTrigger("fail");
            },.75f);
        });
    }
}
