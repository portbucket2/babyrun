using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FRIA;
public class AdultRoutineController_30 : MonoBehaviour
{
    public List<Transform> path;
    public float waitTimeAtStove = 2;
    public float waitTimeAtRacks = 1;
    public float speed = 1;
    public float bombDelay = 0.1f;


    public GameObject tomato;
    public GameObject bomato;
    AdultStateController stateCon;
    public ParticleSystem bombParticle;
    public GameObject pickedBombReference;
    public List<GameObject> enableOnExplodeList;
    public List<GameObject> disableOnExplodeList;
    public Material sootyMaterial;
    public List<Renderer> tobeSootedRenderers;
    // Start is called before the first frame update
    IEnumerator Start()
    {
        //prc_30.onBombPlanted += SetCharge;
        stateCon = GetComponent<AdultStateController>();
        stateCon.stateMate.AddStateEntryCallback(AdultCharState.win, () =>
         {
             StopAllCoroutines();
             stateCon.rig.SetTrigger(Const.STARTLE);
             stateCon.rig.ChangeFace(AdultFace.Surprised);
             stateCon.rig.PlayExclamation();
         });
        stateCon.rig.onAnimationGrab += OnAnimationGrab;



        //yield return StartCoroutine(transform.SteadyWalk(path[0].position, speed, 5));
        $"In play Mode : {LevelManager.Instance.InPlayMode}".Debug("FF00FF");
        while (isOnLoop)
        {

            for (int i = 1; i < path.Count; i++)
            {
                yield return StartCoroutine(transform.SteadyWalk(path[i].position, speed, 5));
            }
            if (planted)
            {

                "cam swapped".Debug("0022FF");
                CamSwapController.SetCamera(CamID.celebration);
            }            
            stateCon.rig.anim.CrossFadeInFixedTime("grab", .1f);
            yield return new WaitForSeconds(waitTimeAtRacks);
            stateCon.rig.anim.CrossFadeInFixedTime("mobile_walk", .1f);


            for (int i = path.Count - 2; i >= 0; i--)
            {   
                yield return StartCoroutine(transform.SteadyWalk(path[i].position, speed, 5));

            }
            stateCon.rig.anim.CrossFadeInFixedTime("grab", .1f);
            yield return new WaitForSeconds(waitTimeAtStove);
            stateCon.rig.anim.CrossFadeInFixedTime("mobile_walk", .1f);
        }
    }

    bool planted;
    public void SetCharge()
    {

        planted = true;
    }
    void OnAnimationGrab()
    {
        if (tomato.activeSelf)
        {
            tomato.SetActive(false);
        }
        else
        {
            if (!planted)
            {
                tomato.SetActive(true);
            }
            else
            {

                pickedBombReference.SetActive(false);
                bomato.SetActive(true);
                this.Add_DelayedAct(OnExplode,bombDelay);
            }
        }
    }
    void OnExplode()
    {
        isOnLoop = false;
        StopAllCoroutines();
        bomato.SetActive(false);
        AdultStateController asc = GetComponent<AdultStateController>();
        asc.rig.anim.enabled = false;
        asc.rig.ChangeFace(AdultFace.Surprised);
        asc.transform.RotateTowards(Camera.main.transform.position, 0.25f);
        bombParticle?.Play();
        LevelManager.Instance.ForceFillMeter();
        enableOnExplodeList.ActivateAll();
        disableOnExplodeList.DeactivateAll();
        foreach (var item in tobeSootedRenderers)
        {
            item.material = sootyMaterial;
        }
        
    }
    public bool isOnLoop= true;
}
