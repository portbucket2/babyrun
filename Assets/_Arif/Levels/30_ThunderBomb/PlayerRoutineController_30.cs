using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerRoutineController_30 : MonoBehaviour
{
    public event System.Action onBombPlanted;
    public Transform throwPoint;
    public float angle;
    public void OnInteract() 
    {
        StartCoroutine(ThrowRoutine());
    }
    private IEnumerator ThrowRoutine()
    {
        PlayerController.Instance.rig.SetBabyAnimation(Const.THROW);
        PlayerController.Instance.transform.LookAt(transform.position.XZ());
        yield return new WaitForSeconds(0.2f);
        DropCurrentItem();
        yield return new WaitForSeconds(0.2f);
        onBombPlanted?.Invoke();

    }
    void DropCurrentItem()
    {
        if (PickItem.currentItem) PickItem.currentItem.DropItems(throwPoint, angle);
        if (PickableItem.currentItem) PickableItem.currentItem.DropItems(throwPoint, angle);
    }
}
