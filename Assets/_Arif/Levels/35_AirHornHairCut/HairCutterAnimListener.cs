using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HairCutterAnimListener : MonoBehaviour
{
    public ParticleSystem mainParticle;
    public ParticleSystem failParticle;
    public GameObject hairToDisable;
    // Update is called once per frame
    public void OnAnimationCutHair()
    {
        mainParticle?.Play();
    }
    public void OnAnimationCutHairMistake()
    {
        failParticle?.Play();
        hairToDisable.SetActive(false);
    }
}
