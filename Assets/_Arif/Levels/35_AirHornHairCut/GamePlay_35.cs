using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FRIA;

public class GamePlay_35 : MonoBehaviour
{
    public ParticleSystem hornParticle;
    public AdultStateController hairCutterAdult;
    public AdultStateController victimAdult;

    public GameObject fovObject;

    public float threatenDelay = 0.35f;
    private void Start()
    {
        LevelManager.Instance.onLevelFinished += OnFinished;
    }

    void OnFinished(bool success)
    {
        if (!success)
        {
            CamSwapController.SetCamera(CamID.deathcam);
            hairCutterAdult.rig.SetTrigger(Const.FAIL);
            hairCutterAdult.transform.LookAt(PlayerController.Instance.transform);
            //hairCutterAdult.transform.RotateTowards(PlayerController.Instance.transform.position, 0.25f, LevelManager.Instance.ForceFillMeter);
        }
    }

    public void OnTargetTriggerReached()
    {
        PlayerController.Instance.babyState = BabyState.Interested;
        PlayerController.Instance.rig.ChangeFace(BabyFace.Naughty);
        PlayerController.Instance.rig.SetBabyAnimation(Const.SHAKE);
        if (hornParticle) hornParticle.Play();

        hairCutterAdult.rig.SetTrigger(Const.STARTLE);
        this.Add_DelayedAct(() => {
            fovObject.SetActive(false);
            LevelManager.Instance.ForceFillMeter();
            hairCutterAdult.rig.SetTrigger(Const.FAIL);
            victimAdult.rig.SetTrigger(Const.FAIL);
            hairCutterAdult.transform.LookAt(PlayerController.Instance.transform);

            CamSwapController.SetCamera(CamID.celebration);
        },threatenDelay);

    }
}
