using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FRIA;

public class CatchDog : MonoBehaviour
{
    public float catchDistance = 1;
    [SerializeField] private Collider dogCollider;
    [SerializeField] private Animator anim;
    public StrechRopeController rope;
    public Transform babyHand;
    public Transform runToPoint;
    void Start()
    {
        playerController = PlayerController.Instance;
        parentController = ParentController.Instance;
        levelManager = LevelManager.Instance;

        levelManager.beforeLevelFinished = LetItGo;
    }
    GameObject childCollider;
    public void Getcaught()
    {
        rope.SetTarget(babyHand);
        transform.SetParent(playerController.transform, true);
        transform.localRotation = Quaternion.identity;
        transform.localPosition = catchDistance * Vector3.forward;
        anim.SetBool("run",true);
        gameObject.layer = LayerMask.NameToLayer("Player");
        playerController.CatCaught();
        levelManager.MeterFiller();
        if (parentController) parentController.GetStartle();

        childCollider = new GameObject("ChildCollider") { layer = LayerMask.NameToLayer("Player") };

        childCollider.tag = Const.PLAYER;
        childCollider.transform.SetParent(playerController.transform);
        childCollider.transform.position = transform.position;
        childCollider.transform.rotation = transform.rotation;
        childCollider.transform.localScale = transform.localScale;

        SphereCollider newCollider = childCollider.AddComponent<SphereCollider>();
        newCollider.center = ((SphereCollider)dogCollider).center;
        newCollider.radius = ((SphereCollider)dogCollider).radius * 0.9f;
        newCollider.isTrigger = false;

        //VerticalscratchX.Play();
        //VerticalscratchZ.Play();
        //horizontalscratch.Play();
    }

    public void LetItGo()
    {
        rope.SetTarget(null);
        //VerticalscratchX.Stop();
        //VerticalscratchZ.Stop();
        //horizontalscratch.Stop();
        Destroy(childCollider);

        transform.SetParent(null, true);
        dogCollider.isTrigger = false;
        dogCollider.attachedRigidbody.useGravity = false;
        dogCollider.attachedRigidbody.isKinematic = true;
        StartCoroutine(transform.MoveToPoint(runToPoint.position,2,10,10,rotRate:5));
        //StartCoroutine(DashForward(4f));
    }
    //IEnumerator DashForward(float speed)
    //{
    //    anim.SetTrigger("run");
    //    float t = 0;
    //    Vector3 pos = transform.position;
    //    while (t < 3)
    //    {
    //        transform.Translate(transform.forward * speed * Time.deltaTime, Space.World);
    //        dogCollider.attachedRigidbody.MovePosition(pos + transform.forward * t * speed);
    //        t += Time.deltaTime;
    //        yield return null;
    //    }

    //    anim.SetTrigger("lick");
    //}
    //private void OnTriggerStay(Collider other)
    //{
    //    if (other.gameObject.layer == LayerMask.NameToLayer("scratchable"))
    //    {
    //        catAnim.SetBool("stand", true);
    //    }
    //}

    //private void OnTriggerExit(Collider other)
    //{
    //    if (other.gameObject.layer == LayerMask.NameToLayer("scratchable"))
    //    {
    //        catAnim.SetBool("stand", false);
    //    }
    //}

    private PlayerController playerController;
    private ParentController parentController;
    private LevelManager levelManager;


}
