using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FRIA;

public class StrechRopeController : MonoBehaviour
{
    public Transform target;
    public float lerpRate = 5;
    public float defaultlength = 1.5f;
    public Transform defaultDir;

    private void LateUpdate()
    {
        if (target)
        {
            Vector3 distV = target.position - this.transform.position;

            this.transform.forward = distV.normalized;
            this.transform.localScale = new Vector3(1, 1, distV.magnitude);
        }
        else
        {
            if (defaultDir)
            {
                this.transform.rotation = Quaternion.Lerp(transform.rotation, defaultDir.rotation, lerpRate * Time.deltaTime);
            }
            this.transform.localScale = Vector3.Lerp(transform.localScale, new Vector3(1, 1, defaultlength), lerpRate * Time.deltaTime);
        }
    }
    public void SetTarget(Transform newTarget)
    {
        target = newTarget;    
    }
}
