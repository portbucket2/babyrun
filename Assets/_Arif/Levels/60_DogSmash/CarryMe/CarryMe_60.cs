using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FRIA;
using DG.Tweening;
public class CarryMe_60 : MonoBehaviour
{
    public Animator guybrideAnim;
    public Animator brideCatcherAnim;
    public float catcherDelay=0.25f;
    public string stateName;

    public Transform target;
    public Transform endPos;
    public float timeBudget= 0.5f;
    public float jumpPower = 2;
    public float bonusFill = 0.3f;
    public void OnTriggered()
    {

        LevelManager.Instance.totalFill += bonusFill;
        AdultRigController arc = guybrideAnim.GetComponent<AdultRigController>();
        arc.PlayExclamation();
        arc.ChangeFace(AdultFace.Surprised);
        this.Add_DelayedAct(() => {

            guybrideAnim.SetTrigger(stateName);
            brideCatcherAnim.SetTrigger(stateName);
        }, catcherDelay);

        if (target && endPos)
        {
            target.DOJump(endPos.position, jumpPower, 1, timeBudget);
            StartCoroutine(target.ReTransform(endPos, timeBudget,false,onComplete:()=> {
                target.SetParent(endPos);
                target.localPosition = Vector3.zero;
                target.localRotation = Quaternion.identity;
            }));
        }
    }
}
