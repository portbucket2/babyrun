using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FRIA;
using DG.Tweening;

public class SwingJump_60 : MonoBehaviour
{
    public Animator swingAnim;
    public float swingerReleaseDelay;
    public Transform swingerTarget;
    public Transform swingerEndPos;
    public float swingerTimeBudget = 0.5f;
    public float swingerJumpPower = 2;

    public Animator seesawAnim;
    public float seesawReleaseDelay;
    public Transform seesawTarget;
    public Transform seesawEndPos;
    public float seesawTimeBudget = 0.5f;
    public float seesawJumpPower = 2;
    public float bonusFill = 1f;
    public Animator catAnim;
    public ParticleSystem leafParticles;
    public void OnTriggered()
    {
        StartCoroutine(Routine());
    }

    IEnumerator Routine()
    {
        LevelManager.Instance.totalFill += bonusFill;
        CamSwapController.SetCamera(CamID.celebration);
        swingAnim.SetTrigger("action");
        yield return new WaitForSeconds(swingerReleaseDelay);
        swingerTarget.SetParent(null);
        swingerTarget.DOJump(swingerEndPos.position, swingerJumpPower, 1, swingerTimeBudget);
        yield return new WaitForSeconds(swingerTimeBudget);
        swingerTarget.SetParent(swingerEndPos.transform);
        swingerTarget.localRotation = Quaternion.identity;

        seesawAnim.SetTrigger("action");
        yield return new WaitForSeconds(seesawReleaseDelay);
        seesawTarget.DOJump(seesawEndPos.position, seesawJumpPower, 1, seesawTimeBudget);
        catAnim.SetTrigger("fail");
        yield return new WaitForSeconds(seesawTimeBudget);
        leafParticles.Play();
    }
}
