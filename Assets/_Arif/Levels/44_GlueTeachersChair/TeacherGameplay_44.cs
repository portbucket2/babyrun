using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FRIA;

public class TeacherGameplay_44 : MonoBehaviour
{
    public List<Transform> path;
    public float speed = 1;
    public float waitTimeAtHotPoint = 1;
    public float waitTimeAtColdPoint = 2;
    AdultStateController stateCon;
    bool planted;

    public string waitState = "idle";
    public string walkState = "walk";

    public Transform sitPoint;
    public Transform sitLookTarget;
    Coroutine mainRoutine;


    void Start()
    {
        mainRoutine = StartCoroutine(MainRoutine()) ;
    }
    IEnumerator MainRoutine()
    {
        stateCon = GetComponent<AdultStateController>();
        stateCon.stateMate.AddStateEntryCallback(AdultCharState.win, () =>
        {
            StopAllCoroutines();
            stateCon.rig.SetTrigger(Const.FAIL);
            stateCon.rig.ChangeFace(AdultFace.Surprised);
            stateCon.rig.PlayExclamation();
        });

        while (true)
        {

            for (int i = 1; i < path.Count; i++)
            {
                yield return StartCoroutine(transform.SteadyWalk(path[i].position, speed, 5));
            }
            if (planted)
            {
                StopCoroutine(mainRoutine);
                StartCoroutine(ExecutionRoutine());
            }
            stateCon.rig.anim.CrossFadeInFixedTime(waitState, .1f);
            yield return new WaitForSeconds(waitTimeAtHotPoint);
            stateCon.rig.anim.CrossFadeInFixedTime(walkState, .1f);


            for (int i = path.Count - 2; i >= 0; i--)
            {
                yield return StartCoroutine(transform.SteadyWalk(path[i].position, speed, 5));

            }
            stateCon.rig.anim.CrossFadeInFixedTime(waitState, .1f);
            yield return new WaitForSeconds(waitTimeAtColdPoint);
            stateCon.rig.anim.CrossFadeInFixedTime(walkState, .1f);
        }
    }
    public void Plant()
    {
        planted = true;

    }
    public IEnumerator ExecutionRoutine()
    {
        stateCon.rig.anim.CrossFadeInFixedTime(walkState, .1f);
        yield return StartCoroutine(transform.SteadyWalk(sitPoint.position, speed, 5));
        CamSwapController.SetCamera(CamID.celebration);
        this.transform.position = sitPoint.position;
        transform.LookAt(sitLookTarget);
        stateCon.rig.anim.CrossFadeInFixedTime(waitState, .1f);
        yield return new WaitForSeconds(.2f);
        stateCon.rig.anim.CrossFadeInFixedTime("sit", .5f);
    }
}
