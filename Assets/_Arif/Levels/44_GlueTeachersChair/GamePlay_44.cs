using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FRIA;
using DG.Tweening;
public class GamePlay_44 : MonoBehaviour
{
    public Transform glue;
    public float glueDelay=0.5f;
    public float releaseDelay = 0.25f;
    public float dropDelay = 0.25f;
    public Transform throwPoint;
    private void Start()
    {
        glue.localScale = Vector3.zero;
    }
    public void OnGlue()
    {
        PlayerController.Instance.babyState = BabyState.Interested;
        PlayerController.Instance.transform.LookAt(glue);
        PlayerController.Instance.rig.SetBabyAnimation("glue");
        PlayerController.Instance.rig.ChangeFace(BabyFace.Naughty);
        PlayerController.Instance.inputEnabled = false;

        this.Add_DelayedAct(() =>
        {
            glue.DOScale(1, 0.75f);
            this.Add_DelayedAct(()=> {
                PlayerController.Instance.babyState = BabyState.Normal;
                PlayerController.Instance.inputEnabled = true;

                PlayerController.Instance.rig.SetBabyAnimation(Const.THROW);


                this.Add_DelayedAct(() =>
                {
                    if (PickItem.currentItem) PickItem.currentItem.DropItems(throwPoint, 0);
                    if (PickableItem.currentItem) PickableItem.currentItem.DropItems(throwPoint, 0);
                }, dropDelay);
            }, releaseDelay);
        }, glueDelay);
    }

}
