using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FRIA;

public class TeacherAnimListener_44 : MonoBehaviour
{
    public GameObject glue;
    public Transform chair;
    public Transform hipBone;
    public float forceMag;
    
    public void OnAnimChairStuck()
    {
        glue.SetActive(false);
        chair.SetParent(hipBone);
    }
    public void OnAnimChairRelease()
    {
        chair.SetParent(null);
        Rigidbody rgbd = chair.GetComponent<Rigidbody>();
        rgbd.isKinematic = false;
        rgbd.AddForce(-forceMag*chair.up,ForceMode.Impulse);
        this.Add_DelayedAct(() => {
            LevelManager.Instance.ForceFillMeter();
        },1);
    }
}
