using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickUpTrigger : Trigger
{
    public Transform pickable;

    protected override void OnTrigger()
    {
        base.OnTrigger();

        PlayerController pc = PlayerController.Instance;

        pc.babyState = BabyState.Interested;
        pc.rig.ChangeFace(BabyFace.Happy);
        pc.rig.SetBabyAnimation(Const.PICK);

        pickable.SetPositionAndRotation(pc.rig.rightHand.position,pc.rig.rightHand.rotation);
        pickable.SetParent(pc.rig.rightHand);
        pc.rb.LookAtXZ(transform);
    }

}
