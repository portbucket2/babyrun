using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FRIA;


public class DoorControl : MonoBehaviour
{
    public Transform openPosition;
    public Transform closePosition;
    public float timeBudget = 0.15f;

    public IEnumerator Open() 
    {
        yield return transform.ReTransform(openPosition, timeBudget);
    }
    public IEnumerator Close()
    {
        yield return transform.ReTransform(closePosition, timeBudget);
    }
}
