using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using PotatoSDK;
using FRIA;
public class SuccessPanelController : MonoBehaviour
{
    public int baseEarning = 50;
    [SerializeField] internal Button homeButton;
    [SerializeField] internal Button claimButton;
    [SerializeField] internal Button skipButton;
    [SerializeField] internal GameObject root;
    [SerializeField] float proceedDelay = 1f;



    [SerializeField] internal float limit;
    [SerializeField] float worth = 3f;
    [SerializeField] internal float speed = 3;
    public Transform ticker;

    public GameObject skipButtonObject;

    public Text skipEarningText;
    public Text claimEarningText;

    float cell_width;
    Vector3 initialPosition;
    void Start()
    {
        homeButton.onClick.AddListener(OnHome);
        claimButton.onClick.AddListener(OnClaim);
        skipButton.onClick.AddListener(OnSkip);
        initialPosition = ticker.localPosition;
        initialPosition.x = 0;
        cell_width = limit / worth;
        skipButtonObject.SetActive(false);

    }

    void OnSkip()
    {
        claimButton.interactable = false;
        skipButton.interactable = false;
        homeButton.interactable = false;
        Currency.Transaction(CurrencyType.COIN, baseEarning);
        AnalyticsAssistant.ReportClick(AnalyticClickType.SKIP_COIN_MULTIPLIER, LevelLoader.SelectedLevelNumber-1);
        OnProceed();
    }
    void OnProceed()
    {
        if (EnergyTracker.hasEnergyToPlay && !LevelLoader.ShouldVisitHome)
        {
            Unload();
            LevelProgressionController.Proceed_IsBonus(false);
        }
        else
        {
            if (BuildSpecManager.enableInterstitial) MAXMan.ShowInterstitial();
            LevelLoader.LoadHome();
        }

    }
    void OnClaim()
    {
        claimButton.interactable = false;
        skipButton.interactable = false;
        homeButton.interactable = false;
        paused = true;
        MAXMan.ShowRV((bool success) => 
        {

            if (success)
            {

                Currency.Transaction(CurrencyType.COIN, claimableEarning);
                AnalyticsAssistant.ReportCollect(AnalyticCollectType.RV_MULTIPLIED_COINS, LevelLoader.SelectedLevelNumber-1);
                this.Add_DelayedAct(OnProceed, proceedDelay);
            }
            else 
            {
                paused = false;
                claimButton.interactable = true;
                skipButton.interactable = true;
                homeButton.interactable = true;
            }
        });

    }

    void OnHome()
    {
        claimButton.interactable = false;
        skipButton.interactable = false;
        homeButton.interactable = false;
        Currency.Transaction(CurrencyType.COIN, baseEarning);
        if (BuildSpecManager.enableInterstitial) MAXMan.ShowInterstitial();
        LevelLoader.LoadHome();
        AnalyticsAssistant.ReportClick(AnalyticClickType.HOME_BUTTON, LevelLoader.SelectedLevelNumber-1,"ON_SUCCESS");
    }
    public void Load()
    {
        LevelLoader.Instance.IncreaseGameLevel();
        root.Active();
        skipEarningText.text = $"{baseEarning}";

        skipButtonObject.SetActive(false);
        this.Add_DelayedAct(() => {
            skipButtonObject.SetActive(true);
        }, 2);
    }
    public void Unload()
    {
        root.Inactive();
    }
    bool paused;
    float T;
    int x;
    int x_abs;
    int claimableEarning;
    private void LateUpdate()
    {
        if (!paused) T += Time.deltaTime;

        float p = Mathf.Sin(T * speed);
        float pos = p * limit;
        ticker.localPosition = new Vector3(pos, 0, 0) + initialPosition;
        x = Mathf.RoundToInt(pos / cell_width);
        x_abs = Mathf.Abs(x);
        claimableEarning = baseEarning * (5 - x_abs);
        claimEarningText.text = $"{claimableEarning}";



    }
}
