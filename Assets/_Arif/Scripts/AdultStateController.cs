using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using FRIA;

public class AdultStateController : MonoBehaviour
{
    //[SerializeField] private StartAnimation startAnimation;
    //private enum Gender { Boy, Girl }
    //[SerializeField] private Gender gender;
    [SerializeField] public AdultRigController[] allRigs;


    internal StateMate<AdultCharState> stateMate;
    internal AdultRigController rig;


    private void Awake()
    {
        stateMate = new StateMate<AdultCharState>(initial: AdultCharState.idle, enableDefinedTransitionOnly: false, loadEmpties: true);

        foreach (var item in allRigs)
        {
            if (item.gameObject.activeInHierarchy && rig == null)
            {
                rig = item;
            }
            else {
                item.gameObject.SetActive(false);
            }
        }
        rig.Init();

    }
    private void Start()
    {
        LevelManager.Instance.onLevelFinished += OnLevelFinished;
    }
    private void OnLevelFinished(bool levelSuccess)
    {
        if (levelSuccess)
        {
            SetState((AdultCharState.lose).ToString());
        }
        else
        {
            SetState((AdultCharState.win).ToString());
        }
    }

    public virtual void SetState(string state)
    {
        stateMate.SwitchState((AdultCharState) System.Enum.Parse(typeof(AdultCharState),state)) ;
    }
}

public enum AdultCharState { idle, chase, lose, win }
