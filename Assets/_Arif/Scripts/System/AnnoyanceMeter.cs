using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FRIA;

public class AnnoyanceMeter : MonoBehaviour
{

    public Transform annoyanceTicker;

    float xMin;
    float xMax;
    AnnoyanceController ac;
    Vector3 initialPos;
    void Start()
    {
        ac = GetComponent<AnnoyanceController>();
        ac.onAnnoyanceChanged += OnAnnoyanceChanged;
        ac.onProgessChanged += OnProgessChanged;

        initialPos = annoyanceTicker.localPosition;
        xMin = initialPos.x;
        xMax = -initialPos.x;
        LevelManager.Instance.ui.barFiller.fillAmount = 0;
    }
    void OnAnnoyanceChanged(float annoyance)
    {
        annoyanceTicker.localPosition = new Vector3(Mathf.Lerp(xMin,xMax,annoyance),initialPos.y,initialPos.z);
        if (annoyance >= 1)
        {
            StopAllCoroutines();
        }
    }

    void OnProgessChanged(float progress)
    {
        //LevelManager.Instance.ui.barFiller.fillAmount =progress;
        //(progress.ToString()).Debug("FF00FF");
        //if ((progress - targetP) > 0)
        //{
        //    float T = (progress - p) * timeToFillCompletely;
        //    (T.ToString()).Debug("FF00FF");
        //    if (barFiller != null) StopCoroutine(barFiller);
        //    barFiller = StartCoroutine(BarFillRoutine(T, progress));
        //}
    }

    //Coroutine barFiller;
    //float p;
    //float targetP;
    //IEnumerator BarFillRoutine(float T, float targetValue)
    //{
    //    float sT = Time.time;
    //    float startingP = p;
    //    targetP = targetValue;
    //    while (Time.time<sT+T)
    //    {
    //        float routineProg = Mathf.Clamp01((Time.time - sT)/T);
    //        p = Mathf.Lerp(startingP, targetP, routineProg);
    //        LevelManager.Instance.ui.barFiller.fillAmount = p;
    //        yield return null;
    //    }
    //    p = targetP;
    //    LevelManager.Instance.ui.barFiller.fillAmount = p;
    //}
}
