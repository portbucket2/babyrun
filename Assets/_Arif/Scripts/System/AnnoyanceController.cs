using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FRIA;

public class AnnoyanceController : MonoBehaviour
{
    public static AnnoyanceController Instance { get; private set; }
    public float annoyanceUpRate = 0.5f;
    public float annoyanceDownRate = 1f;
    
    public float progressRate = 0.5f;
    public int steps = 5;
    //public float completionTriggerDelay;
    public float winEventDelay;
    public float loseEventDelay;


    [Header("Debug")]
    [SerializeField] float annoyance = 0;
    [SerializeField] float progress = 0;

    public event System.Action onInitiated;
    public event System.Action<float> onProgessChanged;
    public event System.Action<float> onAnnoyanceChanged;
    public event System.Action<int> onProgressStepTaken;

    float achievedProgress;
    float delta;
    int i;
    bool annoyanceActive = false;
    private void Awake()
    {
        Instance = this; 
        delta = 1.0f / steps;
    }
    public void InitiateAnnoyance()
    {
        annoyanceActive = true;
        onInitiated?.Invoke();
    }

    private void Update()
    {
        if (annoyanceActive)
        {
            if (Input.GetMouseButton(0))
            {
                progress += progressRate * Time.deltaTime;
                annoyance += annoyanceUpRate * Time.deltaTime;
            }
            else
            {
                annoyance -= annoyanceDownRate * Time.deltaTime;
            }
            if (progress >= (i+1 ) * delta)
            {
                onProgressStepTaken?.Invoke(i);
                i++;
                achievedProgress = i * delta;
            }


            if (progress >= 1)
            {
                annoyanceActive = false;
                Centralizer.Add_DelayedMonoAct(this, TaskSuccess,winEventDelay);
            }
            if (annoyance >= 1)
            {
                annoyanceActive = false;
                Centralizer.Add_DelayedMonoAct(this, TaskFailed, loseEventDelay);
            }
            annoyance = Mathf.Clamp01(annoyance);
            progress = Mathf.Clamp01(progress);

            onProgessChanged?.Invoke(progress);
            onAnnoyanceChanged?.Invoke(annoyance);
        }
    }
    public void TaskSuccess()
    {
        LevelManager.Instance.ForceFillMeter();
    }
    public void TaskFailed()
    {
        LevelManager.Instance.ReportState(LevelState.Fail);
        LevelManager.Instance.CheckLevelState();
    }
}
