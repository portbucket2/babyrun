using DG.Tweening;
using System.Collections;
using UnityEngine;

public class AdultMotionController : MonoBehaviour
{
    [SerializeField] private float captureDistance = 0.3f;
    [SerializeField] private float chaseSpeed = 2f;
    [SerializeField] private float turnTime = 0.2f;
    [SerializeField] private float turnAngle = 0f;
    [SerializeField] private float walkSpeed = 1f;
    [SerializeField] private float turnLerpRate = 3f;
    private Transform adult { get { return this.transform; } }
    private Transform target;
    private AdultStateController stateController;
    private AdultRigController rig;
    private bool chasing = false;
    private void Start()
    {
        stateController = GetComponent<AdultStateController>();
        rig = stateController.rig;
        stateController.stateMate.AddStateEntryCallback(AdultCharState.chase, GetStartle);
        stateController.stateMate.AddStateEntryCallback(AdultCharState.lose, FailChase);
        stateController.stateMate.AddStateEntryCallback(AdultCharState.win, WinChase);

    }
    public virtual void GetStartle()
    {
        rig.ChangeFace(AdultFace.Surprised);
        rig.SetTrigger(Const.STARTLE);
        rig.PlayExclamation();
        Turn2Baby();
        DOVirtual.DelayedCall(2f, StartChase, false);
    }

    public virtual void StartChase()
    {
        chasing = true;
        target = PlayerController.Instance.transform;
        PlayerController.Instance.lookAtCameraOnFail = false;
        rig.ChangeFace(AdultFace.Angry);
        rig.SetTrigger(Const.RUN);
    }
    public virtual void FailChase()
    {
        rig.SetTrigger(Const.FAIL);
        target = null;
        chasing = false;
    }
    public virtual void WinChase()
    {
        rig.ChangeFace(AdultFace.Happy);
        rig.SetTrigger(Const.SUCCESS);
        rig.Grab(target);
        StartCoroutine(CaughtRoutine());
        target = null;
        chasing = false;
    }

    void Update()
    {
        ChaseBaby();
    }



    private Vector3 targetDirection;
    private void ChaseBaby()
    {
        if (!chasing) return;

        targetDirection = target.position - adult.position;

        if (targetDirection.sqrMagnitude > captureDistance * captureDistance)
        {
            adult.rotation = Quaternion.Slerp(adult.rotation, Quaternion.LookRotation(targetDirection, Vector3.up), turnLerpRate * Time.deltaTime);
            adult.Translate(adult.forward * Time.deltaTime * chaseSpeed, Space.World);
        }
        else
        {
            LevelManager.Instance.ReportState(LevelState.Fail);
            LevelManager.Instance.ForceFillMeter();
        }
    }


    private IEnumerator CaughtRoutine()
    {
        float elapsedTime = 0.0f;
        Vector3 direction = (Camera.main.transform.position.XZ() - transform.position).normalized;

        while (elapsedTime < 1.5f)
        {
            adult.Translate(direction * Time.deltaTime * walkSpeed, Space.World);
            adult.rotation = Quaternion.Slerp(adult.rotation, Quaternion.LookRotation(direction, Vector3.up), turnLerpRate * Time.deltaTime);
            elapsedTime += Time.deltaTime;
            yield return null;
        }
        rig.anim.enabled = false;
    }

    public void Turn2Baby() => adult.DOLocalRotate(new Vector3(0f, turnAngle, 0f), turnTime);
}
