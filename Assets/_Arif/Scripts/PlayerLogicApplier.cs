using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FRIA;

public class PlayerLogicApplier : MonoBehaviour
{
    //public static PlayerLogicApplier Instance { get; private set; }
    PlayerController pc;
    //private void Awake()
    //{
    //    Instance = this;
    //}
    private void Start()
    {
        pc = GetComponent<PlayerController>();
    }

    public void OnPickUpStanding(Transform targetObj)
    {
        pc.rig.ChangeFace(BabyFace.Happy);
        targetObj.SetPositionAndRotation(pc.rig.rightHand.position, pc.rig.rightHand.rotation);
        targetObj.SetParent(pc.rig.rightHand);
        pc.babyState = BabyState.Interested;
        pc.rb.LookAtXZ(transform);
        pc.rig.SetBabyAnimation(Const.PICK);
    }
}
