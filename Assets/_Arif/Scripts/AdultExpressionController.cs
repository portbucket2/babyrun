using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class AdultExpressionController : MonoBehaviour
{
    [SerializeField] private float turnTime = 0.2f;
    [SerializeField] private float turnAngle = 0f;
    private Transform adult { get { return this.transform; } }
    private AdultStateController stateController;
    private AdultRigController rig;
    private void Start()
    {
        stateController = GetComponent<AdultStateController>();
        rig = stateController.rig;
        stateController.stateMate.AddStateEntryCallback(AdultCharState.lose, Lose);
        stateController.stateMate.AddStateEntryCallback(AdultCharState.win, Win);

    }

    public virtual void Win()
    {
        //Turn2Baby();
        rig.ChangeFace(AdultFace.Happy);
        rig.SetTrigger(Const.SUCCESS);
    }

    public virtual void Lose()
    {
        rig.SetTrigger(Const.FAIL);
        rig.ChangeFace(AdultFace.Surprised);
        rig.PlayExclamation();
        //Turn2Baby();
    }
    public void Turn2Baby() => adult.DOLocalRotate(new Vector3(0f, turnAngle, 0f), turnTime);
}
