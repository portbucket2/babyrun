using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
public class AdultRigController : MonoBehaviour
{
    public event Action onAnimationGrab;
    [SerializeField] private GameObject[] parentFaces;
    [SerializeField] private Transform grabPoint;
    [SerializeField] private ParticleSystem exclamationParticle;
    private int currentFace = 0;
    internal Animator anim;
    // Start is called before the first frame update
    public void Init()
    {
        anim = GetComponent<Animator>();
        for (int i = 0; i < parentFaces.Length; i++)
        {
            parentFaces[i].SetActive(i==currentFace);
        }
    }

    public void PlayExclamation()
    {
        if (exclamationParticle)
        {
            exclamationParticle.Play();
        }        
    }

    public void ChangeFace(AdultFace index)
    {
        if (parentFaces.Length <= 0) return;
        parentFaces[currentFace].Inactive();
        currentFace = (int)index;
        parentFaces[currentFace].Active();
    }
    public void Grab(Transform tr)
    {
        tr.SetParent(grabPoint,true);
        tr.SetPositionAndRotation(grabPoint.position, grabPoint.rotation);
    }

    public void SetTrigger(string triggerName)
    {
        anim.SetTrigger(triggerName);
    }

    public void OnAnimationGrab()
    {
        onAnimationGrab?.Invoke();
    }
}

public enum AdultFace { Normal, Surprised, Happy, Angry }