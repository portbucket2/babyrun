using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ForwardFollower : MonoBehaviour
{
    public Transform forwardRef;
    public float lerpRate = 5;
    private void Update()
    {
        Quaternion targetRot =  Quaternion.LookRotation(forwardRef.forward, Vector3.up);
        this.transform.rotation = Quaternion.Lerp(targetRot, transform.rotation, lerpRate * Time.deltaTime);
    }

}
