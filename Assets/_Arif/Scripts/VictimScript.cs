using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using FRIA;

public class VictimScript : MonoBehaviour
{
    public AdultRigController rig;
    [SerializeField] private float turnTime = 0.2f;
    [SerializeField] private float turnAngle = 0f;
    [SerializeField] private float lerpRate = 2;

    public void Startle()
    {
        Turn2Baby();
        rig.PlayExclamation();
        rig.SetTrigger(Const.STARTLE);
        StartCoroutine(TrackBaby());
    }
    IEnumerator TrackBaby()
    {
        yield return new WaitForSeconds(0.5f);
        while (LevelManager.Instance.InPlayMode)
        {
            Quaternion lookQ =  Quaternion.LookRotation(PlayerController.Instance.transform.position - transform.position, Vector3.up);
            transform.rotation = Quaternion.Slerp(transform.rotation,lookQ,lerpRate*Time.deltaTime);
            yield return null;
        }
    }
    public void Turn2Baby() => transform.DOLocalRotate(new Vector3(0f, turnAngle, 0f), turnTime);
}
