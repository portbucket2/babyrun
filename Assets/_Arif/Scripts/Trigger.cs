using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Trigger : MonoBehaviour
{
    public bool interactable= true;
    public GameObject indicator;
    public UnityEvent onTriggerEnter;


    // Update is called once per frame
    private void OnTriggerEnter(Collider other)
    {
        if (!interactable || !LevelManager.Instance.InPlayMode) return;
        if (other.CompareTag(Const.PLAYER)) OnTrigger();
    }
    protected virtual void OnTrigger()
    {
        interactable = false;
        if (indicator)indicator.SetActive(false);
        onTriggerEnter.Invoke();
    }
}
