using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FRIA;
using System;
using TMPro;
#if UNITY_EDITOR
using UnityEditor;
#endif  
public class EnergyTracker : MonoBehaviour
{
    public const CurrencyType currencyType = CurrencyType.ENERGY;
    public const int ENERGY_CAP = 6;
    public const int AUTO_ENERGY_INTERVAL =5*60;
    public const int ENERGY_PER_LEVEL = 1;
    public static TimedItem energyAutoRefiller;

    public static Action<int> onEnergyChanged;
    public static bool hasEnergyToPlay => EnergyLeft >= ENERGY_PER_LEVEL;

    public static int EnergyLeft => Currency.Balance(currencyType);
    public static int EnergyRoom => ENERGY_CAP - EnergyLeft;

    static Coroutine energyRoutine;
    public GameObject root;
    public static Action onEnergyConsumed;

    public Animator lossEffectAnim;
    static void EnsureInit()
    {
        if (energyRoutine == null)
        {
            Centralizer.Init();
            energyRoutine = Centralizer.instance.StartCoroutine(EnergyRoutine());
        }
    }


    static private IEnumerator EnergyRoutine()
    {
        energyAutoRefiller = new TimedItem("TICK_LAST_ENERGY", AUTO_ENERGY_INTERVAL);
        while (true)
        {
            if (energyAutoRefiller.IsReadyToClaim)
            {
                int claimable =  Mathf.FloorToInt( energyAutoRefiller.ProgressMade);
                if (claimable < 0) claimable = 0;
                Currency.Transaction(currencyType, claimable > EnergyRoom ? EnergyRoom : claimable);
                onEnergyChanged?.Invoke(EnergyLeft);
                energyAutoRefiller.Claim();
            }
            yield return null;// new WaitForSeconds(1);
        }
    }
    public static bool ConsumeEnergy()
    {
        if (!BuildSpecManager.enableEnergySystem) return true;
        EnsureInit();
        if (EnergyLeft == 0) return false;
        Currency.Transaction(currencyType,-ENERGY_PER_LEVEL);
        //"energy consumed".Debug("FF00FF");
        onEnergyChanged?.Invoke(EnergyLeft);
        onEnergyConsumed?.Invoke();
        return true;
    }
    public static void RefillEnergy()
    {
        EnsureInit();
        int energyRoom = ENERGY_CAP - EnergyLeft;
        Currency.Transaction(currencyType, EnergyRoom);
    }

    private void Awake()
    {

        if (!BuildSpecManager.enableEnergySystem)
        {
            root.SetActive(false);
        }
        else
        {
            EnsureInit();
        }
    }
    private void Start()
    {
        fillbar.LoadValue(EnergyLeft, ENERGY_CAP, instant: true);
        onEnergyConsumed += () =>
        {
            if (lossEffectAnim) lossEffectAnim.SetTrigger("loss");
        };
    }

    public TextMeshProUGUI coinDisplayText;
    //public UnityEngine.UI.Image fillImage;
    public BarUI fillbar;
    //public float timeToAnimate = 1;
    //public float stepTime = 1 / 20.0f;

    //int savedValue;
    //int displayValue;
    //public bool pauseExternal;
    void ShowLossEffect()
    {
        //"lossEffect".Debug("FF00FF");
        lossEffectAnim.SetTrigger("loss");
    }

    void OnEnable()
    {
        //"on eneable".Debug("FF00FF");
        //savedValue = Currency.Balance(currencyType);
        //coinDisplayText.text = $"{EnergyLeft}/{ENERGY_CAP}";// displayValue.ToString();
        //fillImage.fillAmount = EnergyLeft / (float)ENERGY_CAP;
        //displayValue = savedValue;
        //if (runningTextRoutine != null)
        //{
        //    StopCoroutine(runningTextRoutine);
        //    runningTextRoutine = null;
        //}
        Currency.coinMan.AddListner_BalanceChanged(currencyType, OnCoinChange);
        onEnergyConsumed += ShowLossEffect;
    }
    void OnCoinChange()
    {
        //savedValue = Currency.Balance(currencyType);
        //if (runningTextRoutine != null) StopCoroutine(runningTextRoutine);

        //runningTextRoutine = StartCoroutine(TextRoutine());
        fillbar.LoadValue(EnergyLeft, ENERGY_CAP);
    }

    private void OnDisable()
    {
        //"on disable".Debug("FF00FF");
        Currency.coinMan.RemoveListner_BalanceChanged(currencyType, OnCoinChange);
        onEnergyConsumed -= ShowLossEffect;
    }

    //Coroutine runningTextRoutine;
    //IEnumerator TextRoutine()
    //{
    //    float startTime = Time.realtimeSinceStartup;
    //    int stepCount = Mathf.RoundToInt(timeToAnimate / this.stepTime);

    //    float stepTime = timeToAnimate / stepCount;
    //    int diff = savedValue - displayValue;
    //    int initialValue = displayValue;
    //    for (int i = 0; i < stepCount; i++)
    //    {
    //        while (pauseExternal)
    //        {
    //            yield return null;
    //            startTime += Time.deltaTime;
    //        }

    //        yield return new WaitForSeconds(stepTime);

    //        float p = Mathf.Clamp01((Time.realtimeSinceStartup - startTime) / timeToAnimate);
    //        displayValue = initialValue + Mathf.RoundToInt(diff * p);
    //        //coinDisplayText.text = $"{displayValue}/{ENERGY_CAP}";// displayValue.ToString();
    //        //fillImage.fillAmount = EnergyLeft / (float)ENERGY_CAP;
    //        //fillImage.fillAmount = EnergyLeft / (float)ENERGY_CAP;
    //        fillbar.LoadValue(EnergyLeft,ENERGY_CAP);
    //    }
    //    displayValue = savedValue;
    //    coinDisplayText.text = $"{EnergyLeft}/{ENERGY_CAP}";// displayValue.ToString();
    //}
}
#if UNITY_EDITOR
[CustomEditor(typeof(EnergyTracker))]
public class EnergyTrackerEditor : Editor
{
    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();
        EnergyTracker pct = (EnergyTracker)target;
        EditorGUILayout.BeginHorizontal();
        if (GUILayout.Button("+5"))
        {
            Currency.Transaction(EnergyTracker.currencyType, 5>EnergyTracker.EnergyRoom?EnergyTracker.EnergyRoom:5);
        }
        if (GUILayout.Button("-5"))
        {
            Currency.Transaction(EnergyTracker.currencyType, 5 > EnergyTracker.EnergyLeft ? -EnergyTracker.EnergyLeft : -5);
        }

        EditorGUILayout.EndHorizontal();
    }
}
#endif
