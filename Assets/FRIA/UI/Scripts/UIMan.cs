using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class UIMan : MonoBehaviour
{
    public static UIMan Instance { get; private set; }
    public GameObject successObj;
    public GameObject failObj;
    public Button restartButton;
    public Button nextButton;

    public Button inGameRestartButton;
    private void Awake()
    {
        Instance = this;
    }
    void Start()
    {

        inGameRestartButton.onClick.AddListener(() => {
            //AnalyticsAssistant.LevelRestart(GameLevelManager.Instance.LevelNumberCurrent);
            GameLevelManager.Instance.LoadCurrent();
        });
        restartButton.onClick.AddListener(() => {
            //AnalyticsAssistant.LevelRestart(GameLevelManager.Instance.LevelNumberCurrent);
            GameLevelManager.Instance.LoadCurrent();        
        });
        nextButton.onClick.AddListener(() => {
            GameLevelManager.Instance.LoadNext(true);
        });
        successObj.SetActive(false);
        failObj.SetActive(false);
        AnalyticsAssistant.LevelStarted(GameLevelManager.Instance.LevelNumberCurrent);

        GameStateManager.stateAccess.AddStateEntryCallback(GameState.Success, ShowSuccess);
        GameStateManager.stateAccess.AddStateEntryCallback(GameState.Fail, ShowFail);
    }

    public void ShowSuccess()
    {
        AnalyticsAssistant.LevelCompleted(GameLevelManager.Instance.LevelNumberCurrent);
        successObj.SetActive(true);
    }
    public void ShowFail()
    {
        AnalyticsAssistant.LevelFailed(GameLevelManager.Instance.LevelNumberCurrent);
        failObj.SetActive(true);
    }
}
