﻿#define CINE_CAMSWAP

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif


#if CINE_CAMSWAP
using Cinemachine;
namespace FRIA
{
    public class CamSwapController : MonoBehaviour
    {

        public static CamSwapController Instance { get; private set; }
        public bool willSetMainCamAtStart = true;
        public CinemachineBlendDefinition blend;
        public List<CamChoice> cams;

        CinemachineBrain cb;
        
        private void Awake()
        {
            Instance = this;
            cb = Camera.main.GetComponent<CinemachineBrain>();
            cb.m_DefaultBlend = blend;
        }
        private void Start() {
            
            if(willSetMainCamAtStart) SetCam(CamID.main);
        }
        public void SetCamIndex(int index)
        {
            bool found = false;
            for (int i = 0; i < cams.Count; i++)
            {

                cams[i].vCam.Priority = (index == i) ? 10 : 1;
                found = true;
            }

            if (!found)
            {
                Debug.LogErrorFormat("Cam index not found! {0}", index);
            }
        }
        public void SetCam(CamID id)
        {
            bool found=false;
            for (int i = 0; i < cams.Count; i++)
            {
                if (cams[i].id == id)
                {
                    found = true;
                    cams[i].vCam.Priority =10;
                }
                else
                {
                    cams[i].vCam.Priority = 1;
                }
            }

            if (!found)
            {
                Debug.LogErrorFormat("Cam not found! {0}",id);
            }
        }

        public static void SetCamera(CamID id)
        {
            Instance.SetCam(id);
        }
    }
    [System.Serializable]
    public class CamChoice
    {
        public Cinemachine.CinemachineVirtualCamera vCam;
        public CamID id;
    }
    public enum CamID
    {
        preinitial = -1,
        main = 0,
        action_1 = 1,
        action_2 = 2,
        celebration = 11,
        deathcam = 12,
    }

#if UNITY_EDITOR
    [CustomPropertyDrawer(typeof(CamChoice))]
    public class CamChoiceEditor : PropertyDrawer
    {
        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            EditorGUIUtility.labelWidth = 0;
            label.text = string.Format("");

            EditorGUI.BeginProperty(position, label, property);

            position = EditorGUI.PrefixLabel(position, GUIUtility.GetControlID(FocusType.Passive), label);

            var indent = EditorGUI.indentLevel;
            EditorGUI.indentLevel = 0;

            float elementHeight = position.height;
            var enabledRect = new Rect(position.x, position.y, 80, elementHeight);
            var numberRect = new Rect(position.x + 80, position.y, position.width - 80, elementHeight);

            EditorGUI.PropertyField(enabledRect, property.FindPropertyRelative("id"), GUIContent.none);
            EditorGUI.PropertyField(numberRect, property.FindPropertyRelative("vCam"), GUIContent.none);

            EditorGUI.indentLevel = indent;


            EditorGUI.EndProperty();

        }

        //public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
        //{
        //    return base.GetPropertyHeight(property, label) * 2 + 10;
        //}

    }
#endif
}
#endif