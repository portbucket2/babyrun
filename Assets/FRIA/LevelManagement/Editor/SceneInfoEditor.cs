﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(BuildSceneOrganizer))]
public class SceneInfoEditor : Editor
{
    public void OnEnable()
    {
        BuildSceneOrganizer bsi = (BuildSceneOrganizer)target;
        LevelData.currentFocus = bsi;
    }

    public override void OnInspectorGUI()
    {
        EditorGUI.BeginDisabledGroup(true);
        EditorGUILayout.PropertyField(serializedObject.FindProperty("m_Script"));
        EditorGUI.EndDisabledGroup();
        BuildSceneOrganizer bsi = (BuildSceneOrganizer)target;
        serializedObject.Update();
        //EditorGUILayout.BeginHorizontal();
        if (bsi.sequenceMode == SequenceMode.CSV)
        {
            GUILayout.Label(string.Format("Current Mode: CSV"));
            if (GUILayout.Button("Switch To local Build", GUILayout.Height(30)))
            {
                bsi.sequenceMode = SequenceMode.LOCAL;
                EditorFix.SetObjectDirty(bsi);
            }
        }
        else if (bsi.sequenceMode == SequenceMode.LOCAL)
        {
            GUILayout.Label(string.Format("Current Mode: Local"));
            if (GUILayout.Button("Switch To CSV Build", GUILayout.Height(30)))
            {
                bsi.sequenceMode = SequenceMode.CSV;
                bool csv0_isMissing = bsi.csvList[0] == null;
                bsi.showData = csv0_isMissing;
                if(!csv0_isMissing) bsi.LoadFromCSV();

                EditorFix.SetObjectDirty(bsi);
            }
        }
        //EditorGUILayout.EndHorizontal();


        //EditorGUILayout.PropertyField(serializedObject.FindProperty("sequenceMode"));
        //EditorGUILayout.PropertyField(serializedObject.FindProperty("forceCSVChoice"));

        GUILayout.Space(5);
        if (bsi.sequenceMode == SequenceMode.CSV)
        {
            EditorGUILayout.PropertyField(serializedObject.FindProperty("csvBox"));
            EditorGUILayout.PropertyField(serializedObject.FindProperty("csvList"));

            EditorGUILayout.BeginHorizontal();
            if (GUILayout.Button("Download CSV", GUILayout.Height(25)))
            {
                bsi.DownloadCSV();
                EditorFix.SetObjectDirty(bsi);
            }
            if (GUILayout.Button("Load From CSV", GUILayout.Height(25)))
            {
                if (bsi.sequenceMode == SequenceMode.LOCAL)
                {
                    if (EditorUtility.DisplayDialog("Switch to Standard build?",
                        "Unsaved sequence data will be lost. Are you sure?", "Accept", "Cancel"))
                    {
                        bsi.sequenceMode = SequenceMode.CSV;
                        bsi.LoadFromCSV();
                    }
                }
                else
                {
                    bsi.sequenceMode = SequenceMode.CSV;
                    bsi.LoadFromCSV();
                }
                EditorFix.SetObjectDirty(bsi);

            }
            EditorGUILayout.EndHorizontal();

            EditorGUILayout.PropertyField(serializedObject.FindProperty("showData"));
            if (bsi.showData) Show(serializedObject.FindProperty("levels"), bsi);
        }
        else if (bsi.sequenceMode == SequenceMode.LOCAL)
        {

            Show(serializedObject.FindProperty("levels"), bsi);
        }





        serializedObject.ApplyModifiedProperties();

        //EditorGUILayout.BeginHorizontal();
        //if (GUILayout.Button("Load From Build Settiings", GUILayout.Height(40)))
        //{

        //    sc.LoadFromBuildSettings();
        //}
        //if (GUILayout.Button("Save to Build Settiings", GUILayout.Height(40)))
        //{
        //    if (EditorUtility.DisplayDialog("Save Build Settings?", "This action will overwrite current list of scenes added to build settings. Are you sure?", "Confirm", "Cancel"))
        //    {
        //        sc.SaveToBuildSettings();
        //    }
        //}
        //EditorGUILayout.EndHorizontal();



    }

    public static void Show(SerializedProperty list, BuildSceneOrganizer bsi)
    {
        EditorGUILayout.Space();
        EditorGUI.indentLevel += 1;


        //if (GUILayout.Button("Sort by Order"))
        //{
        //    scinfo.Sort(false);
        //}
        //if (GUILayout.Button("Sort by ID"))
        //{
        //    scinfo.Sort();// true);
        //}
        if (bsi.sequenceMode == SequenceMode.LOCAL)
        {
            EditorGUILayout.BeginHorizontal();
            if (GUILayout.Button("Check All"))
            {
                bsi.SetAll(true);
                EditorFix.SetObjectDirty(bsi);
            }
            if (GUILayout.Button("Uncheck All"))
            {
                bsi.SetAll(false);
                EditorFix.SetObjectDirty(bsi);
            }
            if (GUILayout.Button("Clear List"))
            {
                bsi.levels.Clear();
                EditorFix.SetObjectDirty(bsi);
            }
            EditorGUILayout.EndHorizontal();
            if (GUILayout.Button("(+)Add"))
            {
                bsi.levels.Insert(0, new LevelData(true, "1"));
                EditorFix.SetObjectDirty(bsi);
            }
        }




        EditorGUILayout.BeginHorizontal();
        GUILayout.Label("", GUILayout.Width(10));
        GUILayout.Label("Serial", GUILayout.Width(105));
        GUILayout.Label("Is Bonus?", GUILayout.Width(65));
        GUILayout.Label("ID", GUILayout.Width(30));
        GUILayout.Label("Scene", GUILayout.Width(100));
        EditorGUILayout.EndHorizontal();
        List<string> duplicateCheckList = new List<string>();
        for (int i = 0; i < list.arraySize; i++)
        {
            LevelData ld = bsi.levels[i];
            EditorGUILayout.BeginVertical("Box");
            EditorGUILayout.PropertyField(list.GetArrayElementAtIndex(i));
            SceneAsset scAsset = AssetDatabase.LoadAssetAtPath<SceneAsset>(BuildSceneOrganizer.IDtoPath(ld.id));
            ld.scene = scAsset;
            EditorGUI.indentLevel -= 1;
            if (!scAsset) EditorGUILayout.HelpBox(string.Format("Level Not Found: Assets/Scenes/Level{0}", ld.id), MessageType.Error);
            else
            {
                if (duplicateCheckList.Contains(ld.id))
                {
                    EditorGUILayout.HelpBox(string.Format("Duplicate Entry Detected: {0}", ld.id), MessageType.Warning);
                }
                else
                {
                    duplicateCheckList.Add(ld.id);
                }
            }
            EditorGUI.indentLevel += 1;
            EditorGUILayout.EndVertical();

        }

        EditorGUI.indentLevel -= 1;
        EditorGUILayout.Space(); 
        if (bsi.sequenceMode == SequenceMode.LOCAL)
        {
            if (GUILayout.Button("Save to Build"))
            {
                bsi.SaveToBuildSettings(false);
                EditorFix.SetObjectDirty(bsi);
            }
        }
    }
}