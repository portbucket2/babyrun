﻿using UnityEngine;
using FRIA;
#if UNITY_EDITOR
using UnityEditor;
#endif

[System.Serializable]
public class LevelData
{
    public static ILevelDataContainer currentFocus;
    public bool enabled;
    public bool isBonus;

    public string id;
    //public int idd;

    public string sceneName
    {
        get 
        {
            return string.Format("Level_{0}",id);
        }
    }


    public HardData<bool> _bonusLockHD;
    public void UnlockBonusLevel()
    {
        if (_bonusLockHD == null)
        {
            _bonusLockHD = new HardData<bool>(string.Format("BONUS_LOCKED_{0}", id), false);
        }
        _bonusLockHD.value = false;
    }
    public bool isBonusLocked {
        get
        {
            if (!isBonus) return false;
            if (_bonusLockHD==null)
            {
                _bonusLockHD = new HardData<bool>(string.Format("BONUS_LOCKED_{0}",id), true);
            }
            return _bonusLockHD.value;
        }
    }


#if UNITY_EDITOR
    public SceneAsset scene;
#endif
    public LevelData() { }
    public LevelData(bool enabled, string id)
    {
        this.enabled = enabled;
        this.id = id;
    }
}

public interface ILevelDataContainer
{
    void RemoveID(string id);
    void AddElement(string id);

    int FindSerial(string id);
}
