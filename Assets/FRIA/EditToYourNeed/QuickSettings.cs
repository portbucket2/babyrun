﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QuickSettings : ScriptableObject
{
    [SerializeField] bool _decorationSharedMaterial = false;
    [SerializeField] bool _paintModeEnabled = false;
    [SerializeField] bool enableSampleInMain = false;

    public bool editorPaintMode
    {
        get
        {
#if UNITY_EDITOR
            return _paintModeEnabled;
#else
            return false;
#endif
        }
    }
    public bool decorationSharedMaterial
    {
        get
        {
#if UNITY_EDITOR
            return _decorationSharedMaterial;
#else
            return false;
#endif
        }
    }

    public bool enableSampleInMainView
    {
        get
        {
#if UNITY_EDITOR
            return enableSampleInMain;
#else
            return false;
#endif
        }
    }
    public bool showEjectLog = false;
    public bool drawDebugRays = false;
}
