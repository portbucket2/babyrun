using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using System.Text.RegularExpressions;
using UnityEngine.SceneManagement;
using System;
using FRIA;

public static class TimeManager
{
    public const long TicksPerSecond = 10000000;
    public static long ticks
    {
        get
        {
            return DateTime.Now.Ticks;
        }
    }

    public static bool IsItTimeYet(long lastClaimed, long period)
    {
        //$"time yet {ticks} >= {lastClaimed} + {period}".Debug("FF0000");
        return (ticks >= lastClaimed + period);
    }

    public static float ProgressMade(long lastClaimed, long period)
    {
        //$"time yet {ticks} - {lastClaimed} / {period} = {((ticks - lastClaimed) / (float)period)}".Debug("FF0000");

        return ((ticks - lastClaimed) / (float)period);
    }

    public static float SecondsToNext(long lastClaimed, long period)
    {
        return  (period - (ticks - lastClaimed))/TicksPerSecond;
    }

}

public class TimedItem
{
    public string timer_key;
    public int period_seconds;
    
    public long periodTicks => period_seconds * TimeManager.TicksPerSecond;
    public bool IsReadyToClaim => TimeManager.IsItTimeYet(lastClaimed.value, periodTicks);
    public float TimeRemaining_Seconds => TimeManager.SecondsToNext(lastClaimed.value, periodTicks);
    public string TimeRemaining_String => TimeString.FromSeconds_0toMAX(TimeRemaining_Seconds);
    public float ProgressMade => TimeManager.ProgressMade(lastClaimed.value, periodTicks);

    HardData<long> lastClaimed;
    public TimedItem()
    {
        lastClaimed = new HardData<long>(timer_key, 0);
    }
    public TimedItem(string key, int period_sec, long initial_value = 0)
    {
        timer_key = key;
        period_seconds = period_sec;
        lastClaimed = new HardData<long>(timer_key, initial_value);
    }
    public void Claim()
    {
        lastClaimed.value = TimeManager.ticks;
    }
}