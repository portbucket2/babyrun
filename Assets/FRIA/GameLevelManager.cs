﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FRIA;
using UnityEngine.SceneManagement;

public class GameLevelManager : MonoBehaviour
{
    public static GameLevelManager Instance;
    public static System.Action<int> onLevelLoaded;

    HardData<int> currentLevelIndex;
    HardData<int> lastUnlockedIndex;

    public int LevelNumberCurrent { get { return currentLevelIndex.value + 1; } }
    public int LevelNumberLatest { get { return lastUnlockedIndex.value + 1; } }

    void Awake()
    {
        if (Instance)
        {
            Destroy(this.gameObject);
        }
        else
        {
            Instance = this;
            //Debug.Log(GameLevelManager.Instance);
            this.transform.SetParent(null);
            DontDestroyOnLoad(this.gameObject);
            currentLevelIndex = new HardData<int>("CURRENT_LEVEL_INDEX", 0);
            lastUnlockedIndex = new HardData<int>("LAST_UNLOCKED_INDEX", 0);

            OnAwake();
        }
    }


    protected virtual void OnAwake()
    {
    }


    public void UnlockNextLevel()
    {
        //Debug.LogFormat("UnlockNext A: {0}/{1}",currentLevelIndex.value ,lastUnlockedIndex.value);
        if (lastUnlockedIndex.value == currentLevelIndex.value)
        {
            lastUnlockedIndex.value++;
        }
        //Debug.LogFormat("UnlockNext B: {0}/{1}", currentLevelIndex.value, lastUnlockedIndex.value);
    }
    /// <summary>
    /// Loads next level
    /// </summary>
    /// <param name="forceUnlock">setting true will unlock level without explicitly reporting current level completion</param>
    public void LoadNext(bool forceUnlock)
    {
        //Debug.LogFormat("NEXT A: {0}/{1} force {2}", currentLevelIndex.value, lastUnlockedIndex.value,forceUnlock);
        if (forceUnlock)
        {
            currentLevelIndex.value++;
            if (lastUnlockedIndex.value < currentLevelIndex) lastUnlockedIndex.value++;
            //Debug.LogFormat("NEXT B: {0}/{1} force {2}", currentLevelIndex.value, lastUnlockedIndex.value, forceUnlock);
            LoadLevelOnIndex(currentLevelIndex.value);
        }
        else
        {
            if (lastUnlockedIndex.value >= currentLevelIndex.value + 1)
            {
                currentLevelIndex.value++;
                //Debug.LogFormat("NEXT C: {0}/{1} force {2}", currentLevelIndex.value, lastUnlockedIndex.value, forceUnlock);
                LoadLevelOnIndex(currentLevelIndex.value);
            }
            else
            {
                Debug.LogError("Next level unlock not recorded. try forceUnlock=true if you dont plan to unlock manually ");
                //Debug.LogFormat("NEXT D: {0}/{1} force {2}", currentLevelIndex.value, lastUnlockedIndex.value, forceUnlock);
            }
        }
    }
    public void LoadCurrent()
    {
        LoadLevelOnIndex(currentLevelIndex.value);
    }
    public void LoadLatest()
    {
        LoadLevelOnIndex(lastUnlockedIndex.value);
    }

    private const int MAX_SCENE_COUNT = 1;
    public virtual int GetBuildIndexForLevelIndex(int levelIndex)
    {
        Debug.LogErrorFormat("Max scene count is capped at {0}", MAX_SCENE_COUNT);

        return (levelIndex % MAX_SCENE_COUNT);
    }
    public void LoadLevelOnIndex(int index)
    {
        currentLevelIndex.value = index;
        SceneManager.LoadScene(GetBuildIndexForLevelIndex(index));
        onLevelLoaded?.Invoke(LevelNumberCurrent);

    }
}


