using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace FRIA
{
    public static class MonoBehaviourExtender
    {
        public static void Add_DelayedAct(this MonoBehaviour mono, System.Action act, float delay)
        {
            Centralizer.Add_DelayedMonoAct(mono,act,delay);
        }
    }

}
