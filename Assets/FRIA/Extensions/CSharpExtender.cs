using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

namespace FRIA
{
    public static class CSharpExtender
    {
        public static void Iter(this Action<int> f, int start_i, int end_i)
        {
            int inc = (start_i <= end_i) ? +1 : -1;
            Func<int, bool> cond = (int i) =>
            {
                if (inc > 0) return i <= end_i;
                else return i >= end_i;
            };
            for (int i = start_i; cond(i); i += inc)
            {
                f?.Invoke(i);
            }
        }
        public static IEnumerator Iter(Func<int, IEnumerator> f, int start_i, int end_i)
        {
            int inc = (start_i <= end_i) ? +1 : -1;
            Func<int, bool> cond = (int i) =>
            {
                if (inc > 0) return i <= end_i;
                else return i >= end_i;
            };
            for (int i = start_i; cond(i); i += inc)
            {
                yield return f?.Invoke(i);
            }
        }
    }
}