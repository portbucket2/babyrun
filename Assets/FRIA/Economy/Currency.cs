﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class Currency 
{
    public static CurrencyManager<CurrencyType> _coinMan;
    public static CurrencyManager<CurrencyType> coinMan
    {
        get
        {
            if (_coinMan == null)
            {
                _coinMan = new CurrencyManager<CurrencyType>();
                _coinMan.EnsureInit(CurrencyType.COIN, BuildSpecManager.initialCoinBonus);
                _coinMan.EnsureInit(CurrencyType.LVL, 1);
                _coinMan.EnsureInit(CurrencyType.ENERGY, EnergyTracker.ENERGY_CAP);
            }
            return _coinMan;
        }
    }


    public static int Balance(CurrencyType type)
    {
        return coinMan.GetBalance(type);
    }
    public static void Transaction(CurrencyType type, int value)
    {
        if (BuildSpecManager.disableCoinReduction && value < 0) return;
        coinMan.ChangeBy(type, value);
    }
}
public enum CurrencyType
{
    COIN = 0,
    XP = 1,
    LVL =2,
    ENERGY = 3,
}
