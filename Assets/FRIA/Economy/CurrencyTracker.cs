﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using TMPro;

#if UNITY_EDITOR
using UnityEditor;
#endif  
public class CurrencyTracker : MonoBehaviour
{
    public TextMeshProUGUI coinDisplayText;
    public CurrencyType currencyType;
    public float timeToAnimate = 1;
    public float stepTime = 1/20.0f;

    int savedValue;
    int displayValue;
    public bool pauseExternal;
    void OnEnable()
    {
        savedValue = Currency.Balance(currencyType);
        coinDisplayText.text = savedValue.ToString();
        displayValue = savedValue;
        if (runningTextRoutine != null)
        {
            StopCoroutine(runningTextRoutine);
            runningTextRoutine = null;
        }
        Currency.coinMan.AddListner_BalanceChanged(currencyType, OnCoinChange);
    }
    void OnCoinChange()
    {
        savedValue = Currency.Balance(currencyType);
        if (runningTextRoutine != null) StopCoroutine(runningTextRoutine);
        runningTextRoutine = StartCoroutine(TextRoutine());
    }

    private void OnDisable()
    {
        Currency.coinMan.RemoveListner_BalanceChanged(currencyType, OnCoinChange);
    }

    Coroutine runningTextRoutine;
    IEnumerator TextRoutine()
    {
        float startTime = Time.realtimeSinceStartup;
        int stepCount = Mathf.RoundToInt( timeToAnimate / this.stepTime);

        float stepTime = timeToAnimate / stepCount;
        int diff = savedValue - displayValue;
        int initialValue = displayValue;
        for (int i = 0; i < stepCount; i++)
        {
            while (pauseExternal)
            {
                yield return null;
                startTime += Time.deltaTime;
            }

            yield return new WaitForSeconds(stepTime);

            float p = Mathf.Clamp01((Time.realtimeSinceStartup - startTime) / timeToAnimate);
            displayValue = initialValue +  Mathf.RoundToInt(diff * p);
            coinDisplayText.text = displayValue.ToString();
        }
        displayValue = savedValue;
        coinDisplayText.text = displayValue.ToString();
    }
}


#if UNITY_EDITOR
[CustomEditor(typeof(CurrencyTracker))]
public class CurrencyTrackerEditor : Editor
{
    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();
        CurrencyTracker pct = (CurrencyTracker)target;
        EditorGUILayout.BeginHorizontal();
        if (GUILayout.Button("+200"))
        {
            Currency.Transaction(pct.currencyType, 200);
        }
        if (GUILayout.Button("-100"))
        {
            Currency.Transaction(pct.currencyType, -100);
        }

        EditorGUILayout.EndHorizontal();
    }
}
#endif
