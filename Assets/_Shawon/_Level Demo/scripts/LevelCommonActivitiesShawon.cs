using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class LevelCommonActivitiesShawon : MonoBehaviour
{
    public Animator babyAnim;
    public Animator parentAnim;
    public GameObject playerObj;
    public Action actionOnLevelSuccess;
    public Action actionOnLevelFail;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void LevelEndSuccessForcefully()
    {
        LevelManager.Instance.ReportState(LevelState.Success);
        LevelManager.Instance.ForceFillMeter();

    }
    public void ForceFillMeter()
    {
        LevelManager.Instance.ForceFillMeter();
    }
    public void ParentEndChase()
    {
        ParentController.Instance.EndChase(false);
    }
    IEnumerator LevelEndSuccessForcefullyCR(float f)
    {
        yield return new WaitForSeconds(f);
        LevelEndSuccessForcefully();
    }
    IEnumerator LevelEndFailForcefullyCR(float f)
    {
        yield return new WaitForSeconds(f);
        ParentController.Instance.OnBabyCaught();
        //LevelManager.Instance.ReportState(LevelState.Fail);
    }
    public void LevelEndSuccessForcefullyDelayed(float f)
    {
        StartCoroutine(LevelEndSuccessForcefullyCR(f));
    }
    public void LevelEndFailForcefullyDelayed(float f)
    {
        StartCoroutine(LevelEndFailForcefullyCR(f));
    }
    public void ParentGetStartle()
    {
        ParentController.Instance.GetStartle();
        
    }
    public void LevelMeterFillStart()
    {
        LevelManager.Instance.MeterFiller();
    }
    public void LevelFinishedCustomCallback(bool suc)
    {
        if (suc)
        {
            actionOnLevelSuccess?.Invoke();
            Debug.Log("success");
        }
        else
        {
            actionOnLevelFail?.Invoke();
            Debug.Log("fail");
        }
    }

    public void MakeBabyMovementControlDisable()
    {
        //playerObj.GetComponent<MovementHandler>().enabled = false;
        //playerObj.GetComponent<PlayerController>().enabled = false;
        playerObj.GetComponent<PlayerController>().inputEnabled = false;
        playerObj.GetComponent<Rigidbody>().useGravity = false;
        playerObj.GetComponent<Rigidbody>().isKinematic = true;

        //playerObj.GetComponent<MovementHandler>().moveDirection = Vector3.zero;
    }
    public void MakeBabyMovementControlEnable()
    {
        //playerObj.GetComponent<MovementHandler>().enabled = true;
        //playerObj.GetComponent<PlayerController>().enabled = true;
        playerObj.GetComponent<PlayerController>().inputEnabled = true;
        playerObj.GetComponent<Rigidbody>().useGravity = true;
        playerObj.GetComponent<Rigidbody>().isKinematic = false;

        //playerObj.GetComponent<MovementHandler>().SetMoveRotation(Vector3.zero);
    }

}
