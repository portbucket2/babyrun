using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickItemShawon : Task
{
    public static PickItemShawon currentItem;
    [SerializeField] private bool connectToSystem = true;
    [SerializeField] internal GrabableItem rightHandItem;
    [SerializeField] internal GrabableItem leftHandItem;


    public override void OnInteract()
    {

        
        if (currentItem) return;
        currentItem = this;

        PlayerController.Instance.PickupBehavior(this.transform, pickType, HoldItem);
        if (!connectToSystem) return;

        levelManager.MeterFiller();
        if (parentController) parentController.GetStartle();

    }

    #region OnStart
    //private PlayerController playerController;
    private ParentController parentController;
    private LevelManager levelManager;


    [SerializeField] private PickType pickType;

    public override void OnStart()
    {
        //playerController = PlayerController.Instance;
        parentController = ParentController.Instance;
        levelManager = LevelManager.Instance;
    }
    #endregion

    private void HoldItem()
    {
        PlayerController.Instance.SetHolding();
        if (rightHandItem)
        {
            rightHandItem.HoldItem(false);
        }
        if (leftHandItem)
        {
            leftHandItem.HoldItem(true);
        }
        LevelManager.Instance.onLevelFinished += (bool success) =>
        {
            DropItems(null, 0);
        };
    }

    public void DropItems(Transform target, float angle)
    {
        if (!currentItem) return;
        if (rightHandItem)
        {
            rightHandItem.DropItem(target, angle);
        }
        if (leftHandItem)
        {
            leftHandItem.DropItem(target, angle);
        }
        PlayerController.Instance.rig.SetBabyAnimation(Const.HOLDING, false);
        currentItem = null;
    }

    public void LevelEndForcefully()
    {
        levelManager.ReportState(LevelState.Fail);
        levelManager.ForceFillMeter();

    }
}
