using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ShawonGameTools;

public class ObjectTransitionToTargetPos : MonoBehaviour
{
    public bool localPosMode;
    Vector3 fromPos;
    Quaternion fromRot;
    public Vector3 toPos;
    Quaternion toRot;
    Vector3 fromRotVec;
    public Vector3 toRotVec;
    public GameObject objToTransition;
    
    public AnimationCurve transitionCurve;
    public AnimationCurve addedY;
    public float addedYMul ;
    public AnimationCurve addedZ;
    public float addedZMul;
    public AnimationCurve addedX;
    public float addedXMul;
    
    public float transitionSpeed = 1;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    public void TransitionStart(float delay = 0)
    {
        StartCoroutine(TransitionStartCR(delay));
    }
    IEnumerator TransitionStartCR(float f)
    {
        yield return new WaitForSeconds(f);
        if (localPosMode)
        {
            fromPos = objToTransition.transform.localPosition;
        }
        else
        {
            fromPos = objToTransition.transform.position;
        }



        fromRot = objToTransition.transform.localRotation;
        fromRotVec = Quaternion.ToEulerAngles(fromRot) * Mathf.Rad2Deg;



        SmoothGainValuesManager.instance.TransitionValueSetTo(0, 1, null, TransitionUpdate, null, transitionSpeed);
    }
    //public void TransitionStartDelayed(float delay)
    //{
    //    TransitionStart(delay);
    //}
    public void TransitionStartToTargetGo(GameObject go, bool followRot = true)
    {
        if (localPosMode)
        {
            fromPos = objToTransition.transform.localPosition;
        }
        else
        {
            fromPos = objToTransition.transform.position;
        }

        toPos = go.transform.position;
        if (followRot)
        {
            toRotVec = Quaternion.ToEulerAngles(go.transform.rotation) * Mathf.Rad2Deg;
        }
       

        fromRot = objToTransition.transform.localRotation;
        fromRotVec = Quaternion.ToEulerAngles(fromRot) * Mathf.Rad2Deg;



        SmoothGainValuesManager.instance.TransitionValueSetTo(0, 1, null, TransitionUpdate, null, transitionSpeed);
    }
    public void TransitionUpdate(float f)
    {
        if (localPosMode)
        {
            objToTransition.transform.localPosition = Vector3.LerpUnclamped(fromPos, toPos, transitionCurve.Evaluate(f)) + new Vector3(addedX.Evaluate(f) * addedXMul, addedY.Evaluate(f) * addedYMul, addedZ.Evaluate(f)) * addedZMul;
        }
        else
        {
            objToTransition.transform.position = Vector3.LerpUnclamped(fromPos, toPos, transitionCurve.Evaluate(f)) + new Vector3(0, addedY.Evaluate(f) * addedYMul, addedZ.Evaluate(f)) * addedZMul;
        }
        
        //objToTransition.transform.localRotation = Quaternion.LerpUnclamped(fromRot, toRot, transitionCurve.Evaluate(f));
        objToTransition.transform.localRotation = Quaternion.Euler(Vector3.Lerp( fromRotVec, toRotVec, transitionCurve.Evaluate(f)));
    }
}
