using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ShawonGameTools;

public class ObjectTransitionToTargetGo : MonoBehaviour
{
    //public bool localPosMode;
    Vector3 fromPos;
    Quaternion fromRot;
    Vector3 fromRotVec;
    Vector3 fromScale;
    public Vector3 toPos;
    public bool posUpdateCustom;


    public Vector3 toRotVec;
    public Vector3 toScale;
    public GameObject objToTransition;
    public GameObject targetGo;

    public AnimationCurve transitionCurve;
    public AnimationCurve addedY;
    public float addedYMul;
    public AnimationCurve addedZ;
    public float addedZMul;
    public AnimationCurve addedX;
    public float addedXMul;

    public float transitionSpeed = 1;
    
    public bool rotUpdate;
    public bool scaleUpdate;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    public void TransitionStart(float delay = 0)
    {
        StartCoroutine(TransitionStartCR(delay));
    }
    IEnumerator TransitionStartCR(float f)
    {
        yield return new WaitForSeconds(f);
        TransitionStartToTargetGo();
    }
    
    public void TransitionStartToTargetGo()
    {
        fromPos = objToTransition.transform.position;
        if (posUpdateCustom)
        {

        }
        else
        {
            toPos = targetGo.transform.position;
        }
        

        fromRot = objToTransition.transform.localRotation;
        fromRotVec = Quaternion.ToEulerAngles(fromRot) * Mathf.Rad2Deg;
        if (rotUpdate)
        {
            
        }
        else
        {
            toRotVec = fromRotVec;
        }
        fromScale = objToTransition.transform.localScale;
        if (scaleUpdate)
        {
            
        }
        else
        {
            toScale = fromScale;
        }

        SmoothGainValuesManager.instance.TransitionValueSetTo(0, 1, null, TransitionUpdate, null, transitionSpeed);
    }
    public void TransitionUpdate(float f)
    {
        if(targetGo != null)
        {
            toPos = targetGo.transform.position;
        }
        
        //objToTransition.transform.localPosition = Vector3.LerpUnclamped(fromPos, toPos, transitionCurve.Evaluate(f)) + new Vector3(addedX.Evaluate(f) * addedXMul, addedY.Evaluate(f) * addedYMul, addedZ.Evaluate(f)) * addedZMul;
        objToTransition.transform.position = Vector3.LerpUnclamped(fromPos, toPos, transitionCurve.Evaluate(f)) + new Vector3(addedX.Evaluate(f) * addedXMul, addedY.Evaluate(f) * addedYMul, addedZ.Evaluate(f)) * addedZMul;
        //objToTransition.transform.localRotation = Quaternion.LerpUnclamped(fromRot, toRot, transitionCurve.Evaluate(f));
        objToTransition.transform.localRotation = Quaternion.Euler(Vector3.Lerp(fromRotVec, toRotVec, transitionCurve.Evaluate(f)));
        objToTransition.transform.localScale = Vector3.Lerp(fromScale, toScale, transitionCurve.Evaluate(f));

    }
}
