using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjAnimTrigger : MonoBehaviour
{
    public Animator anim;
    public enum TriggerType
    {
        Trigger, Bool, Float
    }
    public TriggerType triggerType;
    
    public string trigger;
    public bool onOff;
    public float floatVal;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void AnimTriggerHappen(float delay)
    {
        StartCoroutine(AnimTriggerCR(delay));
    }
    
    IEnumerator AnimTriggerCR(float f)
    {
        yield return new WaitForSeconds(f);

        
        if (anim != null)
        {
            switch (triggerType)
            {
                case TriggerType.Trigger:
                    {
                        anim.SetTrigger(trigger);
                        break;
                    }
                case TriggerType.Bool:
                    {
                        anim.SetBool(trigger, onOff);
                        break;
                    }
                case TriggerType.Float:
                    {
                        anim.SetFloat(trigger, floatVal);
                        break;
                    }
            }
        }
        
    }
}
