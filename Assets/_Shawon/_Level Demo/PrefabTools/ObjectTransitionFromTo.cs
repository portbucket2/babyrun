using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ShawonGameTools;

public class ObjectTransitionFromTo : MonoBehaviour
{
    public bool localPosMode;
    Vector3 fromPos;
    Quaternion fromRot;
    Vector3 toPos;
    Quaternion toRot;
    Vector3 fromRotVec;
    Vector3 toRotVec;
    public GameObject objToTransition;
    
    public AnimationCurve transitionCurve;
    public AnimationCurve addedY;
    public float addedYMul ;
    public AnimationCurve addedZ;
    public float addedZMul;
    public AnimationCurve addedX;
    public float addedXMul;
    public Vector3 addedPos;
    public Vector3 addedRot;
    public float transitionSpeed = 1;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    public void TransitionStart(float delay = 0)
    {
        if (localPosMode)
        {
            fromPos = objToTransition.transform.localPosition;
        }
        else
        {
            fromPos = objToTransition.transform.position;
        }
        
        toPos = fromPos + addedPos;

        fromRot = objToTransition.transform.localRotation;
        fromRotVec = Quaternion.ToEulerAngles(fromRot) * Mathf.Rad2Deg;
        toRotVec = fromRotVec + addedRot;
        toRot = Quaternion.Euler(fromRotVec + addedRot) ;
        
        SmoothGainValuesManager.instance.TransitionValueSetTo(0, 1, null, TransitionUpdate, null,transitionSpeed);
    }
    
    public void TransitionUpdate(float f)
    {
        if (localPosMode)
        {
            objToTransition.transform.localPosition = Vector3.LerpUnclamped(fromPos, toPos, transitionCurve.Evaluate(f)) + new Vector3(addedX.Evaluate(f) * addedXMul, addedY.Evaluate(f) * addedYMul, addedZ.Evaluate(f)) * addedZMul;
        }
        else
        {
            objToTransition.transform.position = Vector3.LerpUnclamped(fromPos, toPos, transitionCurve.Evaluate(f)) + new Vector3(0, addedY.Evaluate(f) * addedYMul, addedZ.Evaluate(f)) * addedZMul;
        }
        
        //objToTransition.transform.localRotation = Quaternion.LerpUnclamped(fromRot, toRot, transitionCurve.Evaluate(f));
        objToTransition.transform.localRotation = Quaternion.Euler(Vector3.Lerp( fromRotVec, toRotVec, transitionCurve.Evaluate(f)));
    }
}
