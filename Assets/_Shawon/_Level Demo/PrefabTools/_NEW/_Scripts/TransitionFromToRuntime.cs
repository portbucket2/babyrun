using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using System;

public class TransitionFromToRuntime : MonoBehaviour
{
    [SerializeField]
    float transitionVal = 0;
    float transitionSpeed = 1;
    //public Action        actionOnStart;
    //public Action        actionOnFinish;
    public Action<float> actionDuringUpdateValue;

    public bool running;
    public bool shutDown;
    UnityEvent unityEventAtStart;
    UnityEvent unityEventAtFinish;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        RunTransition();
    }
    public void StartTransition(UnityEvent unityEventStart, Action<float> updateVal = null , UnityEvent unityEventFinish = null,float speed = 1)
    {
        if (shutDown)
        {
            return;
        }
        transitionVal = 0;
        transitionSpeed = speed;
        unityEventAtStart =unityEventStart;
        unityEventAtFinish = unityEventFinish;
        actionDuringUpdateValue += updateVal;
        running = true;


        unityEventStart?.Invoke();
    }
    public void RunTransition()
    {
        if (shutDown)
        {
            ResetTransition();
            return;
        }

        if (running)
        {
            transitionVal += Time.deltaTime * transitionSpeed;
            if(transitionVal >= 1)
            {
                transitionVal = 1;
            }
            actionDuringUpdateValue?.Invoke(transitionVal);
            if (transitionVal == 1)
            {
                unityEventAtFinish?.Invoke();
                running = false;
            }
        }
    }
    public void ShutDownTransition()
    {
        shutDown = true;
    }
    public void ShutDownResume()
    {
        shutDown = false;
    }
    public void ResetTransition()
    {
        running = false;
        transitionVal = 0;
        unityEventAtStart = null;
        unityEventAtFinish = null;
        actionDuringUpdateValue = null;
    }
}
