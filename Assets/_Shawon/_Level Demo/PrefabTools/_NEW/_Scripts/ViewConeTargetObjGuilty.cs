using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class ViewConeTargetObjGuilty : MonoBehaviour
{
    public GameObject targetObj;
    public bool guilty;

    public UnityEvent punishmentEvent;
    public ViewConeShawon viewConeRef;
    // Start is called before the first frame update
    void Start()
    {
       
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void MakeBabyGuilty()
    {
        guilty = true;
    }
    public void MakeBabyNotGuilty()
    {
        guilty = false;
    }
    public void CheckBabyGuiltyOrNot()
    {
        if (guilty)
        {
            punishmentEvent?.Invoke();
            viewConeRef.DisableViewConeDetection();
        }
        
    }
    
}
