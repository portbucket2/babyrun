using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectUnparentFreeFall : MonoBehaviour
{
    public GameObject objToUnParent;
    public bool AddRigidbody;
    public bool AddCollider;
    public Vector3 velocityInitial;
    bool unparented;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void UnparentHappen()
    {
        if (unparented)
        {
            return;
        }
        objToUnParent.transform.SetParent(null);
        if (AddCollider)
        {
            objToUnParent.AddComponent<BoxCollider>();
        }
        else
        {
            if (objToUnParent.GetComponent<Collider>() != null)
            {
                objToUnParent.GetComponent<Collider>().enabled = true;
            }
        }
        if (AddRigidbody)
        {
            if(objToUnParent.GetComponent<Rigidbody>() == null)
            {
                objToUnParent.AddComponent<Rigidbody>();
            }

            
            objToUnParent.GetComponent<Rigidbody>().velocity = velocityInitial;
            
        }
        unparented = true;
    }
}
