using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using ShawonGameTools;

public class TransitionRuntimeSolidGo : MonoBehaviour
{
    //public bool localPosMode;
    Vector3 previousFramePos;


    Vector3 fromPos;
    Quaternion fromRot;
    Vector3 fromRotVec;
    Vector3 fromScale;

    Vector3 toPos;
    Vector3 toRotVec;
    public Vector3 toScale;
    public bool scaleUpdateToGivenValue;

    public GameObject objToTransition;
    public GameObject targetGo;

    public AnimationCurve transitionCurve;
    public AnimationCurve addedY;
    public float addedYMul;
    public AnimationCurve addedZ;
    public float addedZMul;
    public AnimationCurve addedX;
    public float addedXMul;

    public float transitionSpeed = 1;
    public enum RotatinUpdateMode
    {
        UpadteAsTarget, NoRotate, FacingToTarget, AlignToPath
    }
    public RotatinUpdateMode rotUpdateMode;
    //public bool rotUpdateAsTargetObj;

    [Header("Events")]
    public UnityEvent eventsAtStart;
    public bool restrictendEvent;
    public UnityEvent eventsAtFinish;

    [SerializeField]
    TransitionFromToRuntime transitionFromToRuntime;
    // Start is called before the first frame update
    void Start()
    {
        if(objToTransition != null)
        {
            previousFramePos = objToTransition.transform.position;
        }
        
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void TransitionStart(float delay = 0)
    {
        StartCoroutine(TransitionStartCR(delay));
    }
    IEnumerator TransitionStartCR(float f)
    {
        yield return new WaitForSeconds(f);
        TransitionStartToTargetGo();

        //yield return new WaitForSeconds(1 / transitionSpeed);
        //if (!restrictendEvent)
        //{
        //    //eventsAtFinish?.Invoke();
        //}

    }

     void TransitionStartToTargetGo()
    {

        if(objToTransition == null || targetGo == null)
        {
            return;
        }

        fromPos = objToTransition.transform.position;

        toPos = targetGo.transform.position;

        fromRot = objToTransition.transform.rotation;
        fromRotVec = Quaternion.ToEulerAngles(fromRot) * Mathf.Rad2Deg;
        //if (rotUpdateAsTargetObj)
        //{
        //    Quaternion toRot = targetGo.transform.rotation;
        //    toRotVec = Quaternion.ToEulerAngles(toRot) * Mathf.Rad2Deg;
        //}
        //else
        //{
        //    toRotVec = fromRotVec;
        //}
        Quaternion toRot = targetGo.transform.rotation;
        toRotVec = Quaternion.ToEulerAngles(toRot) * Mathf.Rad2Deg;
        fromScale = objToTransition.transform.localScale;
        if (scaleUpdateToGivenValue)
        {

        }
        else
        {
            toScale = fromScale;
        }
        
        transitionFromToRuntime.StartTransition(eventsAtStart, TransitionUpdate, eventsAtFinish, transitionSpeed);
        
    }
    void TransitionUpdate(float f)
    {
        if (targetGo != null)
        {
            toPos = targetGo.transform.position;
        }

        objToTransition.transform.position = Vector3.LerpUnclamped(fromPos, toPos, transitionCurve.Evaluate(f)) + new Vector3(addedX.Evaluate(f) * addedXMul, addedY.Evaluate(f) * addedYMul, addedZ.Evaluate(f)) * addedZMul;

        //objToTransition.transform.rotation = Quaternion.Euler(Vector3.Lerp(fromRotVec, toRotVec, transitionCurve.Evaluate(f)));
        objToTransition.transform.rotation = ProcessRot( f);



        objToTransition.transform.localScale = Vector3.Lerp(fromScale, toScale, transitionCurve.Evaluate(f));

    }
    public void ShutDownTransition()
    {
        transitionFromToRuntime.ShutDownTransition();
    }
    public Quaternion ProcessRot(float f)
    {
        Quaternion outrot = Quaternion.Euler(Vector3.Lerp(fromRotVec, toRotVec, transitionCurve.Evaluate(f))); 
        switch (rotUpdateMode)
        {
            case RotatinUpdateMode.UpadteAsTarget:
                {
                    outrot = Quaternion.Lerp(fromRot, targetGo.transform.rotation, transitionCurve.Evaluate(f));
                    //REMAINS THE SAME
                    break;
                }
            case RotatinUpdateMode.NoRotate:
                {
                    outrot = objToTransition.transform.rotation;
                    break;
                }
            case RotatinUpdateMode.FacingToTarget:
                {
                    outrot = Quaternion.LookRotation(targetGo.transform.position- objToTransition.transform.position, objToTransition.transform.up); 
                    break;
                }
            case RotatinUpdateMode.AlignToPath:
                {

                    outrot = Quaternion.LookRotation( objToTransition.transform.position- previousFramePos, objToTransition.transform.up); 
                    previousFramePos = objToTransition.transform.position;
                    break;
                }
                
        }

        return outrot;
    }
}
