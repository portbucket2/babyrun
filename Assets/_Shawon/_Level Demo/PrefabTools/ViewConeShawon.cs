using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

public class ViewConeShawon : MonoBehaviour
{
    public GameObject targetObj;
    public float angle;
    public float range;
    public bool targetObjInRange;
    bool disableDetection;
    public float angleCalculated;
    public GameObject coneObj;

    [Header("ConeVisualRelated")]
    public Color colorIdle;
    public Color colorAlert;
    public Image coneImage;

    [Header("EventRelated")]
    public UnityEvent eventToFireAtFinding;
    // Start is called before the first frame update
    void Start()
    {
        EnableConeVisual();
    }

    // Update is called once per frame
    void Update()
    {
        DetectTargetObjInRangeOrNot();
    }
    public void DetectTargetObjInRangeOrNot()
    {
        if (disableDetection)
        {
            return;
        }
        bool insideCone;
        bool insideRange;
        bool alertOn = false;
        Vector3 localPosOfTarget = transform.InverseTransformPoint(targetObj.transform.position);
        angleCalculated = Mathf.Atan2(localPosOfTarget.x , localPosOfTarget.z) * Mathf.Rad2Deg;

        if(Mathf.Abs(angleCalculated)<= angle*0.5f)
        {
            insideCone = true;
        }
        else{
            insideCone = false;
        }
        Vector3 localPosOfTargetIgnoringHeight = new Vector3(targetObj.transform.position.x,0, targetObj.transform.position.z);
        //if (Vector3.Distance(transform.position, targetObj.transform.position) <= range)
        if (Vector3.Distance (transform.position, localPosOfTargetIgnoringHeight) <= range)
        {
            insideRange = true;
        }
        else
        {
            insideRange = false;
        }

        if(insideRange && insideCone)
        {
            eventToFireAtFinding?.Invoke();
            targetObjInRange = true;

            //DisableConeVisual();
            if (!alertOn)
            {
                alertOn = true;
                coneImage.color = colorAlert;
            }

        }
        else
        {
            if (alertOn)
            {
                alertOn = false;
                
            }
            coneImage.color = colorIdle;
            targetObjInRange = false;
        }

        
    }
    public void DisableViewConeDetection()
    {
        //targetObjInRange = true;
        disableDetection = true;
        DisableConeVisual();
    }
    public void DisableConeVisual()
    {
        coneObj.SetActive(false);
    }
    public void EnableConeVisual()
    {
        coneObj.SetActive(true);
        coneImage.color = colorIdle;
    }
}
