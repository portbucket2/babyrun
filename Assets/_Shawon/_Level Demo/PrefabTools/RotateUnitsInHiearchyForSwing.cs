using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateUnitsInHiearchyForSwing : MonoBehaviour
{
    public Vector3 localRot;
    //public HexSpiralFlowMaker hexSpiralFlowMaker;
    public List<GameObject> unitsToRotate;

    [Header("SwingEnblingValues")]
    public float swingIntensity;
    float minSwingIntensity;
    public float swingFadingSpeed;
    public float swingFollowIntensity;
    public float swingAmp;
    // this defines the hierarchy chain wont follow the rotations At the beginning. the value increases gradullay and it follows
    float swingFollowCatchupSpeed = 2;
    [Header("SineWaveValues")]
    public float sinAngle;
    public float sinSpeed;
    public float sinOffsetinHierarchy;
    public bool rotateRunTime;
    // Start is called before the first frame update
    void Start()
    {
        //hexSpiralFlowMaker.actionAfterSpiralInitialization += InsertUnitsToRotate;
        //hexSpiralFlowMaker.actionOnSpiralUpdate += RotateUnitsAccordingtoSineValue;
    }

    // Update is called once per frame
    void Update()
    {
        //if (Input.GetKeyDown(KeyCode.P))
        //{
        //    SwingStart();
        //}
        if (rotateRunTime)
        {
            RotateUnitsRuntimeContinuiusNoFallOff();
        }
        
    }
    //public void InsertUnitsToRotate()
    //{
    //    for (int i = 0; i < hexSpiralFlowMaker.spiralUnits.Count; i++)
    //    {
    //        unitsToRotate.Add(hexSpiralFlowMaker.spiralUnits[i].gameObject);
    //    }
    //}
    public void RotateUnitsAccordingtoValue()
    {
        for (int i = 0; i < unitsToRotate.Count; i++)
        {
            unitsToRotate[i].transform.localRotation = Quaternion.Euler(localRot);
        }
    }
    public void RotateUnitsAccordingtoSineValue()
    {
        if(swingIntensity <= 0)
        {
            return;
        }
        SwingIntensityUpdate();
        SwingFollowIntensityUpdate();

        sinAngle += Time.deltaTime * sinSpeed;
        if(sinAngle > 360)
        {
            sinAngle -= 360;
        }
        if (sinAngle < 0)
        {
            sinAngle += 360;
        }
        for (int i = 0; i < unitsToRotate.Count; i++)
        {
            float angZ = Mathf.Sin(Mathf.Deg2Rad*( sinAngle + (sinOffsetinHierarchy * (float)i)));
            unitsToRotate[i].transform.localRotation = Quaternion.Euler(new Vector3(0,0,angZ* swingIntensity * swingAmp*swingFollowIntensity));
        }
    }
    public void RotateUnitsRuntimeContinuiusNoFallOff()
    {
        if (swingIntensity <= 0)
        {
            return;
        }
        //SwingIntensityUpdate();
        //SwingFollowIntensityUpdate();

        sinAngle += Time.deltaTime * sinSpeed;
        if (sinAngle > 360)
        {
            sinAngle -= 360;
        }
        if (sinAngle < 0)
        {
            sinAngle += 360;
        }
        for (int i = 0; i < unitsToRotate.Count; i++)
        {
            float angZ = Mathf.Sin(Mathf.Deg2Rad * (sinAngle + (sinOffsetinHierarchy * (float)i)));
            unitsToRotate[i].transform.localRotation = Quaternion.Euler(new Vector3(0, 0, angZ * swingIntensity * swingAmp ));
        }
    }
    public void SwingStart()
    {
        swingIntensity = 1;
        swingFollowIntensity = 0;
    }
    public void SwingIntensityUpdate()
    {
        swingIntensity -= Time.deltaTime * swingFadingSpeed;

        if (swingIntensity < 0)
        {
            swingIntensity = 0;
            sinAngle = 0;
        }
    }
    public void SwingFollowIntensityUpdate()
    {
        if(swingFollowIntensity < 1)
        {
            swingFollowIntensity += Time.deltaTime * swingFollowCatchupSpeed;
        }
        else
        {
            swingFollowIntensity = 1;
        }
        
    }
}
