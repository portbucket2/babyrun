using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjMakeParentChild : MonoBehaviour
{
    public GameObject obj;
    public GameObject parent;
    public ParticleSystem particle;
    
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void ParentChildHappen(float delay)
    {
        StartCoroutine(ParentChildCR(delay));
    }
    IEnumerator ParentChildCR(float f)
    {
        yield return new WaitForSeconds(f);
        if(obj != null && parent != null)
        {
            obj.transform.SetParent(parent.transform);
        }
        if(particle != null)
        {
            particle.Play();
        }
        
    }
}
