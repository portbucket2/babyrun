using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;

public class CameraTransitionTo : MonoBehaviour
{
    public CinemachineVirtualCamera camToFocus;
    public CinemachineVirtualCamera[] otherCams;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void CameraTransitionHappen(float delay)
    {
        StartCoroutine(CameraTransitionCR(delay));
    }

    IEnumerator CameraTransitionCR(float f)
    {
        yield return new WaitForSeconds(f);
        camToFocus.Priority = 20;
        for (int i = 0; i < otherCams.Length; i++)
        {
            otherCams[i].Priority = 10;
        }

    }
}
