using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class RangedTriggerEvent : MonoBehaviour
{
    
    public GameObject targetObj;
    public float range = 0.5f;
   public bool triggered;

    public UnityEvent eventtotrigger;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (!triggered)
        {
            TargetNearOfNotToTrigger();
        }
    }

    public void TargetNearOfNotToTrigger()
    {
        if(targetObj == null)
        {
            return;
        }
        if(Vector3.Distance(transform.position, targetObj.transform.position)<= range)
        {
            eventtotrigger?.Invoke();
            triggered = true;
        }
    }
}
