using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class ObjSnapToRefGo : MonoBehaviour
{
    public GameObject objToTransition;
    public GameObject refGo;
    public UnityEvent eventsBeforeSnap;
    public UnityEvent eventsAfterSnap;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void SnapHappen(float delay)
    {
        if(objToTransition == null || refGo == null)
        {
            return;
        }
        StartCoroutine(SnapHappenCr(delay));
    }
    IEnumerator SnapHappenCr(float f)
    {
        yield return new WaitForSeconds(f);

        eventsBeforeSnap?.Invoke();

        SnapTheGoHappen();

        eventsAfterSnap?.Invoke();
        
    }
    void SnapTheGoHappen()
    {
        objToTransition.transform.position = refGo.transform.position;
        objToTransition.transform.rotation = refGo.transform.rotation;
    }
}
