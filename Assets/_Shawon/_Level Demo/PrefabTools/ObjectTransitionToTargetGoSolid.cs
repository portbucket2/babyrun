using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using ShawonGameTools;

public class ObjectTransitionToTargetGoSolid : MonoBehaviour
{
    //public bool localPosMode;
    Vector3 fromPos;
    Quaternion fromRot;
    Vector3 fromRotVec;
    Vector3 fromScale;

     Vector3 toPos;
    Vector3 toRotVec;
    public Vector3 toScale;
    public bool scaleUpdateToGivenValue;

    public GameObject objToTransition;
    public GameObject targetGo;

    public AnimationCurve transitionCurve;
    public AnimationCurve addedY;
    public float addedYMul;
    public AnimationCurve addedZ;
    public float addedZMul;
    public AnimationCurve addedX;
    public float addedXMul;

    public float transitionSpeed = 1;
    
    public bool rotUpdateAsTargetObj;
    [Header("Events")]
    public UnityEvent eventsAtStart;
    public bool restrictendEvent;
    public UnityEvent eventsAtFinish;


    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    public void TransitionStart(float delay = 0)
    {
        StartCoroutine(TransitionStartCR(delay));
    }
    IEnumerator TransitionStartCR(float f)
    {
        yield return new WaitForSeconds(f);
        TransitionStartToTargetGo();

        yield return new WaitForSeconds(1/ transitionSpeed);
        if (!restrictendEvent)
        {
            eventsAtFinish?.Invoke();
        }
        
    }
    
    public void TransitionStartToTargetGo()
    {

        fromPos = objToTransition.transform.position;
        
        toPos = targetGo.transform.position;

        fromRot = objToTransition.transform.rotation;
        fromRotVec = Quaternion.ToEulerAngles(fromRot) * Mathf.Rad2Deg;
        if (rotUpdateAsTargetObj)
        {
            Quaternion toRot = targetGo.transform.rotation;
            toRotVec = Quaternion.ToEulerAngles(toRot) * Mathf.Rad2Deg;
        }
        else
        {
            toRotVec = fromRotVec;
        }
        fromScale = objToTransition.transform.localScale;
        if (scaleUpdateToGivenValue)
        {
            
        }
        else
        {
            toScale = fromScale;
        }
        eventsAtStart?.Invoke();
        SmoothGainValuesManager.instance.TransitionValueSetTo(0, 1, null, TransitionUpdate, null, transitionSpeed);
    }
    public void TransitionUpdate(float f)
    {
        if(targetGo != null)
        {
            toPos = targetGo.transform.position;
        }
        
        //objToTransition.transform.localPosition = Vector3.LerpUnclamped(fromPos, toPos, transitionCurve.Evaluate(f)) + new Vector3(addedX.Evaluate(f) * addedXMul, addedY.Evaluate(f) * addedYMul, addedZ.Evaluate(f)) * addedZMul;
        objToTransition.transform.position = Vector3.LerpUnclamped(fromPos, toPos, transitionCurve.Evaluate(f)) + new Vector3(addedX.Evaluate(f) * addedXMul, addedY.Evaluate(f) * addedYMul, addedZ.Evaluate(f)) * addedZMul;
        //objToTransition.transform.localRotation = Quaternion.LerpUnclamped(fromRot, toRot, transitionCurve.Evaluate(f));
        objToTransition.transform.rotation = Quaternion.Euler(Vector3.Lerp(fromRotVec, toRotVec, transitionCurve.Evaluate(f)));
        objToTransition.transform.localScale = Vector3.Lerp(fromScale, toScale, transitionCurve.Evaluate(f));

    }
}
