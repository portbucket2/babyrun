using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjEnableDisableTo : MonoBehaviour
{
    public GameObject objToEnable;
    public GameObject objToDisable;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void ObjEnableDisableHappen(float delay)
    {
        StartCoroutine(ObjEnableDisablenCR(delay));
    }

    IEnumerator ObjEnableDisablenCR(float f)
    {
        yield return new WaitForSeconds(f);
        if(objToEnable != null)
        {
            objToEnable.SetActive(true);
        }
        if (objToDisable != null)
        {
            objToDisable.SetActive(false);
        }
    }
}
