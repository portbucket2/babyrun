using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class ObjDelayedEvents : MonoBehaviour
{

    public UnityEvent events;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void EventTriggerHappen(float delay)
    {
        StartCoroutine(EventTriggerCR(delay));
    }

    IEnumerator EventTriggerCR(float f)
    {
        yield return new WaitForSeconds(f);
        events?.Invoke();

    }
}
