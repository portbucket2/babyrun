using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjParticlesPlay : MonoBehaviour
{
    public ParticleSystem[] particles;
    public enum ParticleOperationMode
    {
        Start, Stop, Pause
    }
    public ParticleOperationMode particleOperationMode;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    public void ParticlePlayHappen(float delay)
    {
        StartCoroutine(ParticlePlayCR(delay));
    }
    IEnumerator ParticlePlayCR(float f)
    {
        yield return new WaitForSeconds(f);


        for (int i = 0; i < particles.Length; i++)
        {
            switch (particleOperationMode)
            {
                case ParticleOperationMode.Start:
                {
                        particles[i].Play();
                        break;
                }
                case ParticleOperationMode.Stop:
                    {
                        particles[i].Stop();
                        break;
                    }
                case ParticleOperationMode.Pause:
                    {
                        particles[i].Pause();
                        break;
                    }
            }
            
        }
        

    }
}

