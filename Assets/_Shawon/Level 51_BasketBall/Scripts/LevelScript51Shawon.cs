using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;

public class LevelScript51Shawon : LevelCommonActivitiesShawon
{
    public GameObject playerObj;
    
    public ObjectTransitionToTargetPos playerTransitionIntoBowl;
    public ObjectTransitionToTargetPos playerTransitionIntoNet;
    public GameObject playerIntoBowlPos;

    [Header("ParentRelated")]
    public GameObject parentObj;
    
    public ObjectTransitionFromTo parentRotateToBowl;
    public ObjectTransitionFromTo parentRotateToBasket;
    public bool playerReady;

    [Header("BallRelated")]
    public GameObject ballObj;
    public ObjectTransitionToTargetPos playerTransitionIntoParentHand;
    public ObjectTransitionToTargetPos playerTransitionIntoBasket;
    public ObjectTransitionToTargetPos playerTransitionBallLocal;
    public GameObject parentHandPos;
    public GameObject basketPos;
    public GameObject bowlPos;
    public GameObject ballVisual;
    public Animator ballFallAnim;

    [Header("CamRelated")]
    public CinemachineVirtualCamera cam;
    public CinemachineVirtualCamera cam2;
    // Start is called before the first frame update
    void Start()
    {
        ParentBasketballLoopStart();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void PlayerJumpIntoBowl()
    {
        PlayerControlDisable();
        PlayerJumpAnim();
    }
    public void PlayerControlDisable()
    {
        playerObj.GetComponent<MovementHandler>().enabled = false;
        playerObj.GetComponent<PlayerController>().enabled = false;
        playerObj.GetComponent<Rigidbody>().useGravity = false;
        playerObj.GetComponent<Rigidbody>().isKinematic = true;
        playerTransitionIntoBowl.TransitionStartToTargetGo(playerIntoBowlPos);
    }
    public void PlayerJumpAnim()
    {
        babyAnim.SetTrigger("joyJump");
        Invoke("PlayerReadyMake", 1f);
    }
    public void PlayerReadyMake()
    {
        playerReady = true;
        //cam.Follow = null;
        //cam.LookAt = null;
        cam.Priority = 10;
        cam2.Priority = 20;
    }

    public void ParentBasketballLoopStart()
    {
        StartCoroutine(BasketballLoop());
    }
    IEnumerator BasketballLoop(bool playerRdy = false)
    {
        //if (playerReady)
        //{
        //    yield return null;
        //}
        bool gotPlayer = false;
        if (playerReady)
        {
            gotPlayer = true;
            playerObj.transform.SetParent(ballObj.transform);
            ballVisual.SetActive(false);
        }
        else
        {
            ballObj.transform.localPosition = new Vector3(0.3f, 0, 0);
            ballVisual.SetActive(true);
        }
        parentRotateToBowl.TransitionStart();
        //yield return new WaitForSeconds(1);
        parentAnim.SetTrigger("collectBall");
        yield return new WaitForSeconds(1.1f);

        ballObj.transform.SetParent(parentHandPos.transform);
        playerTransitionIntoParentHand.TransitionStart();

        parentRotateToBasket.TransitionStart();
        yield return new WaitForSeconds(1);
        parentAnim.SetTrigger("throwBall");
        yield return new WaitForSeconds(0.5f);
        ballObj.transform.SetParent(basketPos.transform);
        playerTransitionIntoBasket.TransitionStart();
        if (gotPlayer)
        {
            babyAnim.SetTrigger("babyOnAir");
            playerTransitionIntoNet.TransitionStart();
        }
        yield return new WaitForSeconds(1);
        
        if (gotPlayer)
        {
            babyAnim.SetTrigger("babyIntoNet");
        }
        else
        {
            ballFallAnim.SetTrigger("BallFall");
        }
        if (!gotPlayer)
        {
            ballVisual.SetActive(false);
            ballObj.transform.SetParent(bowlPos.transform);
            ballObj.transform.localPosition = new Vector3(0.0f, 0, 0);
            ballObj.transform.localRotation = Quaternion.Euler(Vector3.zero);
        }
        

        yield return new WaitForSeconds(1);
        
        //StartCoroutine(BasketballLoop());
        if (gotPlayer)
        {
            LevelEndSuccessForcefully();
        }
        else
        {
            
            StartCoroutine(BasketballLoop());
        }
    }
    
}
