using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Level69TasksShawon : LevelCommonActivitiesShawon
{
    public ObjectTransitionToTargetGoSolid coneTransitions;
    public bool babyHasTheBag;
    public bool babyTask2Done;
    public bool babyFailed;
    public bool babyBusy;
    public GameObject coneObj;
    public ObjDelayedEvents babyTask2Success;
    public ObjDelayedEvents babyTask2Fail;
    [Header("Parent")]
    public ObjectTransitionToTargetGoSolid parentFall;
   

    [Header("Birds")]
    public ObjectTransitionToTargetGoSolid BirdHit;

    // Start is called before the first frame update
    void Start()
    {
        ConeRotateStart();
       
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void MakeBabyHasThebag()
    {
        babyHasTheBag = true;
    }
    public void MakeBabyTask2Done()
    {
        if (!babyFailed)
        {
            babyTask2Done = true;
            babyTask2Success.EventTriggerHappen(0);
            BirdHitStart(); 
        }
        else
        {
            LevelEndFailForcefullyDelayed(0.25f);
            MakeBabyMovementControlDisable();
             
            babyTask2Fail.EventTriggerHappen(0);
           
        }
        
        //MakeBabyMovementControlDisable();
    }
    public void ConeRotateStart()
    {
        coneTransitions.TransitionStart();
    }
    public void EvaluateBabyGuiltyOrNot()
    {
        if (babyTask2Done)
        {
            return;
        }
        if (babyHasTheBag)
        {
            coneObj.SetActive(false);
            parentAnim.SetTrigger("threat");
            //LevelEndFailForcefullyDelayed(0.25f);
            //MakeBabyMovementControlDisable();
            parentFall.TransitionStart();
            babyFailed = true;

            if (!babyBusy)
            {
                LevelEndFailForcefullyDelayed(0.25f);
                MakeBabyMovementControlDisable();

                babyTask2Fail.EventTriggerHappen(0);
            }
        }
    }
    public void BabySuccess()
    {
        coneObj.SetActive(false);
        LevelEndSuccessForcefullyDelayed(0.25f);
        //BirdHitStart();
    }
    public void BirdHitStart()
    {
        BirdHit.TransitionStart();
    }
    public void MakeBabyBusy()
    {
        babyBusy = true;
    }
}
