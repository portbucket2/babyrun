using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Level56TasksShawon : LevelCommonActivitiesShawon
{
    [Header("Player")]
    public GameObject boneObj;
    public GameObject boneHoldRefHand;

    [Header("kitty")]
    public Animator kittyAnim;
    public ObjDelayedEvents objDelayedEventsCatFail;

    [Header("Aqua")]
    public GameObject solidGlass;
    public GameObject brokenGlass;
    public ObjectTransitionToTargetGo waterScaleDown;
    public ObjectTransitionToTargetGo frameFallTransition;
    public ParticleSystem waterParticle;
    // Start is called before the first frame update
    void Start()
    {
        LevelManager.Instance.onLevelFinished += LevelFinishedCustomCallback;
        actionOnLevelFail += FailHappen;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void KittyResponseToTrigger1()
    {
        KittyAnimHiss();
        Invoke("KittyAnimRun", 2f);
        ParentGetStartle();
    }
    public void KittyAnimHiss()
    {
        kittyAnim.SetTrigger("scrachingFloor");
    }
    public void KittyAnimRun()
    {
        kittyAnim.SetTrigger("run");
    }

    public void FailHappen()
    {
        objDelayedEventsCatFail.EventTriggerHappen(0);
    }
    public void BreakAqua()
    {
        solidGlass.SetActive(false);
        brokenGlass.SetActive(true);
        waterScaleDown.TransitionStart();
        waterParticle.Play();
        frameFallTransition.TransitionStart();
    }
    
}
