using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Level68TasksShawon : LevelCommonActivitiesShawon
{
    public ObjectTransitionToTargetGoSolid parentWalkTransition;
    public GameObject outWoldParent;
    public bool parentWalking;
    public GameObject parentObj;
    public GameObject babyHolder;
    public bool babyGuilty;
    bool babyCaught;
    public ViewConeShawon viewConeShawon;
    [Header("Manhole")]
    public GameObject triggerManhole;
    public bool manholeReady;

    [Header("Parentrelated")]
    public Renderer[] meshRend;
    public Material blackMat;
    // Start is called before the first frame update
    void Start()
    {
        ParentWalkStart();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void ParentWalkStart()
    {
        parentWalking = true;
        parentWalkTransition.TransitionStart();
}
    public void ParentWalkStop()
    {
        parentWalking = false; ;
        parentObj.transform.SetParent(outWoldParent.transform);
    }
    public void SnapBabyHolderAtBabyPos()
    {
        babyHolder.transform.position = playerObj.transform.position;
        babyHolder.transform.rotation = playerObj.transform.rotation;
        playerObj.transform.SetParent(babyHolder.transform);
    }
    public void ParentBabyToOut()
    {
        
        playerObj.transform.SetParent(outWoldParent.transform);
    }
    public void MakeManholeReady()
    {
        manholeReady = true;
        triggerManhole.SetActive(true);
    }
    public void BabyGuiltyOn()
    {
        babyGuilty = true;
    }
    public void BabyGuiltyOff()
    {
        babyGuilty = false;
    }
    public void ViewConeInsideFun()
    {
        if (babyGuilty)
        {
            viewConeShawon.gameObject.SetActive(false);
            ParentWalkStop();
            parentAnim.SetTrigger("threat");
            ParentBabyToOut();
            LevelEndFailForcefullyDelayed(1);

            babyCaught = true;
        }
    }
    public void BabyFreeRunOrNot()
    {
        if (!babyCaught)
        {
            MakeBabyMovementControlEnable();
        }
    }
    public void MakeParentBlack()
    {
        for (int i = 0; i < meshRend.Length; i++)
        {
            meshRend[i].material = blackMat;
        }
        
    }
}
