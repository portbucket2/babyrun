using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class SuckIntoVacuum : MonoBehaviour
{
    public ObjectTransitionToTargetGo objTransition;
    public GameObject vaccumHeadObj;
    public float sucktionDistance;
    public bool useTrigger;
    bool sucked;
    public ParticleSystem burstParticle;
    public Level38TasksShawon levelTasks;

    [SerializeField] private UnityEvent onInteractEvent;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (!useTrigger && levelTasks.vaccumOn)
        {
            DetectSuctionOrNot();
        }
    }
    public void DetectSuctionOrNot()
    {
        if (sucked)
        {
            return;
        }
        float dis = Vector3.Distance(vaccumHeadObj.transform.position, transform.position);
        if(dis < sucktionDistance)
        {
            sucked = true;
            SuctionHappen();
        }
    }

    public void SuctionHappen()
    {
        objTransition.objToTransition = this.gameObject;
        objTransition.targetGo = vaccumHeadObj;
        objTransition.TransitionStart();
        if (burstParticle != null)
        {
            burstParticle.Play();
        }
        onInteractEvent?.Invoke();
    }
}
