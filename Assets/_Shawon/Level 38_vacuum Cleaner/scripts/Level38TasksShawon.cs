using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Level38TasksShawon : LevelCommonActivitiesShawon
{
    [Header("Player")]
    public GameObject playerObj;
    public GameObject vaccumObj;
    public bool vaccumOn;
    public ObjectTransitionToTargetGo playerChildjumpDown;
    public ParticleSystem suckParticle;

    [Header("Cat")]
    public Animator catAnim;
    public ObjectTransitionToTargetGo catTransitionRun;
    public Renderer[] kittyRends;
    public Material kittyTrimmedmat;

    [Header("NpcBitch")]
    public Animator bitchAnim;
    public GameObject bitchMeshToAppear;
    public GameObject bitchMeshToHide;

    // Start is called before the first frame update
    void Start()
    {
        LevelManager.Instance.onLevelFinished += LevelFinishedCustomCallback;
        actionOnLevelFail += LevelFail;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void FirstTriggerAction()
    {
        MakeBabyMovementControlDisable();
        Invoke("MakeVaccumCleanerAChildOfPlayer", 0.8f);
    }
    public void MakeBabyMovementControlDisable()
    {
        //playerObj.GetComponent<MovementHandler>().enabled = false;
        //playerObj.GetComponent<PlayerController>().enabled = false;
        playerObj.GetComponent<PlayerController>().inputEnabled = false;
        playerObj.GetComponent<Rigidbody>().useGravity = false;
        playerObj.GetComponent<Rigidbody>().isKinematic = true;

        playerObj.GetComponent<MovementHandler>().moveDirection = Vector3.zero;
    }
    public void MakeBabyMovementControlEnable()
    {
        //playerObj.GetComponent<MovementHandler>().enabled = true;
        //playerObj.GetComponent<PlayerController>().enabled = true;
        playerObj.GetComponent<PlayerController>().inputEnabled = true;
        playerObj.GetComponent<Rigidbody>().useGravity = true;
        playerObj.GetComponent<Rigidbody>().isKinematic = false;

        //playerObj.GetComponent<MovementHandler>().SetMoveRotation(Vector3.zero);
    }
    public void MakeVaccumCleanerAChildOfPlayer()
    {
        vaccumObj.transform.SetParent(playerObj.transform);
        MakeBabyMovementControlEnable();
        babyAnim.SetFloat("RunBlend", 1);
        vaccumOn = true;
        ParentGetStartle();
        LevelMeterFillStart();
        suckParticle.Play();
    }
    public void LevelFail()
    {
        vaccumObj.transform.SetParent(this.transform);
        vaccumObj.transform.localPosition = new Vector3(vaccumObj.transform.localPosition.x, 0, vaccumObj.transform.localPosition.z);
        vaccumObj.transform.rotation = Quaternion.identity;
        playerChildjumpDown.TransitionStart();
    }
    public void CatSuck()
    {
        catAnim.SetTrigger("vaccumRun");
        Invoke("CatRunAway", 0.5f);

        for (int i = 0; i < kittyRends.Length; i++)
        {
            kittyRends[i].material = kittyTrimmedmat;
        }
    }
    public void CatRunAway()
    {
        catTransitionRun.TransitionStart();
    }
    public void NpcBitchBikiniVisual()
    {
        bitchAnim.SetTrigger("HideBoobs");
        bitchMeshToAppear.SetActive(true);
        bitchMeshToHide.SetActive(false);
    }
}
