using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Level95Shawon : LevelCommonActivitiesShawon
{
    public bool hasPlates;
    public ObjDelayedEvents objDelayedEventParentSuc;
    public ObjDelayedEvents objDelayedEventParentSucWithPlates;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void HasPlatesOn()
    {
        hasPlates = true;
    }
    public void HasPlatesOff()
    {
        hasPlates = false;
    }

    public void LevelEndFailDelayornot()
    {
        if (hasPlates)
        {
            //LevelEndFailForcefullyDelayed(3);
            objDelayedEventParentSucWithPlates.EventTriggerHappen(0);
            MakeBabyMovementControlDisable();
        }
        else
        {
            LevelEndFailForcefullyDelayed(0);
            objDelayedEventParentSuc.EventTriggerHappen(0);
        }
    }
}
