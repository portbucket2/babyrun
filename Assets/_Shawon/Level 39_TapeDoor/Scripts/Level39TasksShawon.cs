using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ShawonGameTools;
using Cinemachine;

public class Level39TasksShawon : LevelCommonActivitiesShawon
{
    public GameObject tapeStick;
    
    public Material tapeOutMat;
    public Material tapeInMat;
    public GameObject TapeInGoDoor;
    public GameObject TapeInGoFace;
    public GameObject TapeFaceRefPos;
    public GameObject tapeMesh;

    public ObjectTransitionToTargetPos objTransitionToTargetTape;
    public ObjectTransitionToTargetPos objTransitionToTargetParent;
    public ObjectTransitionToTargetGo objTransitionToTargetGoTape;

    public GameObject playerObj;
    public ObjectTransitionToTargetPos objTransitionToTargetPlayer;

    [Header("CamRelated")]
    public CinemachineVirtualCamera cam;
    public CinemachineVirtualCamera cam2;
    // Start is called before the first frame update
    void Start()
    {
        //Material m = new Material(tapeInMat);
        //TapeInGoFace.GetComponentInChildren<Renderer>().material = m;
        //tapeInMat = m;
        TapesBendUpdate(8);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void TapeStickAtDoorPos()
    {
        tapeStick.transform.localRotation = Quaternion.Euler(new Vector3(0, -90, 0));
        //LevelManager.Instance.
    }
    public void ParentFallDownWard()
    {
        parentAnim.SetTrigger("FallDownward");
    }
    public void ParentAnimSleeping()
    {
        parentAnim.SetTrigger("Sleeping");
        //ParentLevitateOnBed();
    }
    public void ParentLevitateOnBed()
    {
        //ParentController.Instance.gameObject.transform.localPosition = new Vector3(ParentController.Instance.gameObject.transform.localPosition.x, 0.62f, ParentController.Instance.gameObject.transform.localPosition.z);
    }
    public void ParentGetDownFromBed()
    {
        parentAnim.gameObject.transform.localPosition = new Vector3(0, 0, 0);
    }
    public void TapeFallHappen()
    {
        //TapeInGoFace.transform.SetParent(TapeFaceRefPos.transform);
        //SmoothGainValuesManager.instance.TransitionValueSetTo(0, 10, null, TapesBendUpdate, null, 2);
        //TapeOnFaceTransition();
    }
    public void TapesBendUpdate(float f)
    {
        //tapeOutMat.SetFloat("BendOrNot", f);
        //tapeInMat.SetFloat("BendOrNot", f);
    }
    public void TapeMeshScale(float f)
    {
        tapeMesh.transform.localScale = new Vector3(tapeMesh.transform.localScale.x, tapeMesh.transform.localScale.y,  f);
    }
    public void TapeUnfold()
    {
        SmoothGainValuesManager.instance.TransitionValueSetTo(8, 0, null, TapesBendUpdate, null, 2);
        SmoothGainValuesManager.instance.TransitionValueSetTo(0.1f, 1f, null, TapeMeshScale, null, 2);
    }
    public void TapeOnFaceTransition()
    {
        //objTransitionToTargetTape.TransitionStart();
        objTransitionToTargetGoTape.TransitionStart();
    }
    public void ParentTransitiontoFinalPos()
    {
        objTransitionToTargetParent.TransitionStart();
    }

    public void ThirdTriggerDelayed()
    {
        MakeBabyMovementControlDisable();
        //Invoke("ThirdTrigger", 1f);
        //Invoke("TapeFallHappen", 0.25f);
        ParentTransitiontoFinalPos();
        PlayerControlDisable();
        objTransitionToTargetPlayer.TransitionStart();
        CamTransition();
    }
    public void ThirdTrigger()
    {
        LevelEndSuccessForcefully();
        //LevelManager.Instance.ReportState(LevelState.Success);
        LevelManager.Instance.ForceFillMeter();
        //TapeFallHappen();
        
        
    }
    public void PlayerControlDisable()
    {
        playerObj.GetComponent<MovementHandler>().enabled = false;
        playerObj.GetComponent<PlayerController>().enabled = false;
        playerObj.GetComponent<Rigidbody>().useGravity = false;
        playerObj.GetComponent<Rigidbody>().isKinematic = true;
    }
    public void CamTransition()
    {
       
        cam.Priority = 10;
        cam2.Priority = 20;
    }
}
