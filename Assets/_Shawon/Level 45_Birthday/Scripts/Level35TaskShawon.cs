using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Level35TaskShawon : LevelCommonActivitiesShawon
{
    public GameObject cakeObj;
    public GameObject cakeAtHandHolder;
    public ObjectTransitionToTargetPos cakeTransitionComeIntoHand;
    public ObjectTransitionToTargetPos cakeTransitionIntoHand;
    public Animator babytAnim;

    public ParticleSystem[] cakeParticles;

    [Header("NPCFather")]
    public Animator npcFatherAnim;
    public GameObject cakeHolderFace;
    public ObjectTransitionToTargetPos cakeTransitionIntoFace;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void MakeCakeComeIntoHand()
    {

        cakeTransitionComeIntoHand.TransitionStartToTargetGo(cakeAtHandHolder,false);
        Invoke("MakeCakeChildOfHand", 0.5f);
    }
    public void MakeCakeChildOfHand()
    {
        cakeObj.transform.SetParent(cakeAtHandHolder.transform);
        cakeTransitionIntoHand.TransitionStart();
    }
    public void MakeCakeChildOfFace()
    {
        cakeObj.transform.SetParent(cakeHolderFace.transform);
        cakeTransitionIntoFace.TransitionStart();
    }
    public void NpcFatherCakeOnFaceAnimDelayed()
    {
        Invoke("NpcFatherCakeOnFaceAnim", 0.5f);
    }
    public void NpcFatherCakeOnFaceAnim()
    {
        npcFatherAnim.SetTrigger("CakeHitOnFace");
        CakePartclesPlay();
    }
    public void BabyAnimRunBlend()
    {
        babyAnim.SetFloat("cakeRunBlend", 1);
    }
    public void CakePartclesPlay()
    {
        for (int i = 0; i < cakeParticles.Length; i++)
        {
            cakeParticles[i].Play();
        }
        Invoke("CakePartclesPause", 0.8f);
    }
    public void CakePartclesPause()
    {
        for (int i = 0; i < cakeParticles.Length; i++)
        {
            cakeParticles[i].Pause();
        }
    }
}
