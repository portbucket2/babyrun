﻿using UnityEngine;
using DG.Tweening;
using FRIA;

public class CoinCollection : MonoBehaviour
{
    private enum CoinType { Trigger, OnEnable}
    [SerializeField] private CoinType coinType;
    [SerializeField] private Transform target;
    [SerializeField] private float rotationSpeed = 360f;
    [SerializeField] private float targetScale = 100f;
    [SerializeField] private float duration = 1f;
    [SerializeField] private int value = 1;
    [SerializeField] private ParticleSystem coinParticle;

    private const string PLAYER = "Player";
    private bool coinFired = false;

    private void Start()
    {
        target = LevelManager.Instance.ui.coinIcon;

        if (coinType == CoinType.OnEnable)
        {
            Initialize();
        }
    }

    private void Update()
    {
        transform.Rotate(0f, rotationSpeed * Time.deltaTime, 0f);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (coinType == CoinType.OnEnable || coinFired) return;
        if (other.CompareTag(PLAYER)) Initialize();
    }

    public void Initialize()
    {
        coinFired = true;
        transform.parent = target;
        coinParticle.Play();
        rotationSpeed = 720f;

        transform.DOScale(Vector3.one * targetScale, duration);
        transform.DOLocalMove(Vector3.zero, duration).OnComplete(() =>
        {
            //LevelManager.Instance.CoinCount += value;
            Currency.Transaction(CurrencyType.COIN, value);
            DestroyThis();
        });
    }
    public virtual void DestroyThis()
    {
        Destroy(gameObject);
    }
    public void SetValue(int newValue)
    {
        value = newValue;
    }
}
