using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GrabRopeLevel71 : Task
{
    [SerializeField] AutoFollow grabTissue;
    [SerializeField] Transform mop;
    [SerializeField] GameObject arrow;
    public override void OnInteract()
    {
        //throw new System.NotImplementedException();
        grabTissue.Follow(true);
        mop.parent = null;
        mop.GetComponent<Rigidbody>().isKinematic = true;
        arrow.SetActive(false);
        LevelManager.Instance.MeterFiller();
        if (ParentController.Instance) ParentController.Instance.GetStartle();
    }

    public override void OnStart()
    {
        throw new System.NotImplementedException();
    }

    
}
