using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class AdultMaleRoutine_76 : MonoBehaviour
{
    [SerializeField] bool chaseStarted = false;
    [SerializeField] bool isBeingChased = false;
    [SerializeField] Transform child;
    [SerializeField] float movementSpeed;
    [SerializeField] float scaredDistance;
    [SerializeField] Animator animator;
    [SerializeField] GameObject happyFace;
    [SerializeField] GameObject scaredFace;
    [SerializeField] List<Transform> path;
    int lastIndex = -1;
    void Start()
    {

    }

    
    void FixedUpdate()
    {
        if (!isBeingChased && chaseStarted)
        {
            Chasable();
        }
    }
    void Chasable()
    {
        if (chaseStarted && !isBeingChased && Vector3.Distance(transform.position, child.position) < scaredDistance)
        {
            isBeingChased = true;
            StartChase();
        }
        else
            isBeingChased = false;

        
    }
    void StartChase()
    {
        StartCoroutine(ChaseRoutine());
    }
    void RouteComplete()
    {
        Debug.Log("chase complete");
        animator.CrossFadeInFixedTime("idle", 0.25f);
        isBeingChased = false;
    }
    IEnumerator ChaseRoutine()
    {
        happyFace.SetActive(false);
        scaredFace.SetActive(true);
        //transform.DOKill();
        Debug.Log("chase start");
        int max = path.Count;
        int index = 0;
        //float distance = 0f;
        //float preDistance = 0f;
        //for (int i = 0; i < max; i++)
        //{
        //    distance = Vector3.Distance(transform.position, path[i].position);
        //    if (distance > preDistance)
        //        index = i;

        //    preDistance = distance;
        //}
        index = Random.Range(0, max);
        //while (index != lastIndex)
        //{
        //    index = Random.Range(0, max);
        //    yield return null;
        //}
        //lastIndex = index;
        animator.CrossFadeInFixedTime("run", 0.25f);
        float dis = Vector3.Distance(transform.position, path[index].position);
        transform.LookAt(path[index]);
        transform.DOMove(path[index].position, dis / movementSpeed).SetEase(Ease.Linear).OnComplete(RouteComplete);
        yield return null;
    }
    public void ChaseStart()
    {
        chaseStarted = true;
    }
    public void ChaseFinished()
    {
        chaseStarted = false;
        transform.DOKill();
        animator.CrossFadeInFixedTime("fall", 0.25f);
    }
}
