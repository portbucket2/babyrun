using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using Cinemachine;

public class TaskRoutine_97 : Task
{
    [SerializeField] Transform greenChair;
    [SerializeField] Transform blueChair;
    [SerializeField] Transform pullingPosition;
    [SerializeField] Transform clothLandPosition;
    [SerializeField] Transform staticCloth;
    [SerializeField] Transform physicsCloth;
    [SerializeField] List<Rigidbody> arbitaryObjects;
    [SerializeField] List<Breakable> arbitaryBreakables;

    [SerializeField] Animator boyAnimator;
    [SerializeField] Animator girlAnimator;
    [SerializeField] ParticleSystem girlExclaimation;
    [SerializeField] ParticleSystem fallParticle;
    [SerializeField] ParticleSystem foodSpreadParticle;

    [SerializeField] CinemachineVirtualCamera vCam1;
    [SerializeField] CinemachineVirtualCamera vCam2;

    [Header("Debug : ")]
    [SerializeField] PlayerController player;
    [SerializeField] Transform rightHand;

    [SerializeField] Vector3 forceDir = new Vector3(-1f, 1f, 0f);
    [SerializeField] float forcePower;

    readonly string PULL = "pull";
    readonly string FAIL = "fail";
    readonly string OMG = "omg";

    WaitForSeconds WAITONE = new WaitForSeconds(1f);
    WaitForSeconds WAITTHREE = new WaitForSeconds(3f);

    public override void OnInteract()
    {
        PullChair();
    }

    public override void OnStart()
    {
        player = PlayerController.Instance;
        rightHand = player.rig.rightHand;
    }

    void PullChair()
    {
        player.inputEnabled = false;
        player.transform.DOMove(pullingPosition.position, 0.2f);
        //player.transform.LookAt(greenChair);
        player.rig.SetBabyAnimation(PULL);
        player.transform.DOLookAt(greenChair.position - player.transform.position, 0.5f).SetEase(Ease.OutSine).OnComplete(
            ()=> {
                greenChair.SetParent(rightHand);
                player.transform.DOMoveZ(-5f, 1.5f).SetEase(Ease.OutSine).OnComplete(ParentFall);
            });
    }

    void ParentFall()
    {
        

        StartCoroutine(BoyFallRoutine());
    }
    IEnumerator BoyFallRoutine()
    {
        vCam1.Priority = 99;
        yield return WAITONE;

        boyAnimator.CrossFadeInFixedTime(FAIL, 0.25f);

        yield return WAITONE;

        girlAnimator.CrossFadeInFixedTime(OMG, 0.25f);
        girlExclaimation.Play();
        blueChair.DOMoveX(1, 0.25f);

        fallParticle.Play();
        foodSpreadParticle.Play();

        physicsCloth.DOJump(clothLandPosition.position, 2f, 1, 1f);
        staticCloth.gameObject.SetActive(false);

        int max = arbitaryObjects.Count;
        for (int i = 0; i < max; i++)
        {
            arbitaryObjects[i].isKinematic = false;
            arbitaryObjects[i].AddForce(forceDir * forcePower, ForceMode.Impulse);
        }
        int max2 = arbitaryBreakables.Count;
        for (int i = 0; i < max2; i++)
        {
            arbitaryBreakables[i].mainBody.isKinematic = false;
            arbitaryBreakables[i].mainBody.AddForce(forceDir * forcePower, ForceMode.Impulse);
            arbitaryBreakables[i].Break(forceDir, forcePower);
        }
        girlAnimator.transform.LookAt(player.transform);

        yield return WAITTHREE;
        greenChair.parent = null;
        
        LevelManager.Instance.ForceFillMeter();
    }
}
