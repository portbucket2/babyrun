using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class WrapOnToiletLevel70 : Task
{
    [SerializeField] Transform parent;
    [SerializeField] Transform child;
    [SerializeField] GameObject poly;
    [SerializeField] GameObject polyRoll;
    [SerializeField] Transform comod;
    [SerializeField] Transform comodPos;
    [SerializeField] Transform comodPosLeft;
    [SerializeField] ParticleSystem peeParticle;
    [SerializeField] ParticleSystem unrollPolyParticle;

    [SerializeField] ParentRigController rig;

    WaitForSeconds WAITHALF = new WaitForSeconds(0.5f);
    WaitForSeconds WAITONE = new WaitForSeconds(1f);
    WaitForSeconds WAITTHREE = new WaitForSeconds(3f);

    readonly string PICK = "pick";
    readonly string WALK = "walk";
    readonly string PEE = "pee";
    readonly string SLIP = "slip";

    public override void OnInteract()
    {
        StartCoroutine(PeeingRoutine());

        //LevelManager.Instance.MeterFiller();
        //if (ParentController.Instance) ParentController.Instance.GetStartle();
    }

    public override void OnStart()
    {
        //if (ParentController.Instance)
        //    rig = ParentController.Instance.rig;
    }


    IEnumerator PeeingRoutine()
    {
        // baby puts the poly on toilet
        PlayerController.Instance.inputEnabled = false;
        child.LookAt(comod);
        child.DOMove(comodPosLeft.position, 0.3f);
        // child interact
        PlayerController.Instance.rig.SetBabyAnimation(Const.PICK);
        yield return WAITHALF;
        polyRoll.SetActive(false);
        poly.SetActive(true);
        unrollPolyParticle.Play();

        yield return WAITONE;
        PlayerController.Instance.inputEnabled = true;
        //parents walks to it
        ParentWalk();
        // starts peeing

        yield return WAITTHREE;
        //reacts to pee coming back
        // falls on the floor
        ParentFall();
        // level complete
        yield return WAITTHREE;
        LevelManager.Instance.ForceFillMeter();
    }
    void ParentWalk() {
        Vector3 temp = comod.position;
        temp.y = 0;
        Vector3 dir = temp - parent.position;
        parent.DOLookAt(dir, 0.5f);
        rig.SetAnimation(WALK);
        parent.DOMove(comodPos.position, 2f).SetEase(Ease.OutSine).OnComplete(
            () =>
            {
                parent.LookAt(comod);
                StartPeeing();
            });
    }
    void StartPeeing()
    {
        // pee animation
        rig.SetAnimation(PEE);
        //pee particle
        peeParticle.Play();
    }
    void ParentReact() {

    }
    void ParentFall() {
        rig.SetAnimation(SLIP);
    }
}
