using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class TasksRoutine_83 : Task
{
    [SerializeField] Transform airBlower;
    [SerializeField] GameObject task2;
    [SerializeField] Transform lookTarget1;
    [SerializeField] SkinnedMeshRenderer inflatable;
    [SerializeField] Animator boyAnimator;
    [SerializeField] Transform boy;
    [SerializeField] ParticleSystem zzzParticle;

    Vector3 blowerRot = new Vector3(-20f, 143f, -137f);
    Vector3 blowerRot2 = new Vector3(-20f, 143f, -66f);

    Vector3 flyRot = new Vector3(0f, 180f, 0f);

    int taskCount = 0;
    Transform rightHand;
    PlayerController player;

    WaitForEndOfFrame ENDOFFRAME = new WaitForEndOfFrame();
    WaitForSeconds WAITONE = new WaitForSeconds(1f);
    WaitForSeconds WAITFIVE = new WaitForSeconds(5f);

    public override void OnInteract()
    {
        switch (taskCount)
        {
            case 0:
                PickUpBlower();
                break;
            case 1:
                PumpAir();
                break;

            default:
                break;
        }
        taskCount++;
    }

    public override void OnStart()
    {
        player = PlayerController.Instance;
        rightHand = player.rig.rightHand;
    }

    void PickUpBlower() {
        //ParentController.Instance.GetStartle();
        player.rig.SetBabyAnimation(Const.GROUND_PICK);
        airBlower.parent = rightHand;
        airBlower.localPosition = Vector3.zero;
        airBlower.localEulerAngles = blowerRot;
        task2.SetActive(true);
    }
    void PumpAir() {
        StartCoroutine(AirPumpRoutine());
    }
    IEnumerator AirPumpRoutine()
    {
        player.inputEnabled = false;
        player.transform.LookAt(lookTarget1);
        yield return ENDOFFRAME;
        player.rig.SetBabyAnimation(Const.PICK_IDLE);
        airBlower.localEulerAngles = blowerRot2;

        yield return ENDOFFRAME;
        float v = 0;
        DOVirtual.Float(0f, 100f, 4f, v =>
        {
            Debug.Log("print v:" + v);
            inflatable.SetBlendShapeWeight(0, v);

        }).OnComplete(()=> { boyAnimator.CrossFadeInFixedTime("panic", 0.25f);
            zzzParticle.Stop();
            boy.DOMoveY(7, 2f).SetEase(Ease.InSine);
            boy.DOLocalRotate(flyRot, 2f).SetEase(Ease.InSine); ;
        });
        
        yield return WAITFIVE;


        LevelManager.Instance.ForceFillMeter();
    }
}
