using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class SnowBall : MonoBehaviour
{
    [SerializeField] Transform pusher;
    [SerializeField] float startSize;
    [SerializeField] float endSize;
    [SerializeField] float increase;
    [SerializeField] float torque;

    [SerializeField] bool isFollowing = false;

    [SerializeField] Transform animal1;
    [SerializeField] Transform animal2;

    [SerializeField] Transform animal1TT;
    [SerializeField] Transform animal2TT;

    Vector3 animal1Pos;
    Vector3 animal2Pos;


    void Start()
    {
        transform.localScale = new Vector3(startSize, startSize, startSize);
    }

    // Update is called once per frame
    void Update()
    {
        if (isFollowing)
        {
            Rolling();
        }
    }
    void Rolling()
    {
        float scaleFactor = transform.localScale.y;
        transform.position = pusher.position + (pusher.forward * scaleFactor);
        transform.position += new Vector3(0f, scaleFactor, 0f);

        if (Input.GetMouseButton(0))
        {
            if(transform.localScale.x < endSize)
                transform.localScale += transform.localScale * increase;

            AnimalRolling();

            transform.Rotate(Vector3.forward * torque);
        }
    }
    void AnimalRolling()
    {
        if (animal1)
        {
            animal1Pos = Vector3.zero;
            animal1Pos = transform.position + transform.right * (transform.localScale.y / 2f);

            animal1.transform.position = animal1TT.position;

            Vector3 dir = animal1.position - transform.position;
            dir.Normalize();
            Debug.DrawRay(transform.position, dir * 5, Color.green);
            //animal1.LookAt(transform);
            animal1.DOLookAt(transform.position - animal1.position, 0.05f);
        }

        if (animal2)
        {
            animal2Pos = Vector3.zero;
            animal2Pos = transform.position - transform.right * (transform.localScale.y / 2f);

            animal2.transform.position = animal2TT.position;

            Vector3 dir = animal2.position - transform.position;
            dir.Normalize();
            Debug.DrawRay(transform.position, dir * 5, Color.red);
            //animal2.LookAt(transform);
            animal2.DOLookAt(transform.position - animal2.position, 0.05f);
        }

    }

    public void StartRolling(Transform _pusher)
    {
        pusher = _pusher;
        isFollowing = true;
    }
    public void AddAnimal(Transform _animal, bool isSecond)
    {
        if (!isSecond)
        {
            animal1 = _animal;
            //animal1.parent = null;
            //animal1.parent = transform;
        }
        else
        {
            animal2 = _animal;
            //animal2.parent = null;
            //animal2.parent = transform;
        }
    }
    public void StopRolling()
    {
        isFollowing = false;
    }
}
