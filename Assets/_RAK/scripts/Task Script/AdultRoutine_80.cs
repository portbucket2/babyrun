

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FRIA;
public class AdultRoutine_80 : MonoBehaviour
{
    public List<Transform> path;
    public float waitTimeAtStove = 2;
    public float waitTimeAtRacks = 1;
    public float speed = 1;
    public float bombDelay = 0.1f;


    public GameObject tomato;
    public GameObject bomato;
    public Animator animator;
    AdultStateController stateCon;
    public ParticleSystem teaSmokeWhite;
    public ParticleSystem teaSmokeGreen;
    public ParticleSystem vomitingParticle;
    public ParticleSystem vomitParticle;
    public GameObject pickedBombReference;
    public GameObject targetTrigger;
    public List<GameObject> enableOnExplodeList;
    public List<GameObject> disableOnExplodeList;
    public Material sootyMaterial;
    public List<Renderer> tobeSootedRenderers;

    bool roachPicked = false;
    bool isSipping = false;
    // Start is called before the first frame update
    IEnumerator Start()
    {
        //prc_30.onBombPlanted += SetCharge;
        stateCon = GetComponent<AdultStateController>();
        stateCon.stateMate.AddStateEntryCallback(AdultCharState.win, () =>
        {
            StopAllCoroutines();
            stateCon.rig.SetTrigger(Const.STARTLE);
            stateCon.rig.ChangeFace(AdultFace.Surprised);
            stateCon.rig.PlayExclamation();
        });
        stateCon.rig.onAnimationGrab += OnAnimationGrab;



        //yield return StartCoroutine(transform.SteadyWalk(path[0].position, speed, 5));
        $"In play Mode : {LevelManager.Instance.InPlayMode}".Debug("FF00FF");
        while (isOnLoop)
        {

            for (int i = 1; i < path.Count; i++)
            {
                yield return StartCoroutine(transform.SteadyWalk(path[i].position, speed, 5));
            }
            if (planted)
            {

                "cam swapped".Debug("0022FF");
                CamSwapController.SetCamera(CamID.celebration);
            }
            
            stateCon.rig.anim.CrossFadeInFixedTime("grab", .1f);
            bomato.SetActive(false);
            tomato.SetActive(true);
            if (roachPicked) { targetTrigger.SetActive(false); }
            isSipping = true;
            yield return new WaitForSeconds(waitTimeAtRacks);

            if (planted)
            {
                pickedBombReference.SetActive(false);
                bomato.SetActive(true);
                this.Add_DelayedAct(OnExplode, bombDelay);
                vomitingParticle?.Play();
                break;
            }


            stateCon.rig.anim.CrossFadeInFixedTime("mobile_walk", .1f);
            bomato.SetActive(true);
            tomato.SetActive(false);
            if (roachPicked) { targetTrigger.SetActive(true); }
            isSipping = false;
            
            for (int i = path.Count - 2; i >= 0; i--)
            {
                yield return StartCoroutine(transform.SteadyWalk(path[i].position, speed, 5));

            }
            stateCon.rig.anim.CrossFadeInFixedTime("grab", .1f);
            yield return new WaitForSeconds(waitTimeAtStove);
            stateCon.rig.anim.CrossFadeInFixedTime("mobile_walk", .1f);
        }
    }

    [SerializeField] bool planted;
    public void SetCharge()
    {
        planted = true;
        teaSmokeWhite?.Stop();
        teaSmokeGreen?.Play();

    }
    void OnAnimationGrab()
    {
        if (tomato.activeSelf)
        {
            //tomato.SetActive(false);
        }
        else
        {
            
        }
        
    }
    void OnExplode()
    {
        isOnLoop = false;
        StopAllCoroutines();
        bomato.SetActive(false);
        AdultStateController asc = GetComponent<AdultStateController>();
        //asc.rig.anim.enabled = false;
        asc.rig.ChangeFace(AdultFace.Surprised);
        asc.transform.RotateTowards(Camera.main.transform.position, 0.25f);
        animator.CrossFadeInFixedTime("omg", 0.25f);
        
        vomitParticle?.Play();
        LevelManager.Instance.ForceFillMeter();
        enableOnExplodeList.ActivateAll();
        disableOnExplodeList.DeactivateAll();
        //foreach (var item in tobeSootedRenderers)
        //{
        //    item.material = sootyMaterial;
        //}

    }
    public bool isOnLoop = true;

    public void RoachPicked(bool isTrue) { roachPicked = isTrue; }
    public void CoffieMugCheck()
    {
        if (!isSipping)
            targetTrigger.SetActive(true);
    }
}

