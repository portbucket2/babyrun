using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class TasksRoutine_84 : Task
{
    [SerializeField] Transform snake;
    [SerializeField] GameObject target2;
    [SerializeField] GameObject arrow;
    [SerializeField] Transform DropZone;
    [SerializeField] Animator lady;
    [SerializeField] ParticleSystem ladyScared;
    int taskCount = 0;
    Transform rightHand;
    PlayerController player;
    public override void OnInteract()
    {
        switch (taskCount)
        {
            case 0:
                PickUpSnake();
                break;
            case 1:
                DropSnake();
                break;

            default:
                break;
        }
        taskCount++;
    }

    public override void OnStart()
    {
        player = PlayerController.Instance;
        rightHand = player.rig.rightHand;
    }

    void PickUpSnake() {
        ParentController.Instance.GetStartle();
        player.rig.SetBabyAnimation(Const.PICK);
        snake.parent = rightHand;
        snake.localPosition = Vector3.zero;
        target2.SetActive(true);
        arrow.SetActive(true);
    }
    void DropSnake() {

        snake.parent = null;
        snake.DORotate(Vector3.zero, 0.2f);
        snake.DOJump(DropZone.position, 0.5f, 1, 0.5f).SetEase(Ease.InSine).OnComplete(() =>
        {
            ladyGetsScared();
        });
    }
    void ladyGetsScared()
    {
        lady.CrossFadeInFixedTime("omg", 0.25f);
        ladyScared.Play();
        LevelManager.Instance.ForceFillMeter();
    }
}
