using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class PlayerRoutine_80 : MonoBehaviour
{
    public event System.Action onBombPlanted;
    public Transform throwPoint;
    public float angle;
    public AdultRoutine_80 adult;
    public Transform cockroach;
    public void OnInteract()
    {
        StartCoroutine(ThrowRoutine());
    }
    private IEnumerator ThrowRoutine()
    {
        PlayerController.Instance.rig.SetBabyAnimation(Const.THROW);
        PlayerController.Instance.transform.LookAt(transform.position.XZ());
        yield return new WaitForSeconds(0.2f);
        DropCurrentItem();
        yield return new WaitForSeconds(0.2f);
        

    }
    void DropCurrentItem()
    {
        if (PickItem.currentItem) PickItem.currentItem.DropItems(throwPoint, angle);
        if (PickableItem.currentItem) PickableItem.currentItem.DropItems(throwPoint, angle);
        
    }
    public void RoachScale()
    {
        cockroach.DOScale(Vector3.zero, 1f).SetEase(Ease.OutSine);
    }
}
