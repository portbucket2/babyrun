using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class TasksRoutine_90 : Task
{
    [SerializeField] Transform grabPos;
    [SerializeField] SnowBall snowBall;
    [SerializeField] ParticleSystem snowDragParticle;
    [SerializeField] Animator playerAnimator;
    [SerializeField] RuntimeAnimatorController runnningState;
    [SerializeField] RuntimeAnimatorController pushingState;
    [SerializeField] TrailRenderer snowTrail;
    [SerializeField] GameObject task1;
    [SerializeField] GameObject task2;


    int taskCount = 0;
    Transform rightHand;
    PlayerController player;

    public override void OnInteract()
    {
        switch (taskCount)
        {
            case 0:
                GrabSnowBall();
                break;
            //case 1:
            //    DropSnake();
            //    break;
            //case 2:
            //    DropSnake();
            //    break;

            default:
                break;
        }
        taskCount++;
    }

    public override void OnStart()
    {
        player = PlayerController.Instance;
        rightHand = player.rig.rightHand;

        LevelManager.Instance.onLevelFinished += LevelComplete;
        //task1.SetActive(false);
        //task2.SetActive(false);
        //Debug.Log("On start call");
    }

    void GrabSnowBall() {
        snowTrail.enabled = true;
        playerAnimator.runtimeAnimatorController = pushingState;
        ParentController.Instance.GetStartle();
        LevelManager.Instance.MeterFiller();
        player.rig.SetBabyAnimation(Const.GROUND_PICK);
        snowBall.StartRolling(playerAnimator.transform);
        snowDragParticle.Play();


        task1.SetActive(true);
        task2.SetActive(true);
    }
    bool secondAnimal = false;
    public void GetAnimal(Transform _animal) {
        //_animal.parent = grabPos;
        //_animal.localPosition = Vector3.zero;
        //_animal.localEulerAngles = Vector3.zero;

        snowBall.AddAnimal(_animal, secondAnimal);
        
        secondAnimal = true;
        LevelManager.Instance.totalFill += 0.2f;
    }
    public void PlayAnimalAnimation(Animator anim)
    {
        anim.CrossFadeInFixedTime("win", 0.25f);
    }

    public void LevelComplete(bool success)
    {
        if (success)
        {
            Debug.Log("success");
            snowBall.StopRolling();
            //ParentController.Instance.rig.SetAnimation(Const.FAIL);
        }
        else
        {
            Debug.Log("fail");
            snowBall.StopRolling();
        }

    }
}
