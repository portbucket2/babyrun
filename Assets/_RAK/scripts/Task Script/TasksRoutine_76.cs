using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TasksRoutine_76 : Task
{
    [SerializeField] Transform roach;
    [SerializeField] AdultMaleRoutine_76 adult;
    [Header("Debug : ")]
    [SerializeField] PlayerController player;
    [SerializeField] Transform rightHand;
    [SerializeField] Transform selectedObject;

    public override void OnInteract()
    {
        PickUpRoach();
    }

    public override void OnStart()
    {
        player = PlayerController.Instance;
        rightHand = player.rig.rightHand;

        LevelManager.Instance.onLevelFinished += LevelComplete;
    }

    void PickUpRoach()
    {
        LevelManager.Instance.MeterFiller();
        ParentController.Instance.GetStartle();
        player.rig.SetBabyAnimation(Const.PICK);
        roach.parent = rightHand;
        roach.localPosition = Vector3.zero;
    }

    public void LevelComplete(bool success)
    {        
        if (success)
        {
            Debug.Log("success");
            //ParentController.Instance.EndChase(false);
            adult.ChaseFinished();

            //ParentController.Instance.rig.SetAnimation(Const.FAIL);
        }
        else
        {
            Debug.Log("fail");
        }

    }
}
