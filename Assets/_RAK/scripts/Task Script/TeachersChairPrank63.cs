using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class TeachersChairPrank63 : Task
{
    [Header("common : ")]
    [SerializeField] Transform pencil;
    [SerializeField] Transform eraser;
    [SerializeField] Transform chairPos;
    [SerializeField] SphereCollider task2Col;
    [SerializeField] SpriteRenderer task2Ren;
    [Header("adult stuff : ")]
    [SerializeField] Transform adultBody;
    [SerializeField] Animator adultAnimator;
    [SerializeField] Transform stopTarget;
    [SerializeField] Transform lookTarget;
    [SerializeField] Transform adultRightHand;
    [SerializeField] ParticleSystem hitParticle;

    [Header("Debug : ")]
    [SerializeField] PlayerController player;
    [SerializeField] Transform rightHand;
    [SerializeField] Transform selectedObject;
    [SerializeField] bool isRightChoice = false;

    WaitForSeconds WAITHALF = new WaitForSeconds(0.5f);
    WaitForSeconds WAITONE = new WaitForSeconds(1f);
    WaitForSeconds WAITTHREE = new WaitForSeconds(3f);

    readonly string WALK = "walk";
    readonly string IDLE = "idle";
    readonly string SITSUCCESS = "sit_Success";
    readonly string SITFAIL = "sit_Fail";

    public override void OnInteract()
    {
        PlaceObject();
    }

    public override void OnStart()
    {
        player = PlayerController.Instance;
        rightHand = player.rig.rightHand;
        task2Col.enabled = false;
        task2Ren.enabled = false;
    }

    
    public void OnCorrctChoice()
    {
        PlayerController.Instance.rig.SetBabyAnimation(Const.PICK);
        pencil.parent =rightHand;
        pencil.localPosition = Vector3.zero;
        selectedObject = pencil;
        task2Col.enabled = true;
        task2Ren.enabled = true;
        isRightChoice = true;
    }
    public void OnWrongChoice()
    {
        PlayerController.Instance.rig.SetBabyAnimation(Const.PICK);
        eraser.parent = rightHand;
        eraser.localPosition = Vector3.zero;
        selectedObject = eraser;
        task2Col.enabled = true;
        task2Ren.enabled = true;
        isRightChoice = false;
    }
    public void PlaceObject()
    {
        PlayerController.Instance.rig.SetBabyAnimation(Const.PICK);
        selectedObject.parent = null;
        selectedObject.position = chairPos.position;
        selectedObject.eulerAngles = new Vector3(0f, 0f, -90f);
        //AdultCharState character starts walking
        adultAnimator.CrossFadeInFixedTime(WALK, 0.25f);
        adultBody.DOLookAt(stopTarget.position - adultBody.position, 1f).OnComplete(
            () =>{
                adultBody.DOMove(stopTarget.position, 2f).SetEase(Ease.OutSine).OnComplete(AdultSit);
            });        
    }

    void AdultSit()
    {
        StartCoroutine(SittingRoutine());
    }
    IEnumerator SittingRoutine()
    {
        adultAnimator.CrossFadeInFixedTime(IDLE, 0.25f);
        yield return WAITHALF;
        adultBody.LookAt(lookTarget);
        yield return WAITHALF;
        if (isRightChoice)
        {
            adultAnimator.CrossFadeInFixedTime(SITSUCCESS, 0.25f);            
        }
        else
        {
            adultAnimator.CrossFadeInFixedTime(SITFAIL, 0.25f);
        }
        hitParticle.Play();
        LevelManager.Instance.ForceFillMeter();
        yield return WAITONE;
        selectedObject.parent = adultRightHand;
        selectedObject.localPosition = Vector3.zero;
    }
}
