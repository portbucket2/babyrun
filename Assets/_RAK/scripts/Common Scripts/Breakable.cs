using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Breakable : MonoBehaviour
{

    [SerializeField] float explosionForce = 1;
    [SerializeField] public Rigidbody mainBody;
    [SerializeField] List<Rigidbody> smallBodies;

    void Start()
    {
        int max = smallBodies.Count;
        for (int i = 0; i < max; i++)
        {
            smallBodies[i].isKinematic = true;
        }
    }
    public void Break(Vector3 dir , float _explosionForce)
    {
        explosionForce = _explosionForce;
        int max = smallBodies.Count;
        for (int i = 0; i < max; i++)
        {
            smallBodies[i].isKinematic = false;
            dir.Normalize();
            smallBodies[i].AddForce(dir * explosionForce, ForceMode.Impulse);

        }
    }
}
