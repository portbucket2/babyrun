using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AutoFollow : MonoBehaviour
{
    [SerializeField] Transform target;
    [SerializeField] Transform follower;

    [Header("Debug : ")]
    [SerializeField] bool isFollowing = false;

    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (isFollowing)
        {
            follower.transform.position = target.transform.position;
        }
    }
    public void Follow(bool _isOn)
    {
        isFollowing = _isOn;
    }
}
