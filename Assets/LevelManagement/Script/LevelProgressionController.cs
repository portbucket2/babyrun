﻿using UnityEngine;
using UnityEngine.UI;
using PotatoSDK;
using UnityEngine.Video;
using FRIA;
public class LevelProgressionController : MonoBehaviour
{
    public const bool CHECHRA_MODE = true;
    public static LevelProgressionController Instance { get; private set; }

    public GameObject root;
    public Button claimButton;
    public Button skipButton;
    public GameObject skipButtonObject;
    //public Image levelPhoto;

    public int targetLevelNumber;
    public LevelData targetLevelData => BuildSceneOrganizer.GetLevelData(targetLevelNumber);

    public VideoPlayer vplayer;
    void Start()
    {
        Instance = this;
        root.SetActive(false);
        skipButtonObject.SetActive(false);
    }

    void Load(System.Action skipAction)
    {
        root.SetActive(true);
        claimButton.onClick.RemoveAllListeners();
        skipButton.onClick.RemoveAllListeners();

        claimButton.onClick.AddListener(OnClaim);

        skipButton.onClick.AddListener(()=>skipAction?.Invoke());
        //Sprite sp = Resources.Load<Sprite>(string.Format("LevelPhotos/LevelPhoto_{0}",levelData.id));
        //levelPhoto.gameObject.SetActive(sp);
        //levelPhoto.sprite = sp;

        VideoClip  vc = Resources.Load<VideoClip>(string.Format("Videos/Level_{0}", targetLevelData.id));
        vplayer.clip = vc;
        vplayer.Play();
    }
    void OnClaim()
    {
        claimButton.interactable = false;
        skipButton.interactable = false;
        //AnalyticsAssistant.LogRv(RVState.rv_click, RVPlacement.rewarded_level_unlock);
        MAXMan.ShowRV((bool success) => {
            if (success)
            {
                claimButton.onClick.RemoveAllListeners();
                skipButton.onClick.RemoveAllListeners();
                //LevelLoader.SelectedLevel.UnlockBonusLevel();
                AnalyticsAssistant.ReportCollect(AnalyticCollectType.RV_BONUS_LEVEL, targetLevelNumber);
                LevelLoader.LoadLevelDirect(autoTapEnable: false, targetLevelNumber);
                //AnalyticsAssistant.LogRv(RVState.rv_complete, RVPlacement.rewarded_level_unlock);
            }
            else
            {
                claimButton.interactable = true;
                skipButton.interactable = true;
            }

        });
        //if(willShow)AnalyticsAssistant.LogRv(RVState.rv_start, RVPlacement.rewarded_level_unlock);
    }
    void OnSkip_Progress()
    {
        claimButton.onClick.RemoveAllListeners();
        skipButton.onClick.RemoveAllListeners();
        AnalyticsAssistant.ReportClick(AnalyticClickType.SKIP_BONUS_LEVEL, LevelLoader.SelectedLevelNumber);
        LevelLoader.Instance.IncreaseGameLevel(); 
        if (CHECHRA_MODE && !fromHome && BuildSpecManager.enableInterstitial) MAXMan.ShowInterstitial();// TimedAd.AB_ControlledAd(null, IntAdPlacement.Success_LevelDeparture);

        LevelLoader.LoadLevelDirect(autoTapEnable:fromHome);
    }
    void OnSkip_ListLoad()
    {
        claimButton.onClick.RemoveAllListeners();
        skipButton.onClick.RemoveAllListeners();
        //AnalyticsAssistant.ReportClick(AnalyticClickType.SKIP_BONUS_LEVEL, LevelLoader.SelectedLevelNumber);
        //LevelLoader.Instance.IncreaseGameLevel();
        //if (CHECHRA_MODE && !fromHome && BuildSpecManager.enableInterstitial) MAXMan.ShowInterstitial();// TimedAd.AB_ControlledAd(null, IntAdPlacement.Success_LevelDeparture);

        //LevelLoader.LoadLevelDirect(autoTapEnable: fromHome);
        root.SetActive(false);

        BridgeSceneManager.instance.SwitchState(BridgeState.LEVEL_LOAD);
    }
    public static bool fromHome;


    public static bool Proceed_IsBonus(bool fromHome)
    {
        //CrossPromoController.Active(false);
        LevelProgressionController.fromHome = fromHome;

        if(Instance)Instance.skipButtonObject.SetActive(false);
        if (Instance) Instance.Add_DelayedAct(() => {
            if(Instance)Instance.skipButtonObject.SetActive(true);
        },2);
        Instance.targetLevelNumber = LevelLoader.SelectedLevelNumber;
        
        
        if (fromHome)
        {
            if (Instance.targetLevelData.isBonusLocked)
            {
                Instance.Load(Instance.OnSkip_Progress);
                return true;
            }
            else
            {
                LevelLoader.LoadLevelDirect(autoTapEnable: true, Instance.targetLevelNumber);
                return false;
            }
        }
        else
        {
            if (LevelLoader.ShouldVisitHome)
            {
                if (BuildSpecManager.enableInterstitial) MAXMan.ShowInterstitial();
                LevelLoader.LoadHome(); 
                //TimedAd.AB_ControlledAd(null, IntAdPlacement.Success_LevelDeparture);
                return false;
            }
            else
            {
                if (Instance.targetLevelData.isBonusLocked)
                {
                    if (CHECHRA_MODE && BuildSpecManager.enableInterstitial) MAXMan.ShowInterstitial();// TimedAd.AB_ControlledAd(null, IntAdPlacement.Success_LevelDeparture);
                    Instance.Load(Instance.OnSkip_Progress);
                    return true;

                }
                else
                {
                    if (BuildSpecManager.enableInterstitial) MAXMan.ShowInterstitial();
                    LevelLoader.LoadLevelDirect(autoTapEnable: false, Instance.targetLevelNumber);
                    return false;
                }
            }
        }               
    }


    public static bool LoadLeverFromList(int levelNumber)
    {
        if (Instance) Instance.skipButtonObject.SetActive(true);

        Instance.targetLevelNumber = levelNumber;

        if (Instance.targetLevelData.isBonusLocked)
        {
            Instance.Load(Instance.OnSkip_ListLoad);
            return true;
        }
        else
        {
            LevelLoader.LoadLevelDirect(autoTapEnable: true, Instance.targetLevelNumber);
            return false;
        }

    }

}
