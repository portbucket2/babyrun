using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using PotatoSDK;
public class SplashScreenLevelLoader : MonoBehaviour
{

	[Range(0f, 5f)]
	public float defaultDelay;

	void Start()
	{
		LoadSceneWithProgressBar();
	}



	public void LoadSceneWithProgressBar()
	{

		StartCoroutine(ControllerForLoadSceneWithProgressbar());
	}


	//--------------------------------------------------
	#region Configuretion	:	LoadScene (With Progression Bar)

	private const float LOAD_READY_PERCENTAGE = 0.9f;
	public static bool loadComplete;
	private IEnumerator ControllerForLoadSceneWithProgressbar()
	{

		yield return new WaitForSeconds(defaultDelay);

		float timePast = 0;

		//while (!IAPManager.isIAPReady)
		//{
		//	timePast += Time.deltaTime;
		//	if (timePast > 5) break;
		//	yield return null;
		//}


		while (!Potato.IsReady)
		{
			yield return null;
		}


		//LifetimeEvents.Report_GameStart();
		WaitForSeconds t_CycleDelay = new WaitForSeconds(0.1f);

		AsyncOperation t_AsyncOperationForLoadScene = LevelLoader.FirstLoadAsync();
		t_AsyncOperationForLoadScene.allowSceneActivation = false;

		float t_LoadingProgression;

		while (!t_AsyncOperationForLoadScene.isDone)
		{

			t_LoadingProgression = t_AsyncOperationForLoadScene.progress;

			//if (progressionBar != null)
			//{
			//	progressionBar.fillAmount = t_LoadingProgression / LOAD_READY_PERCENTAGE;
			//}

			if (t_LoadingProgression >= LOAD_READY_PERCENTAGE)
			{
				break;
			}

			yield return t_CycleDelay;
		}

		//AnalyticsAssistant.LogLevelStarted(LevelManager.Instance.GetEffectiveLevelNumber());
		loadComplete = true;
		t_AsyncOperationForLoadScene.allowSceneActivation = true;
		MAXMan.ShowBanner();
		//StopCoroutine(ControllerForLoadSceneWithProgressbar(""));
	}

	#endregion
}
