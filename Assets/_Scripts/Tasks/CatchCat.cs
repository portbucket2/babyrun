﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CatchCat : Task
{
    [SerializeField] private Collider catCollider;
    [SerializeField] private Animator catAnim;
    [SerializeField] private ParticleSystem horizontalscratch;
    [SerializeField] private ParticleSystem VerticalscratchX;
    [SerializeField] private ParticleSystem VerticalscratchZ;

    public override void OnInteract()
    {
        Getcaught();
    }

    private void Getcaught()
    {
        transform.SetParent(playerController.transform, true);
        transform.localRotation = Quaternion.identity;
        transform.localPosition = 0.6f * Vector3.forward;
        catAnim.SetTrigger("claw");
        gameObject.layer = LayerMask.NameToLayer("Player");

        playerController.CatCaught();
        levelManager.MeterFiller();
        if (parentController) parentController.GetStartle();

        GameObject childCollider = new GameObject("ChildCollider") {layer = LayerMask.NameToLayer("Player")};

        childCollider.transform.SetParent(playerController.transform);
        childCollider.transform.position = transform.position;
        childCollider.transform.rotation = transform.rotation;
        childCollider.transform.localScale = transform.localScale;

        SphereCollider newCollider = childCollider.AddComponent<SphereCollider>();
        newCollider.center = ((SphereCollider)catCollider).center;
        newCollider.radius = ((SphereCollider)catCollider).radius * 0.9f;
        newCollider.isTrigger = false;

        VerticalscratchX.Play();
        VerticalscratchZ.Play();
        horizontalscratch.Play();
    }

    public void LeaveCat()
    {
        VerticalscratchX.Stop();
        VerticalscratchZ.Stop();
        horizontalscratch.Stop();


        transform.SetParent(null, true);
        catCollider.isTrigger = false;
        playerController.babyState = BabyState.Normal;
        //catCollider.attachedRigidbody.useGravity = true;
        //catCollider.attachedRigidbody.isKinematic = false;
        StartCoroutine(DashForward(2f));
    }

    IEnumerator DashForward(float speed)
    {
        catAnim.SetTrigger("run");
        float t = 0;
        Vector3 pos = transform.position;
        while (t < 2)
        {
            transform.Translate(transform.forward * speed * Time.deltaTime, Space.World);
            catCollider.attachedRigidbody.MovePosition(pos + transform.forward * t * speed);
            t += Time.deltaTime;
            yield return null;
        }

        catAnim.SetTrigger("lick");
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.gameObject.layer == LayerMask.NameToLayer("scratchable"))
        {
            catAnim.SetBool("stand", true);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.layer == LayerMask.NameToLayer("scratchable"))
        {
            catAnim.SetBool("stand", false);
        }
    }

    private PlayerController playerController;
    private ParentController parentController;
    private LevelManager levelManager;

    public override void OnStart()
    {
        playerController = PlayerController.Instance;
        parentController = ParentController.Instance;
        levelManager = LevelManager.Instance;

        levelManager.beforeLevelFinished = LeaveCat;
    }
}
       