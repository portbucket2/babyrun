﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestructPillow : Task
{
    [SerializeField] private Collider[] childColliders;
    [SerializeField] private ParticleSystem destructionParticles;
    [SerializeField] private float bonusFill;
    public override void OnInteract()
    {
        LevelManager.Instance.totalFill += bonusFill;
        destructionParticles.Play();

        foreach (Collider c in childColliders)
        {
            c.attachedRigidbody.isKinematic = false;
            c.attachedRigidbody.useGravity = true;
        }
    }

    public override void OnStart()
    {
        
    }
}
