using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickItem : Task
{
    public static PickItem currentItem;
    [SerializeField] private bool connectToSystem = true;
    [SerializeField] internal GrabableItem rightHandItem;
    [SerializeField] internal GrabableItem leftHandItem;
    [SerializeField] internal Transform dropTargetPoint;
    [SerializeField] internal float angle = 0;
    public override void OnInteract()
    {


        if (currentItem) return;
        currentItem = this;

        PlayerController.Instance.PickupBehavior(this.transform, pickType, HoldItem);
        if (!connectToSystem) return;

        levelManager.MeterFiller();
        if (parentController) parentController.GetStartle();

    }

    private void HoldItem()
    {
        PlayerController.Instance.SetHolding();
        if (rightHandItem)
        {
            rightHandItem.HoldItem(false);
        }
        if (leftHandItem)
        {
            leftHandItem.HoldItem(true);
        }
        LevelManager.Instance.onLevelFinished += (bool success) =>
        {
            DropItems(null, 0);
        };
    }

    public void DropItems(Transform target, float angle)
    {
        if (!currentItem) return;
        if (rightHandItem)
        {
            rightHandItem.DropItem(target, angle);
        }
        if (leftHandItem)
        {
            leftHandItem.DropItem(target, angle);
        }
        PlayerController.Instance.rig.SetBabyAnimation(Const.HOLDING, false);
        currentItem = null;
    }

    public IEnumerator DefaultDropRoutine()
    {
        if (currentItem)
        {
            PlayerController.Instance.rig.SetBabyAnimation(Const.THROW);
            if(dropTargetPoint) PlayerController.Instance.transform.LookAt(dropTargetPoint.position.XZ());
            yield return new WaitForSeconds(0.2f);
            DropItems(dropTargetPoint, angle);
        }
    }

    #region OnStart
    //private PlayerController playerController;
    private ParentController parentController;
    private LevelManager levelManager;


    [SerializeField] private PickType pickType;

    public override void OnStart()
    {
        //playerController = PlayerController.Instance;
        parentController = ParentController.Instance;
        levelManager = LevelManager.Instance;
    }
    #endregion
}
