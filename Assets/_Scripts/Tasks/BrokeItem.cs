using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BrokeItem : Task
{
    [SerializeField] private GameObject[] solidPiece;
    [SerializeField] private GameObject[] brokenPiece;

    public override void OnInteract()
    {
        foreach (var item in solidPiece)
        {
            item.SetActive(false);
        }

        foreach (var item in brokenPiece)
        {
            item.SetActive(true);
        }
    }

    public override void OnStart()
    {
        //
    }
}
