using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class FallItem : Task
{
    [SerializeField] private Transform target;
    [SerializeField] private float power;
    [SerializeField] private float duration;
    [SerializeField] private bool broke;
    [SerializeField] private GameObject[] solidPiece;
    [SerializeField] private GameObject[] brokenPiece;

    [SerializeField] private Animator targetAmim;

    private bool interactable = true;
    public override void OnInteract()
    {
        if (interactable)
        {
            interactable = false;
            transform.DOJump(target.position, power, 1, duration).OnStepComplete(() =>
            {
                Broke();
                targetAmim.SetBool("hit", true);
                LevelManager.Instance.MeterFiller();
                LevelManager.Instance.totalFill = 1f;
                PlayerController.Instance.MakeKinematic();
            });

        }
    }

    private void Broke()
    {
        if (!broke) return;

        foreach (var item in solidPiece)
        {
            item.SetActive(false);
        }

        foreach (var item in brokenPiece)
        {
            item.SetActive(true);
        }
    }

    public override void OnStart()
    {
        //
    }
}
