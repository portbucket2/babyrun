using UnityEngine;

public abstract class Task : MonoBehaviour
{
    public abstract void OnStart();
    public abstract void OnInteract();
}
