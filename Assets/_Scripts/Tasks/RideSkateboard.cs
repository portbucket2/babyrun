using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RideSkateboard : MonoBehaviour
{
    private PlayerController playerController;

    private bool started = false;

    private void Update()
    {
        if (!started && LevelManager.Instance.GameStarted)
        {
            started = true;

            //transform.parent = playerController.transform;
            //transform.ResetLocalPositionAndRotation();
            //playerController.rig.transform.localPosition = Vector3.up * 0.03f;
            //playerController.babyState = BabyState.Vehicle;
            //playerController.rig.SetBabyAnimation("onboard", true);
            playerController.moving = false;
            //playerController.rb.freezeRotation = false;

            LevelManager.Instance.MeterFiller();
            ParentController.Instance.GetStartle();
        }
    }

    private void Start()
    {
        playerController = PlayerController.Instance;
    }
}
