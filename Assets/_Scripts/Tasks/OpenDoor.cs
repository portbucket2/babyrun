using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OpenDoor : Task
{
    public float openingTime = 1f;
    public Transform openingPoint;
    public GameObject deactivate;
    public GameObject activate;

    public override void OnInteract()
    {
        playerController.babyState = BabyState.Interested;
        playerController.rb.MovePosition(openingPoint.position);
        playerController.rb.MoveRotation(openingPoint.rotation);
        playerController.rig.SetBabyAnimation(Const.OPEN);

        Invoke(nameof(OnOpen), 0.45f);
    }

    private void OnOpen()
    {
        playerController.babyState = BabyState.Normal;
        playerController.rig.SetBabyAnimation(playerController.moving ? Const.RUN : Const.IDLE);
        StartCoroutine(OpenRoutine());
    }

    private IEnumerator OpenRoutine()
    {
        float t = 0f;
        while (t < openingTime)
        {
            transform.eulerAngles = 90 * (1 - (t / openingTime)) * Vector3.up;
            t += Time.deltaTime;
            yield return null;
        }

        transform.rotation = Quaternion.identity;

        if (deactivate) deactivate.Inactive();
        if (activate) activate.Active();
    }

    private PlayerController playerController;
    private ParentController parentController;
    private LevelManager levelManager;

    public override void OnStart()
    {
        playerController = PlayerController.Instance;
        parentController = ParentController.Instance;
        levelManager = LevelManager.Instance;
    }
}
