using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DropableItem : Interactable
{
    [SerializeField] private Collider itemCollider;
    [SerializeField] private Rigidbody rb;
    [SerializeField] internal Transform dropOffPoint;
    [SerializeField] private ParticleSystem itemParticle;
    [SerializeField] private bool isTriggerAtEnd;

    public override void OnInteract()
    {
        playerController.babyState = BabyState.Interested;
        playerController.rig.SetBabyAnimation(Const.PICK);

        transform.parent = dropOffPoint;
        transform.localPosition = new Vector3(UnityEngine.Random.Range(-0.2f, 0.2f), 0f, UnityEngine.Random.Range(-0.2f, 0.2f));
        itemCollider.isTrigger = isTriggerAtEnd;
        rb.isKinematic = false;
        rb.useGravity = true;
    }

    #region OnStart
    private PlayerController playerController;
    private LevelManager levelManager;


    public override void OnStart()
    {
        playerController = PlayerController.Instance;
        levelManager = LevelManager.Instance;
    }
    #endregion
}
