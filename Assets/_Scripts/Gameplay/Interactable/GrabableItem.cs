using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class GrabableItem : MonoBehaviour
{
    public Collider itemCollider;
    public ParticleSystem itemParticle;
    public bool isTriggerAtEnd;

    internal Rigidbody rb;
    public void HoldItem(bool leftHand)
    {
        //transform.SetParent(go.transform);
        transform.SetParent(leftHand ? PlayerController.Instance.rig.leftHand : PlayerController.Instance.rig.rightHand, true);
        transform.localPosition = Vector3.zero;
        transform.localRotation = Quaternion.identity;
        if (itemParticle) ParticleControl(true);


    }

    public void DropItem(Transform target, float angle)
    {
        rb = transform.GetComponentInChildren<Rigidbody>();
        if (!rb) rb = transform.gameObject.AddComponent<Rigidbody>();

        transform.parent = null;
        itemCollider.isTrigger = isTriggerAtEnd;
        if (itemParticle)
        {
            itemParticle.Stop();
            ParticleControl(false);
        }

        if (target)
        {
            if (angle > 1)
            {
                rb.isKinematic = false;
                rb.useGravity = true;
                rb.Throw(target.position, angle);
            }
            else
            {
                transform.rotation = Quaternion.identity;
                transform.DOJump(target.position, 0.5f, 1, 0.5f);
            }
        }
        else
        {
            rb.isKinematic = false;
            rb.useGravity = true;
            rb.AddForce(1f * Vector3.up, ForceMode.Impulse);
        }

    }

    public void ParticleControl(bool setParticle)
    {
        if (setParticle)
        {
            PlayerController.Instance.OnBabyMove += itemParticle.Play;
            PlayerController.Instance.OnBabyStop += itemParticle.Stop;

            if (PlayerController.Instance.moving) itemParticle.Play();
        }
        else
        {
            PlayerController.Instance.OnBabyMove -= itemParticle.Play;
            PlayerController.Instance.OnBabyMove -= itemParticle.Stop;
        }
    }
}