using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BabyRigController : MonoBehaviour
{
    [SerializeField] internal Animator anim;
    [SerializeField] internal GameObject[] babyFaces;
    [SerializeField] internal GameObject tears;
    public Transform rightHand;
    public Transform leftHand;

    private int currentFace = 0;
    public void ChangeFace(BabyFace index)
    {
        if ((int)index>= babyFaces.Length) return;

        babyFaces[currentFace].Inactive();
        currentFace = (int)index;
        babyFaces[currentFace].Active();
    }

    public void SetBabyAnimation(string stateName) => anim.CrossFadeInFixedTime(stateName, 0.25f);
    public void SetBabyAnimation(string stateName, bool value) => anim.SetBool(stateName, value);

    #region presets
    public void Cry()
    {
        if (tears) tears.SetActive(true);
        ChangeFace(BabyFace.Cry);
        SetBabyAnimation(Const.CRY);
    }
    public void CelebrateWin()
    {
        ChangeFace(BabyFace.Happy);
        SetBabyAnimation(Const.WIN);
    }

    public void PickStanding()
    { 
    }
    public void PickBending()
    {
        
    }
    #endregion

}
public enum BabyState { Normal, Interested, Drag, Vehicle }
public enum BabyFace { Normal, Naughty, Happy, Devil, Cry }