using UnityEngine;
using DG.Tweening;
using UnityEngine.Events;

public class TaskIndicator : MonoBehaviour
{
    [SerializeField] internal Task task;
    private enum TriggerType { Enter, Stay }
    [SerializeField] private TriggerType triggerType;
    [SerializeField] internal GameObject activate;
    [SerializeField] internal GameObject deactivate;
    [SerializeField] internal ParticleSystem particle;
    [SerializeField] internal float particleDelay;
    [SerializeField] internal bool waitForTap = false;
    [SerializeField] internal bool onlyEnptyHand = false;
    [SerializeField] internal UnityEvent onInteractEvent;

    private void Start()
    {
        if (task) task.OnStart();
        transform.DOScale(0.8f, 0.5f).SetLoops(-1, LoopType.Yoyo);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (triggerType == TriggerType.Stay) return;
        if (!LevelManager.Instance.InPlayMode) return;
        if (PickItem.currentItem && onlyEnptyHand) return;
        OnTrigger();
    }

    private void OnTriggerStay(Collider other)
    {
        if (triggerType == TriggerType.Enter) return;
        if (!LevelManager.Instance.InPlayMode) return;
        if (PickItem.currentItem && onlyEnptyHand) return;
        if (!waitForTap || Input.GetMouseButton(0)) OnTrigger();
    }

    private void OnTrigger()
    {
        if (task) task.OnInteract();
        onInteractEvent?.Invoke();
        if (deactivate) deactivate.Inactive();
        if (activate) activate.Active();
        if (particle) DOVirtual.DelayedCall(particleDelay, particle.Play);
        LevelManager.Instance.itemCount++;
        gameObject.SetActive(false);
    }
}
