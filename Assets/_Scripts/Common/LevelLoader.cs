﻿using System.Collections;
using System.Collections.Generic;
using PotatoSDK;
using UnityEngine;
using UnityEngine.SceneManagement;
using FRIA;


public class LevelLoader : MonoBehaviour
{
    private static LevelLoader instance;
    private static string GAME_LEVEL_PREFERENCE_KEY = "LEVEL_INDEX";
    static bool LEVEL_ROAMING_WAS_UNLOCKED = false;
    [SerializeField] private int initialLevelPreference = 0;
    [SerializeField] private bool unlockLevelRoaming = false;

    #region Static Load Calls

    private static PotatoSDK.HardData<bool> isFirstTimeLoad;
    public static bool ShouldVisitHome
    {
        get
        {
            if (!LEVEL_ROAMING_WAS_UNLOCKED) return false;
            int N = 5;// ABManager.GetValueInt(ABtype.home_return_freq);
            switch (N)
            {
                default: return ((currentSelectionNumber-1) % N == 0);
                case 0: return false;
                    //case 1: return true;
                    //case 2: return (GetLatestReachedLevelNumber() >= 4 && (instance.currentEffectiveLevelNumber+0) % 2 == 0);
                    //case 3: return (GetLatestReachedLevelNumber() >= 4 && (instance.currentEffectiveLevelNumber+2) % 3 == 0);
            }
        }
    }
    //public static bool autoTap { get; private set; }
    //public static PrankCollectibleProfile pendingCollectible;
    public static bool showStartScreen { get; private set; }
    //public static bool blockTapButton { get { return pendingCollectible != null; } }
    public static void LoadHome()
    {
        SceneManager.LoadScene(BuildSceneOrganizer.BRIDGE_SCENE_NAME);
    }

    public static void LoadLevelDirect(bool autoTapEnable = false, int LevelNumber = -1)
    {

        if (LevelNumber < 0) LevelNumber = SelectedLevelNumber;
        else instance.ForceCurrentEffectiveNumberTo(LevelNumber);

        //pendingCollectible = PrankCollectibleManager.GetProfile_ByLevel(SelectedLevel.id);
        //if (SelectedLevelNumber == ReviewPromptManager.LEVEL_START_NUMBER_TO_SHOW)
        //{
        //    Debug.Log("Auto tap disabled for review level");
        //    showStartScreen = true;
        //}
        //else
        {
            showStartScreen = !autoTapEnable;
        }

        $"level to load {SelectedLevelNumber}".Debug("FF0000");

        string t_Scene = Instance.GetNameOfSceneToLoad();

        SceneManager.LoadScene(t_Scene);
        //AnalyticsAssistant.LogLevelStarted(SelectedLevelNumber);
    }

    public static AsyncOperation FirstLoadAsync()
    {
        if (isFirstTimeLoad == null) isFirstTimeLoad = new PotatoSDK.HardData<bool>("IsFirstTimeLoadHD", true);
        string t_Scene = Instance.GetNameOfSceneToLoad();
        if (!isFirstTimeLoad)
        {
            t_Scene = BuildSceneOrganizer.BRIDGE_SCENE_NAME;
        }
        else
        {
            showStartScreen = true;
            isFirstTimeLoad.value = false;
        }
        AsyncOperation asop = SceneManager.LoadSceneAsync(t_Scene);
        asop.completed += (AsyncOperation asp) => {
            //if (t_Scene != BuildSceneOrganizer.BRIDGE_SCENE_NAME)
                //AnalyticsAssistant.LogLevelStarted(SelectedLevelNumber);
        };
        return asop;
    }

    #endregion

    #region Current Level Management
    private static PotatoSDK.HardData<int> _latestLevel;
    private static void InitializeCurrentNumbers()
    {
        if (_latestLevel == null)
        {
            int presetInitialNumber = instance ? instance.initialLevelPreference : 0;
            presetInitialNumber = presetInitialNumber >= 1 ? presetInitialNumber : 1;
            _latestLevel = new PotatoSDK.HardData<int>(GAME_LEVEL_PREFERENCE_KEY, presetInitialNumber);
            _currentEffectiveLevelNumberPointer = _latestLevel;
            //$"init latest {_latestLevel.value}".Debug("FF0000");
        }
    }

    private static int _currentEffectiveLevelNumberPointer = -1;
    private static int currentSelectionNumber
    {
        get
        {
            InitializeCurrentNumbers();
            return _currentEffectiveLevelNumberPointer;
        }
        set
        {
            InitializeCurrentNumbers();
            _currentEffectiveLevelNumberPointer = value;
            if (value > _latestLevel)
            {
                _latestLevel.value = value;
                //LifetimeEvents.Report_NewLevelReached(value);
            }
        }
    }
    public void IncreaseGameLevel()
    {
        if (Application.isPlaying)
        {
            if (!LEVEL_ROAMING_WAS_UNLOCKED) return;
        }
        else
        {
            if (!unlockLevelRoaming) return;
        }
        currentSelectionNumber++;
        //Debug.LogError("CurrentLevelIndex Increased To" + currentEffectiveLevelNumber);
    }

#if UNITY_EDITOR
    public static void UnlockAllNow()
    {
        _latestLevel.value = BuildSceneOrganizer.Instance.levelCount;
    }
#endif
    public void DecreaseGameLevel()
    {
        if (Application.isPlaying)
        {
            if (!LEVEL_ROAMING_WAS_UNLOCKED) return;
        }
        else
        {
            if (!unlockLevelRoaming) return;
        }
        currentSelectionNumber--;
        if (currentSelectionNumber <= 0) currentSelectionNumber = 1;
        //Debug.LogError(currentEffectiveLevelNumber + " CurrentLevelIndex"); ;
    }
    public void ForceCurrentEffectiveNumberTo(int value)
    {
        currentSelectionNumber = value;
    }
    public static int GetLatestReachedLevelNumber()
    {
        InitializeCurrentNumbers();
        return _latestLevel.value;
    }
    public static int SelectedLevelNumber
    {
        get
        {
            return currentSelectionNumber;
        }
    }
    public static LevelData SelectedLevel
    {
        get
        {
            return BuildSceneOrganizer.GetLevelData(currentSelectionNumber);
        }
    }
    #endregion

    public string GetID()
    {
        if (LEVEL_ROAMING_WAS_UNLOCKED)
        {
            return SelectedLevel.id;
        }
        else return SceneManager.GetActiveScene().name.Split('l')[1];// initialLevelPreference.ToString();
    }





    void Awake()
    {
        if (!instance)
        {
            instance = this;
            if (unlockLevelRoaming)
            {
                LEVEL_ROAMING_WAS_UNLOCKED = true;
            }

        }

    }

    private void Start()
    {
        
    }

    public static LevelLoader Instance
    {
        get
        {
            return instance;
        }
    }



    public string GetEditorDisplayText()
    {
        if (!PlayerPrefs.HasKey(GAME_LEVEL_PREFERENCE_KEY))
        {
            return "Saved Index: NULL";
        }
        else
        {
            return string.Format("Saved Index: {0}", BuildSceneOrganizer.GetLevelData(currentSelectionNumber).id);
        }
    }


    public string GetDisplayTextForLevelNumber(int levelNumber = -1)
    {
        if (levelNumber <= 0) levelNumber = currentSelectionNumber;
        if (!LEVEL_ROAMING_WAS_UNLOCKED)
        {
            levelNumber = instance.initialLevelPreference;
        }
        else if (SelectedLevel != null && SelectedLevel.isBonus)
        {
            return "Bonus Level";
        }
        return string.Format("Level {0}", levelNumber);
    }

    public string GetNameOfSceneToLoad(int i = -1)
    {
        if (i < 0) i = currentSelectionNumber;
        if (!LEVEL_ROAMING_WAS_UNLOCKED) return UnityEngine.SceneManagement.SceneManager.GetActiveScene().name;
        return BuildSceneOrganizer.GetLevelData(i).sceneName;
    }


    //=====================================================================================
    /*
    public static LevelLoader Instance { get; private set; }
    public static int CurrentLevel => leveldata.value + 1;
    public static int LevelToLoad => (leveldata.value % numOfLevel) + 1;

    private static HardData<int> leveldata;
    private static readonly int numOfLevel = 60;

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
            leveldata = new HardData<int>("LEVEL_INDEX", 0);
            DontDestroyOnLoad(gameObject);
        }
        else if (Instance != this)
        {
            Destroy(gameObject);
        }

        Debug.Log(numOfLevel);
    }

    private IEnumerator Start()
    {
        yield return new WaitUntil(() => Potato.IsReady);
        LoadLevel();
    }

    public static void LevelCompleted()
    {
        Debug.LogFormat($"<color=#FFA500>Level {CurrentLevel} Completed</color>");
        leveldata.value++;
        if (CurrentLevel > 2) MAXMan.ShowInterstitial();
        LoadLevel();
    }

    public static void LevelRestart()
    {
        AnalyticsAssistant.LevelRestart(CurrentLevel);
        if (CurrentLevel > 1) MAXMan.ShowInterstitial();
        LoadLevel();
    }

    private static void LoadLevel()
    {
        Debug.LogFormat($"<color=#FFA500>Level {CurrentLevel} Started</color>");
        SceneManager.LoadScene(LevelToLoad);
    }
    */
}
