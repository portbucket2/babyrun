﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Jumpable : MonoBehaviour
{
    //private bool injump;
    [SerializeField] private Transform target;
    [SerializeField] private float power = 1f;
    [SerializeField] private float duration = 1f;
    [SerializeField] private float targetAngle = 70f;

    private PlayerController playerController;

    private void Start()
    {
        playerController = PlayerController.Instance;
    }

    private void OnTriggerEnter(Collider other)
    {
        Vector3 targetDir = target.position.XZ() - playerController.transform.position.XZ();
        float angle = Vector3.Angle(targetDir, playerController.transform.forward);

        if (angle < targetAngle)
        {
            playerController.rig.SetBabyAnimation(Const.JUMP);
            playerController.Jump(target.position, power, duration);
        }
    }
}
