using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;
using DG.Tweening;
using UnityEngine.Events;

public class ChoiceLevelUI : MonoBehaviour
{
    [SerializeField] internal GameObject choicePanel;
    [SerializeField] internal Button correctChoiceButton;
    [SerializeField] internal Button wrongChoiceButton;
    [SerializeField] internal PickableItem correctItem;
    [SerializeField] internal PickableItem wrongItem;
    [SerializeField] internal UnityEvent onCorrectButtonPressed;
    [SerializeField] internal UnityEvent onWrongButtonPressed;

    private PlayerController playerController;

    void Start()
    {
        correctChoiceButton?.onClick.AddListener(OnCorrectChoice);
        wrongChoiceButton?.onClick.AddListener(OnWrongChoice);

        playerController = PlayerController.Instance;
        choicePanel.Inactive();
    }

    public void ShowChoicePopup()
    {
        choicePanel.Active();
        choicePanel.transform.DOScale(1f, 0.25f);
    }

    private void OnCorrectChoice()
    {
        LevelManager.Instance.ReportState(LevelState.Success);
        if (correctItem) correctItem.OnInteract();
        choicePanel.Inactive();
        onCorrectButtonPressed?.Invoke();
        if(playerController) playerController.inputEnabled = true;
    }

    private void OnWrongChoice()
    {
        LevelManager.Instance.ReportState(LevelState.Fail);
        if (wrongItem) wrongItem.OnInteract();
        choicePanel.Inactive();
        onWrongButtonPressed?.Invoke();
        if (playerController) playerController.inputEnabled = true;
    }
}
