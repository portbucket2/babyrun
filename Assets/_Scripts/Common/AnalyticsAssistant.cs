using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PotatoSDK;
using GameAnalyticsSDK;
using LionStudios.Suite.Analytics;
using FRIA;

#if UNITY_EDITOR
using LionStudios.Suite.Debugging;
#endif

public static class AnalyticsAssistant
{
    private static bool lionInitialized = false;

    public static void EnsureInit()
    {
        if (lionInitialized) return;
        lionInitialized = true;

#if UNITY_EDITOR
        LionDebugger.Hide();
#endif

        LionAnalytics.GameStart();
    }

    public static void LevelStarted(int levelNumber)
    {
        if (!Potato.IsReady)
        {
            if(!Application.isEditor) Debug.LogError("Analytics call made before Potato is ready!");
            else Debug.LogWarning("Analytics call made before Potato is ready!");
            return;
        }
        EnsureInit();
        $"start{levelNumber}".Debug("FF00FF");
        LionAnalytics.LevelStart(levelNumber, 1);
        //GameAnalytics.NewProgressionEvent(GAProgressionStatus.Start, levelNumber.ToString());
    }

    public static void LevelCompleted(int levelNumber)
    {
        if (!Potato.IsReady)
        {
            if (!Application.isEditor) Debug.LogError("Analytics call made before Potato is ready!");
            else Debug.LogWarning("Analytics call made before Potato is ready!");
            return;
        }

        EnsureInit();
        $"complete {levelNumber}".Debug("FF00FF");
        LionAnalytics.LevelComplete(levelNumber, 1);
        //GameAnalytics.NewProgressionEvent(GAProgressionStatus.Complete, levelNumber.ToString());
    }

    public static void LevelFailed(int levelNumber)
    {
        if (!Potato.IsReady)
        {
            if (!Application.isEditor) Debug.LogError("Analytics call made before Potato is ready!");
            else Debug.LogWarning("Analytics call made before Potato is ready!");
            return;
        }
        EnsureInit();
        $"fail {levelNumber}".Debug("FF00FF");
        LionAnalytics.LevelFail(levelNumber, 1);
        //GameAnalytics.NewProgressionEvent(GAProgressionStatus.Fail, levelNumber.ToString());
    }

    //public static void LevelRestart(int levelNumber)
    //{
    //    if (!Potato.IsReady)
    //    {
    //        if (!Application.isEditor) Debug.LogError("Analytics call made before Potato is ready!");
    //        else Debug.LogWarning("Analytics call made before Potato is ready!");
    //        return;
    //    }
    //    EnsureInit();
    //    LionAnalytics.LevelRestart(levelNumber, 1);
    //    GameAnalytics.NewDesignEvent($"Restart:Level_{levelNumber}");
    //    //GameAnalytics.NewProgressionEvent(GAProgressionStatus., levelNumber.ToString());
    //}

    public static void ReportClick(AnalyticClickType type, int levelNumber,string param = "")
    {
        if (!Potato.IsReady)
        {
            if (!Application.isEditor) Debug.LogError("Analytics call made before Potato is ready!");
            else Debug.LogWarning("Analytics call made before Potato is ready!");
            return;
        }
        EnsureInit();
        string logString = $"CLICK:{type}:LEVEL_{levelNumber}";
        if (!string.IsNullOrEmpty(param)) logString = $"{logString}:{param}";
        //logString.Debug("FF00FF");
        GameAnalytics.NewDesignEvent(logString);
    }
    public static void ReportCollect(AnalyticCollectType type, int levelNumber, string itemID="", int itemValue = -1)
    {
        if (!Potato.IsReady)
        {
            if (!Application.isEditor) Debug.LogError("Analytics call made before Potato is ready!");
            else Debug.LogWarning("Analytics call made before Potato is ready!");
            return;
        }
        EnsureInit();
        string logString = $"COLLECT:{type}:LEVEL_{levelNumber}";
        if (!string.IsNullOrEmpty(itemID)) logString = $"{logString}:{itemID}";
        if (itemValue>=0) logString = $"{logString}_{itemValue}";
        //logString.Debug("00FFFF");
        GameAnalytics.NewDesignEvent(logString);
    }
}
public enum AnalyticClickType
{
    ROOM_CUSTOMIZE_BUTTON = 0,//
    LEVEL_RETRY =1,//
    HOME_BUTTON = 2,
    SKIP_BONUS_LEVEL = 3,
    SKIP_COIN_MULTIPLIER = 4,//
}
public enum AnalyticCollectType
{
    RV_MULTIPLIED_COINS = 0,//
    RV_ENERGY_REFILL = 1,
    RV_BONUS_LEVEL = 2,//
    RV_ROOM_CUSTOMIZE = 3,//
    COINS_ROOM_CUSTOMIZE = 4,//
}