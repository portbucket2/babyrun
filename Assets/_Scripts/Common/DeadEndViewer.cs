using UnityEngine;
using FRIA;

public class DeadEndViewer : MonoBehaviour
{
    public const float MAX_DIST = 5;
    public const string LAYER_NAME = "dead_end";
    [SerializeField] private float minimumDistance = 0.15f;
    [SerializeField] private float maximumDistance = 1f;
    [SerializeField] private bool onGround = true;

    private MeshRenderer rend;
    private Transform target;
    private void Awake()
    {
        MeshCollider mc = GetComponent<MeshCollider>();
        if (mc)
        {
            Destroy(mc);
            gameObject.AddComponent<BoxCollider>();
        }
    }

    private void Start()
    {
        rend = GetComponent<MeshRenderer>();
        target = PlayerController.Instance.transform;

        if (!target) return;
        rend.sharedMaterial.SetVector("_Threshold", new Vector2(minimumDistance, maximumDistance));
        gameObject.SetLayer(LAYER_NAME);
        Collider c = this.GetComponent<Collider>();
        if (!c || !c.enabled)
        {
            this.transform.parent.gameObject.SetLayer(LAYER_NAME);
        }
    }

    Vector3 position;
    float distance;
    bool success;
    private void FixedUpdate()
    {
        if (!target) return;

        Vector3 targetDir = target.position.XZ() - this.transform.position.XZ();
        Vector3 raycastDir = transform.forward * (Vector3.Dot(targetDir, transform.forward) > 0 ? -1 : 1);
        RaycastHit rch;
        Vector3 origin = target.position + new Vector3(0, 0.35f, 0);
        if (Physics.Raycast(origin, raycastDir, out rch, MAX_DIST, layerMask: 1 << LayerMask.NameToLayer(LAYER_NAME)))
        {
            success = true;
            distance = rch.distance;
            position = rch.point;
            Debug.DrawLine(origin, rch.point, Color.cyan);

        }
        else
        {
            Debug.DrawRay(origin, raycastDir * MAX_DIST, Color.red);
            success = false;
        }
        if (success)
        {
            if (onGround)
            {
                rend.sharedMaterial.SetVector("_TargetPosition", (position - distance * raycastDir).XZ());
            }
            else
            {
                rend.sharedMaterial.SetVector("_TargetPosition", (position - distance * raycastDir));
            }
        }
    }
}