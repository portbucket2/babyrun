﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FRIA;
#if UNITY_EDITOR
using UnityEditor;
#endif




[System.Serializable]
public class TextureItemProfile : BasicItemProfile
{
    public Texture2D texture;
    public TextureItemProfile()
    {
    }
    public TextureItemProfile(int id)
    {
        this.id = id;
    }
}

#if UNITY_EDITOR
[CustomPropertyDrawer(typeof(TextureItemProfile))]
public class TextureItemProfileEditor : PropertyDrawer
{
    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        EditorGUIUtility.labelWidth = 100;
        string id = property.FindPropertyRelative("id").intValue.ToString();
        if (string.IsNullOrEmpty(id))
        {
            id = "no_id";
        }
        label.text = string.Format("{0}_{1}", CategoryTab_TextureEditor.currentSelection.title, id);


        EditorGUI.BeginProperty(position, label, property);

        position = EditorGUI.PrefixLabel(position, GUIUtility.GetControlID(FocusType.Passive), label);

        var indent = EditorGUI.indentLevel;
        EditorGUI.indentLevel = 0;


        float elementHeight = (position.height) / 2;
        var titleRect = new Rect(position.x, position.y, 120, elementHeight);
        var spriteRect = new Rect(position.x + 120, position.y, position.width - 120, elementHeight);


        var numberRect = new Rect(position.x, position.y + elementHeight, 120, elementHeight);
        var textureRect = new Rect(position.x + 120, position.y + elementHeight, position.width - 120, elementHeight);

        EditorGUI.PropertyField(titleRect, property.FindPropertyRelative("id"), GUIContent.none);
        EditorGUI.PropertyField(spriteRect, property.FindPropertyRelative("displaySprite"), GUIContent.none);
        EditorGUI.PropertyField(numberRect, property.FindPropertyRelative("cost"), GUIContent.none);
        EditorGUI.PropertyField(textureRect, property.FindPropertyRelative("texture"), GUIContent.none);

        EditorGUI.indentLevel = indent;


        EditorGUI.EndProperty();

    }

    public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
    {
        return base.GetPropertyHeight(property, label) * 2;
    }

}
#endif