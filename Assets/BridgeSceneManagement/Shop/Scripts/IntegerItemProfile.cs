﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FRIA;
#if UNITY_EDITOR
using UnityEditor;
#endif




[System.Serializable]
public class IntegerItemProfile : BasicItemProfile
{
    public int integerData;
    public IntegerItemProfile()
    {
    }
    public IntegerItemProfile(int id)
    {
        this.id = id;
    }
}

#if UNITY_EDITOR
[CustomPropertyDrawer(typeof(IntegerItemProfile))]
public class IntegerItemProfileEditor : PropertyDrawer
{
    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        EditorGUIUtility.labelWidth = 0;
        label.text = string.Format("");

        EditorGUI.BeginProperty(position, label, property);

        position = EditorGUI.PrefixLabel(position, GUIUtility.GetControlID(FocusType.Passive), label);

        var indent = EditorGUI.indentLevel;
        EditorGUI.indentLevel = 0;

        float elementHeight = (position.height - 10) / 3;
        float imageOffset = position.height;

        float labelWidth = 100;

        var label_idRect = new Rect(position.x, position.y, labelWidth, elementHeight);
        var label_priceRect = new Rect(position.x, position.y + elementHeight + 5, labelWidth, elementHeight);
        var label_integerRect = new Rect(position.x, position.y + elementHeight * 2 + 10, labelWidth, elementHeight);

        var idRect = new Rect(position.x + labelWidth, position.y, position.width - imageOffset - labelWidth, elementHeight);
        var priceRect = new Rect(position.x+ labelWidth, position.y + elementHeight+5, position.width - imageOffset- labelWidth, elementHeight);
        var integerRect = new Rect(position.x+ labelWidth, position.y + elementHeight*2+10, position.width - imageOffset- labelWidth, elementHeight);
        var imageRect = new Rect(position.x + position.width - imageOffset, position.y, imageOffset, position.height);

        EditorGUI.LabelField(label_idRect, "ID");
        EditorGUI.LabelField(label_priceRect, "Price");
        EditorGUI.LabelField(label_integerRect, "Data");

        //EditorGUI.LabelField(labelRect, "isRight?");
        EditorGUI.PropertyField(idRect, property.FindPropertyRelative("id"), GUIContent.none);
        EditorGUI.PropertyField(priceRect, property.FindPropertyRelative("cost"), GUIContent.none);
        EditorGUI.PropertyField(integerRect, property.FindPropertyRelative("integerData"), GUIContent.none);
        //EditorGUI.PropertyField(imageRect, property.FindPropertyRelative("itemSprite"), GUIContent.none);
        property.FindPropertyRelative("displaySprite").objectReferenceValue = EditorGUI.ObjectField(imageRect, property.FindPropertyRelative("displaySprite").objectReferenceValue, typeof(Sprite), false);
        

        EditorGUI.indentLevel = indent;


        EditorGUI.EndProperty();

    }

    public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
    {
        return base.GetPropertyHeight(property, label) * 3 + 10;
    }
}

//{
//    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
//    {
//        EditorGUIUtility.labelWidth = 100;
//        string id = property.FindPropertyRelative("id").intValue.ToString();
//        if (string.IsNullOrEmpty(id))
//        {
//            id = "no_id";
//        }
//        label.text = string.Format("{0}_{1}", CategoryTab_IntegerEditor.currentSelection.title, id);


//        EditorGUI.BeginProperty(position, label, property);

//        position = EditorGUI.PrefixLabel(position, GUIUtility.GetControlID(FocusType.Passive), label);

//        var indent = EditorGUI.indentLevel;
//        EditorGUI.indentLevel = 0;


//        float elementHeight = (position.height) / 2;
//        var titleRect = new Rect(position.x, position.y, 120, elementHeight);
//        var spriteRect = new Rect(position.x + 120, position.y, position.width - 120, elementHeight);


//        var numberRect = new Rect(position.x, position.y + elementHeight, 120, elementHeight);
//        var textureRect = new Rect(position.x + 120, position.y + elementHeight, position.width - 120, elementHeight);

//        EditorGUI.PropertyField(titleRect, property.FindPropertyRelative("id"), GUIContent.none);
//        EditorGUI.PropertyField(spriteRect, property.FindPropertyRelative("displaySprite"), GUIContent.none);
//        EditorGUI.PropertyField(numberRect, property.FindPropertyRelative("cost"), GUIContent.none);
//        EditorGUI.PropertyField(textureRect, property.FindPropertyRelative("integerData"), GUIContent.none);

//        EditorGUI.indentLevel = indent;


//        EditorGUI.EndProperty();

//    }

//    public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
//    {
//        return base.GetPropertyHeight(property, label) * 2;
//    }

//}
#endif