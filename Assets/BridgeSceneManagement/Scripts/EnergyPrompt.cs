using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FRIA;
using PotatoSDK;
using TMPro;
using UnityEngine.UI;
public class EnergyPrompt : MonoBehaviour
{
    public static EnergyPrompt Instance { get; private set; }
    public GameObject root;

    public TextMeshProUGUI timeleftText;
    public Button RV_button;
    public Button closeButton;
    private void Awake()
    {
        root.Inactive();
        Instance = this;
        RV_button.onClick.AddListener(() => {
            RV_button.interactable = false;
            MAXMan.ShowRV((bool success) => {
                if (success)
                {
                    EnergyTracker.RefillEnergy();
                    OnClose();
                    AnalyticsAssistant.ReportCollect(AnalyticCollectType.RV_ENERGY_REFILL, LevelLoader.SelectedLevelNumber);
                    if (BridgeSceneManager.instance)
                    {
                        this.Add_DelayedAct(() =>
                        {
                            BridgeSceneManager.instance.OnTapDetected();
                        }, 0.25f);
                        //BridgeSceneManager.instance.SwitchState(BridgeState.Leave);
                    }
                    //    if (LevelProgressionController.Proceed_IsBonus(fromHome: true))
                    //{
                    //    this.Add_DelayedAct(() => {
                    //    },0.25f);
                    //}
                }
                RV_button.interactable = true;
            });
        });
        closeButton.onClick.AddListener(() => {
            OnClose();
        });
    }

    public void OnClose()
    {
        root.SetActive(false);
        if (BridgeSceneManager.instance)
        {
            BridgeSceneManager.instance.BackToBase();
        }
        else
        {
        }
    }

    public static void Load()
    {
        Instance.root.Active();
    }

    private void Update()
    {

        if (!BuildSpecManager.enableEnergySystem) return;

        if (EnergyTracker.EnergyLeft < EnergyTracker.ENERGY_CAP)  timeleftText.text = $"GET 1 ENERGY\nIN <color=#55C9EB>{EnergyTracker.energyAutoRefiller.TimeRemaining_String}</color>";
        timeleftText.gameObject.SetActive(EnergyTracker.EnergyLeft < EnergyTracker.ENERGY_CAP);
        //{
        //    timeleftText.gameObject.SetActive(false);
        //    return;
        //}
        //if (EnergyTracker.EnergyLeft < EnergyTracker.ENERGY_CAP)
        //{
        //    timeleftText.gameObject.SetActive(true);
        //}
        //else
        //{

        //    timeleftText.gameObject.SetActive(false);
        //}
    }
}
