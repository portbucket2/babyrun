using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ThemeWiseLoader : MonoBehaviour
{
    protected static ShopGroupManager _roomShop;
    protected static ShopGroupManager roomShop
    {
        get
        {
            if (_roomShop == null) _roomShop = Resources.Load<ShopGroupManager>("RoomShop");
            return _roomShop;
        }
    }
    private void OnEnable()
    {
        roomShop.onChoiceChanged += OnChoiceChanged;
    }
    private void OnDisable()
    {
        roomShop.onChoiceChanged -= OnChoiceChanged;
    }
    public int activationChoiceID = -1;
    private void OnChoiceChanged(int categoryID, int choice)
    {
        
    }

    

}
