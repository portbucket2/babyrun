using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SleepyController : MonoBehaviour
{
    public Animator anim;
    public ParticleSystem zzzParticle;
    public Vector3 sleepRot;
    public GameObject happySleepyFace;
    public GameObject normalFace;
    // Start is called before the first frame update
    void Start()
    {
        Currency._coinMan.AddListner_BalanceChanged(CurrencyType.ENERGY, EnergyChanged);
        EnergyChanged();
        if (!EnergyTracker.hasEnergyToPlay)
        {
            this.transform.parent.rotation = Quaternion.Euler(sleepRot);
        }
    }
    private void OnDestroy()
    {
        Currency._coinMan.RemoveListner_BalanceChanged( CurrencyType.ENERGY,EnergyChanged);
    }

    // Update is called once per frame
    void EnergyChanged()
    {
        anim.SetBool("sleepy",!EnergyTracker.hasEnergyToPlay);
        happySleepyFace.SetActive(!EnergyTracker.hasEnergyToPlay);
        normalFace.SetActive(EnergyTracker.hasEnergyToPlay);
        PlayerController.Instance.inputEnabled = EnergyTracker.hasEnergyToPlay;
    }
    private void Update()
    {
        if (PlayerController.Instance.moving)
        {
            SetPartcile(false);
        }
        else 
        {
            SetPartcile(!EnergyTracker.hasEnergyToPlay);
        }
        
    }
    bool isParticleActive = false;
    void SetPartcile(bool active)
    {
        if (active == isParticleActive) return;
        else
        {
            zzzParticle.gameObject.SetActive(active);
            //if (active)
            //{
            //    zzzParticle.Play();
            //}
            //else
            //{
            //    zzzParticle.Stop();
            //}
        }
        isParticleActive = active;
    }
}
