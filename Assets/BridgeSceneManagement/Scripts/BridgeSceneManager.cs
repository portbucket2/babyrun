﻿using FRIA;
//using Portbliss.Ad;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using TMPro;

public class BridgeSceneManager : MonoBehaviour
{
    public static BridgeSceneManager instance;

    public Transform selectableModels;
    public TextMeshProUGUI levelNumberText;

    public Transform playerTransform;

    public BridgeState state { get { return switcheroo.currentState; } }
    public bool allowSwipe
    {
        get
        {
            return state == BridgeState.IDLE;
        }
    }

    public GameObject noAdGameObject;
    [Header("Buttons")]
    public Button noAdButton;
    public Button levelsButton;
    public Button skinShopButton;
    public Button roomShopButton;
    public Button startButton;

    [Header("References")]
    public List<GameObject> idleList;
    public List<GameObject> loaderList;
    public List<GameObject> shopList;
    public List<GameObject> settingsList;
    public List<GameObject> energyPrompList;


    StateSwitchController<BridgeState> switcheroo;

    private Vector3 initialBabyPosition;
    private void Start()
    {
        //int index = Random.Range(0, selectableModels.childCount);
        //int i = 0;
        //foreach (Transform item in selectableModels)
        //{
        //    item.gameObject.SetActive(i==index);
        //    i++;
        //}

        instance = this;

        switcheroo = new StateSwitchController<BridgeState>();
        switcheroo.AddState(BridgeState.Leave, new List<GameObject>());
        switcheroo.AddState(BridgeState.IDLE, idleList);
        switcheroo.AddState(BridgeState.IDLE_NO_INPUT, idleList);
        switcheroo.AddState(BridgeState.LEVEL_LOAD, loaderList);
        switcheroo.AddState(BridgeState.SHOP, shopList);
        switcheroo.AddState(BridgeState.SETTINGS, settingsList);
        switcheroo.AddState(BridgeState.ENERGY, energyPrompList);
        SwitchState(BridgeState.IDLE);

        levelsButton.onClick.AddListener(OnLevelListShowButton);
        skinShopButton.onClick.AddListener(OnSkinShopButton);
        roomShopButton.onClick.AddListener(OnRoomShopButton);
        noAdButton.onClick.AddListener(OnNoAdButton);
        startButton.onClick.AddListener(OnTapDetected);


        levelNumberText.text = $"LEVEL {LevelLoader.SelectedLevelNumber}";
        //noAdGameObject.SetActive(NoAdManager.ShowPurchaseButton);
        initialBabyPosition = playerTransform.position;
    }

    public void OnTapDetected()
    {
        //if (GDPRController.isGDPRFlowOnRequestActive) return;
        //if (SpecialBuildManager.enableUIToggle )
        //{
        //    if (mousePos.y > (0.75f * (float)Screen.height)) return;
        //}

        this.Add_DelayedAct(() =>
        {
        if (state == BridgeState.IDLE) 
            {

                if (!EnergyTracker.hasEnergyToPlay)
                {
                    SwitchState(BridgeState.ENERGY);
                }
                else
                {
                    if (LevelProgressionController.Proceed_IsBonus(fromHome: true))
                    {
                        SwitchState(BridgeState.Leave);
                    }
                    //LevelManager.LoadLevelDirect(autoTapEnable: true);
                }

            }
        }, 0);

    }

    public void SwitchState(BridgeState state)
    {
        switch (state)
        {
            case BridgeState.IDLE:
                //CrossPromoController.Active(true);
                break;
            default:
                //CrossPromoController.Active(false);
                break;
        }
        switcheroo.SwtichState(state);
    }


    public void OnNoAdButton()
    {
        SwitchState(BridgeState.IDLE_NO_INPUT);
        BackToBase();
        //NoAdManager.RequestNoAdPurchase((bool success) =>
        //{
        //    if (success)
        //    {
        //        noAdButton.gameObject.SetActive(false);
        //    }
        //});
    }
    public void OnSkinShopButton()
    {
        SwitchState(BridgeState.SHOP);
        ShopLoadManager.instance.LoadShop(ShopType.SKIN);
    }
    public void OnRoomShopButton()
    {

        SwitchState(BridgeState.SHOP);
        ShopLoadManager.instance.LoadShop(ShopType.ROOM);
        AnalyticsAssistant.ReportClick(AnalyticClickType.ROOM_CUSTOMIZE_BUTTON, LevelLoader.SelectedLevelNumber);
        playerTransform.position = initialBabyPosition;
        playerTransform.GetComponent<PlayerController>().inputEnabled = false;
    }
    public void OnLevelListShowButton()
    {
        if (!EnergyTracker.hasEnergyToPlay)
        {
            SwitchState(BridgeState.ENERGY);
        }
        else
        {
            SwitchState(BridgeState.LEVEL_LOAD);
        }
    }

    public void OnSettingsShowButton()
    {
        SwitchState(BridgeState.SETTINGS);
    }
    public void BackToBase(float delay = 0.1f)
    {
        this.Add_DelayedAct(() =>
        {
            if(EnergyTracker.hasEnergyToPlay) playerTransform.GetComponent<PlayerController>().inputEnabled = true;
            SwitchState(BridgeState.IDLE);
        }, delay);

    }

}


public enum BridgeState
{
    IDLE_NO_INPUT = -2,
    Leave = -1,
    IDLE = 0,
    LEVEL_LOAD = 1,
    SHOP = 2,
    SETTINGS = 3,
    ENERGY = 4
}

