﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TextureRoomItemLoader : RoomItemLoader
{
    Texture defaultTex;

    public override void OnApplyChoice()
    {
        if(defaultTex == null) defaultTex = GetComponent<Renderer>().material.GetTexture("_MainTexture");
        base.OnApplyChoice();
        int i = 0;
        TextureItemProfile ip = roomShop.GetItemProfile(activationCategory) as TextureItemProfile;
        Texture t2d = defaultTex;
        if (ip!=null) t2d = ip.texture ;

        GetComponent<Renderer>().material.SetTexture("_MainTexture", t2d);
    }

}
