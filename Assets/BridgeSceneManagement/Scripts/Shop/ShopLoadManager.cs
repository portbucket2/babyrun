﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using UnityEngine.UI;
using PotatoSDK;

public class ShopLoadManager : MonoBehaviour
{

    public Button closeButton;
    public GameObject rootObject;
    public RectTransform rootRect;

    public static ShopLoadManager instance;
    public SlidePanelController tabPanel;
    public SlidePanelController itemPanel;


    bool adBZ;

    float rootBottomOffset_ad;
    float rootBottomOffset_noad;
    private void Awake()
    {
        rootBottomOffset_ad = rootRect.offsetMin.y;
        rootBottomOffset_noad = 20;
        instance = this;
        closeButton.onClick.AddListener(OnClose);
        rootObject.SetActive(false);
    }

    void OnClose()
    {
        FRIA.CamSwapController.Instance.SetCamIndex(0);
        rootObject.SetActive(false);
        BridgeSceneManager.instance.BackToBase();
    }

    public ShopGroupManager skinGroup;
    public ShopGroupManager roomGroup;

    private ShopGroupManager currentShopGroup;


    public bool isSkinShopSelected;


    public bool isCheck;

    private bool CheckShop()
    {
        bool isCheck = false;

        if (isSkinShopSelected)// && IAPManager.IsShopBundlePurchased)
        {
            isCheck = true;
        }
        else
        {
            isCheck = false;
        }

        return isCheck;
    }

    public void LoadShop(ShopType type)
    {
        rootRect.offsetMin = new Vector2(rootRect.offsetMin.x, rootBottomOffset_ad);// NoAdManager.isBlockingAds? rootBottomOffset_noad : rootBottomOffset_ad);
        switch (type)
        {
            case ShopType.SKIN:
                currentShopGroup = skinGroup;
                isSkinShopSelected = true;
                break;
            case ShopType.ROOM:
                currentShopGroup = roomGroup;
                isSkinShopSelected = false;
                break;
        }
        rootObject.SetActive(true);
        OnShopCategorySelection(0);
    }

    void OnShopCategorySelection(int tabIndex)
    {
        LoadCategory(tabIndex);
        tabPanel.Load(currentShopGroup.tablist, tabIndex, OnShopCategorySelection, false) ;
    }
    private void LoadCategory(int tabIndex)
    {

        FRIA.CamSwapController.Instance.SetCamIndex(tabIndex+1);
        CategoryTab catTab = (currentShopGroup.tablist[tabIndex] as CategoryTab);


        if (CheckShop())
        {
            for (int i = 0; i < catTab.slideItemList.Count; i++)
            {
                IPurchasable slidable = catTab.slideItemList[i] as IPurchasable;
                slidable.unlocked = true;
            }
        }

        itemPanel.Load(catTab.slideItemList, currentShopGroup.GetChoice(tabIndex) ,(int i) => 
        {
            ItemClick(tabIndex, i);
        }, catTab.defaultIsEmpty, catTab.defaultItem);
    }
    void ItemClick(int tab, int selection)
    {
        CategoryTab catTab = (currentShopGroup.tablist[tab] as CategoryTab);
        SkinType sType = (SkinType)tab;

        if (selection != 0)
        {
            IPurchasable sip = catTab.slideItemList[selection - 1] as IPurchasable;
            if (!sip.unlocked)
            {
                bool adType = sip.cost < 0;
                if (adType)
                {
                    OnAd(() => {
                        sip.unlocked = true;
                        ApplySelection(tab, selection);


                        AnalyticsAssistant.ReportCollect(AnalyticCollectType.RV_ROOM_CUSTOMIZE,LevelLoader.SelectedLevelNumber,$"{catTab.title}_{selection}");
                    });
                }
                else
                {
                    if (Currency.Balance(CurrencyType.COIN) >= sip.cost)
                    {
                        sip.unlocked = true;
                        Currency.Transaction(CurrencyType.COIN, -sip.cost);
                        //BasicCoinTracker.ChangeCoin(-sip.cost);
                        AnalyticsAssistant.ReportCollect(AnalyticCollectType.COINS_ROOM_CUSTOMIZE, LevelLoader.SelectedLevelNumber, $"{catTab.title}_{selection}",sip.cost);
                        ApplySelection(tab,selection);
                    }
                }
            }
            else
            {
                ApplySelection(tab, selection);
            }
        }
        else
        {
            ApplySelection(tab, selection);
        }
    }
    void ApplySelection(int tab, int selection)
    {
        currentShopGroup.SetChoice( tab, selection);
        LoadCategory(tab);
    }

    void OnAd(Action onSuccess, Action onFail=null)
    {
        if (adBZ) return;
        else adBZ = true;
        //RVPlacement rvPlacement;
        //switch (currentShopGroup.shopType)
        //{
        //    default:
        //    case ShopType.SKIN:
        //        rvPlacement = RVPlacement.skin_shop_unlock;
        //        break;
        //    case ShopType.ROOM:
        //        rvPlacement = RVPlacement.room_shop_unlock;
        //        break;
        //}
        //AnalyticsAssistant.LogRv(RVState.rv_click, rvPlacement);
        MAXMan.ShowRV((success) =>
        {
            adBZ = false;
            if (success)
            {
                onSuccess?.Invoke();
                //AnalyticsAssistant.LogRv(RVState.rv_complete, rvPlacement);
            }
            else
            {
                onFail?.Invoke();
            }
        });
        //bool didShowRV = AdController.ShowRewardedVideoAd();
        //if(didShowRV) AnalyticsAssistant.LogRv(RVState.rv_start, rvPlacement);

    }
  
}
