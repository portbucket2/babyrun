﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HierarchyRoomItemLoader : RoomItemLoader
{
    public override void OnApplyChoice()
    {
        base.OnApplyChoice();
        int i = 0;
        IntegerItemProfile iip = roomShop.GetItemProfile(activationCategory) as IntegerItemProfile;
        int chosenHierarchy = -1;
        if (iip!=null) chosenHierarchy = iip.integerData ;
        foreach (Transform item in this.transform)
        {
            item.gameObject.SetActive(i == chosenHierarchy);
            i++;
        }
    }

}
