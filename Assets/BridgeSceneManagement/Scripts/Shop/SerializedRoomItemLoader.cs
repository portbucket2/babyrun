using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SerializedRoomItemLoader : RoomItemLoader
{
    [SerializeField] private Transform[] items;
    public override void OnApplyChoice()
    {
        base.OnApplyChoice();
        int i = 0;
        IntegerItemProfile iip = roomShop.GetItemProfile(activationCategory) as IntegerItemProfile;
        int chosenHierarchy = roomShop.customizationGroups[activationCategory].defaultIsEmpty ? -1 : 0;
        Debug.Log(chosenHierarchy);
        if (iip != null) chosenHierarchy = iip.integerData;
        foreach (Transform item in items)
        {
            item.gameObject.SetActive(i == chosenHierarchy);
            i++;
        }
    }
}
