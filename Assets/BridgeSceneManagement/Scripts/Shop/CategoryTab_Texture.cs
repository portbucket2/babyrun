﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif

[CreateAssetMenu(fileName = "CategoryTab_Texture", menuName = "Customization/CategoryTab_Texture", order = 1)]
public class CategoryTab_Texture : CategoryTab
{
    public List<TextureItemProfile> texturetabContents = new List<TextureItemProfile>();

    public override List<ISlidable> slideItemList
    {
        get
        {
            List<ISlidable> islist = new List<ISlidable>();
            islist.AddRange(texturetabContents);
            return islist;
        }
    }
}

#if UNITY_EDITOR

[CustomEditor(typeof(CategoryTab_Texture))]
public class CategoryTab_TextureEditor : Editor
{
    public static CategoryTab_Texture currentSelection;
    public override void OnInspectorGUI()
    {
        currentSelection = (CategoryTab_Texture)base.target;
        serializedObject.Update();
        EditorGUILayout.PropertyField(serializedObject.FindProperty("title"));
        EditorGUILayout.PropertyField(serializedObject.FindProperty("displaySprite"));
        EditorGUILayout.PropertyField(serializedObject.FindProperty("displaySprite_seleceted"));
        EditorGUILayout.PropertyField(serializedObject.FindProperty("defaultItem"));
        Show(serializedObject.FindProperty("texturetabContents"), currentSelection.texturetabContents);
        serializedObject.ApplyModifiedProperties();
    }

    public static void Show(SerializedProperty list, List<TextureItemProfile> listRef)
    {
        //List<SkinItemProfile> listRef = sst.tabContents;
        EditorGUILayout.Space();
        EditorGUI.indentLevel += 1;

        GUILayout.BeginVertical("Box");

        //List<int> duplicateCheckList = new List<int>();
        for (int i = 0; i < list.arraySize; i++)
        {
            //SkinItemProfile sp = splu.listup[i];
            EditorGUILayout.PropertyField(list.GetArrayElementAtIndex(i));
            GUILayout.BeginHorizontal();
            EditorGUILayout.LabelField("", GUILayout.Width(100));
            float defWidth = 70;
            float defHeight = 20;
            if (GUILayout.Button("- Remove", GUILayout.Width(defWidth), GUILayout.Height(defHeight)))
            {
                listRef.RemoveAt(i);
            }
            if (GUILayout.Button("+ Add", GUILayout.Width(defWidth), GUILayout.Height(defHeight)))
            {
                listRef.Insert(i + 1, new TextureItemProfile(i+2));
            }

            EditorGUILayout.LabelField("", GUILayout.MinWidth(0));
            //EditorGUILayout.LabelField("");
            if (GUILayout.Button("↑", GUILayout.Width(defHeight), GUILayout.Height(defHeight)))
            {
                if (i != 0)
                {
                    TextureItemProfile ld = listRef[i];
                    listRef[i] = listRef[i - 1];
                    listRef[i - 1] = ld;

                }
            }

            if (GUILayout.Button("↓", GUILayout.Width(defHeight), GUILayout.Height(defHeight)))
            {
                if (i != list.arraySize - 1)
                {
                    TextureItemProfile ld = listRef[i];
                    listRef[i] = listRef[i + 1];
                    listRef[i + 1] = ld;
                }
            }

            GUILayout.EndHorizontal();
        }

        if (listRef.Count == 0)
            if (GUILayout.Button("(+)Add"))
            {
                listRef.Insert(listRef.Count, new TextureItemProfile(1));
            }
        EditorGUILayout.EndVertical();

        EditorGUI.indentLevel -= 1;
        EditorGUILayout.Space();


        //if (GUILayout.Button("(+)Add"))
        //{
        //    scinfo.dataList.Add(new CSVDownloadData());
        //}
    }
}
#endif