﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RoomItemLoader : MonoBehaviour
{

    protected static ShopGroupManager _roomShop;
    protected static ShopGroupManager roomShop 
    {
        get 
        {
            if(_roomShop==null) _roomShop = Resources.Load<ShopGroupManager>("RoomShop");
            return _roomShop;
        }
    }

    public int activationCategory = -1;

    private void OnEnable()
    {
        roomShop.onChoiceChanged += OnChoiceChanged;
        OnApplyChoice();
    }
    private void OnDisable()
    {
        roomShop.onChoiceChanged -= OnChoiceChanged;
    }
    private void OnChoiceChanged(int categoryID, int choice)
    {
        if (categoryID == activationCategory)
        {
            OnApplyChoice();
        }
    }
    public virtual void OnApplyChoice()
    {
        
    }

}
