﻿
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;


public class SlideItemUI : MonoBehaviour
{
    public Image bgImage;
    public Sprite selectedSprite;
    public Sprite notSelectedSprite;

    public Image displayPic;
    public Button button;

    public GameObject bgModule;
    public Mask bgMask;
    public GameObject selectionIndicator;

    public GameObject costingModule;
    public GameObject coinObject;
    public GameObject adObject;
    public Text costText;


    [Header("Item Only")]
    public GameObject selectionIndicatorBG;
    public Color normalTextColor;
    public Color insufficientTextColor;
    [Header("Category Only")]
    public Color notSelectedColor = new Color(1,1,1,1);

    public void Load(bool selected, int index, Sprite sprite, bool noBG, System.Action<int> onClick)
    {
        displayPic.sprite = sprite;
        LoadBasic(selected, index, onClick,noBG);
        costingModule.SetActive(false);
    }
    public void Load(bool selected, int index, ISlidable slidable, bool noBG, System.Action<int> onClick)
    {
        displayPic.sprite = slidable.GetDisplaySprite(selected);
        IPurchasable purchasable = slidable as IPurchasable;
        LoadBasic(selected,index,onClick,noBG);

        if (purchasable!=null)
        {
            bool needsPurchase = purchasable != null && !purchasable.unlocked;
            costingModule.SetActive(needsPurchase);
            if (purchasable.cost < 0)
            {
                adObject.SetActive(true);
                coinObject.SetActive(false);
                costText.text = "free";
            }
            else
            {
                adObject.SetActive(false);
                coinObject.SetActive(true);
                costText.text = purchasable.cost.ToString();
                costText.color = Currency.Balance(CurrencyType.COIN)<purchasable.cost? insufficientTextColor:normalTextColor ;
            }
        }
    }
    private void LoadBasic(bool selected, int index, System.Action<int> onClick, bool noBG)
    {
        if (bgModule)
        {
            bgModule.SetActive(!noBG);
            bgMask.enabled = !noBG;
            bgMask.GetComponent<Image>().enabled = !noBG;
        }
        if (bgImage) bgImage.sprite = selected ? selectedSprite : notSelectedSprite;
        if(selectionIndicator) selectionIndicator.SetActive(selected);
        if (selectionIndicatorBG) selectionIndicatorBG.SetActive(selected);
        button.onClick.RemoveAllListeners();
        button.onClick.AddListener(() => { onClick?.Invoke(index); });
        displayPic.color = Color.white;//  selected ? Color.white : notSelectedColor;
        OnLoad();
    }

    public virtual void OnLoad()
    {
        
    }
}
