﻿using PotatoSDK;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;


public class LevelUIItemLoader : MonoBehaviour
{
    public GameObject mainObject;
    public Button mainButton;

    public Image bgImage;

    LevelData levelData;



    public GameObject onLockContent;
    public GameObject onUnLockContent;
    public GameObject onRewardedContent;
    public TextMeshProUGUI levelName;
    //public Text bonusName;

    [Header("Sprites")]
    public Sprite sprite_Unlocked;
    public Sprite sprite_Locked;
    public Sprite sprite_Current;
    public Sprite sprite_Bonus;

    public void LoadNormal(int levelIndex)
    {
        //LevelArea levelArea = LevelDataKeeper.instance.levelAreas[areaIndex];
        levelData = BuildSceneOrganizer.Instance.levels[levelIndex];

        mainObject.SetActive(true);
        bool isProgressUnlocked = BuildSpecManager.allLevelUnlocked || LevelLoader.GetLatestReachedLevelNumber()>=levelIndex+1;// LevelDataKeeper.instance.allLevelsUnlocked || LevelDataKeeper.instance.levelAreas[areaIndex].lastUnlockedLevelIndex.value >= levelIndex;
        bool isNowPlaying = levelIndex == LevelLoader.SelectedLevelNumber -1;// (PrimaryLoader.currentLevelInstance!=null && PrimaryLoader.currentLevelInstance.levelDefinition == levelDef && PrimaryLoader.currentLevelInstance.loadType == LoadType.NORMAL);

        mainButton.interactable = isProgressUnlocked;


        onLockContent.SetActive(!isProgressUnlocked);
        onUnLockContent.SetActive(isProgressUnlocked && !levelData.isBonusLocked);
        onRewardedContent.SetActive(isProgressUnlocked && levelData.isBonusLocked);

        levelName.gameObject.SetActive(isProgressUnlocked && !levelData.isBonus);
        //bonusName.gameObject.SetActive(isProgressUnlocked && levelData.isBonus && !levelData.isBonusLocked);

        bgImage.sprite = isNowPlaying ? sprite_Current : (isProgressUnlocked?sprite_Unlocked:sprite_Locked);

        levelName.text = string.Format("{0}", levelIndex + 1);

        mainButton.onClick.RemoveAllListeners();
        mainButton.onClick.AddListener(()=> {
            BridgeSceneManager.instance.SwitchState(BridgeState.Leave);
            LevelProgressionController.LoadLeverFromList(levelIndex + 1);
            //if (levelData.isBonusLocked)
            //{
            //    mainButton.interactable = false;
            //    LevelProgressionController.Proceed_IsBonus(true);
            //    //AnalyticsAssistant.LogRv(RVState.rv_click, RVPlacement.rewarded_level_unlock);
            //    //MAXMan.ShowRV((bool success) =>
            //    //{
            //    //    if (success)
            //    //    {
            //    //        //levelData.UnlockBonusLevel();
            //    //        p
                        
            //    //        LevelLoader.LoadLevelDirect(false, levelIndex + 1);
            //    //        //AnalyticsAssistant.LogRv(RVState.rv_complete, RVPlacement.rewarded_level_unlock);
            //    //    }
            //    //    else
            //    //    {
            //    //        mainButton.interactable = true;
            //    //    }
            //    //});
            //    //bool willShow = AdController.ShowRewardedVideoAd();
            //    //if(willShow) AnalyticsAssistant.LogRv(RVState.rv_start, RVPlacement.rewarded_level_unlock);
            //}
            //else
            //{

            //    LevelLoader.LoadLevelDirect(true, levelIndex + 1);
            //}
        });
    }
    public void LoadAsDummy()
    {
        mainObject.SetActive(false);
    }

   
}